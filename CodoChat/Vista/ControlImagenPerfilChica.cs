﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodoChat.Forms
{
    public partial class ControlImagenPerfilChica : UserControl
    {

        public Image ImagenEstado
        {
            get
            {
                return this.panelPerfil.BackgroundImage;
            }
            set
            {
                this.panelPerfil.BackgroundImage = value;
            }
        }
        public Image Imagen
        {
            get
            {
                return this.pictureBoxPerfil.Image;
            }
            set
            {
                this.pictureBoxPerfil.Image = value;
            }
        }
        public String ToolTipTexto
        {
            set
            {
                this.toolTip1.SetToolTip(this.pictureBoxPerfil, value);
            }
        }
        public ControlImagenPerfilChica()
        {
            InitializeComponent();
        }



    }
}
