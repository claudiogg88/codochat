﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodoChat.Modelo;
using CodoChat.ModeloVista;

namespace CodoChat.Forms
{
    public partial class ControlPerfil : UserControl
    {

        ModeloControlPerfil modelo;
        public ControlPerfil()
        {
           
            InitializeComponent();
            this.modelo = new ModeloControlPerfil(this);
        }

        private void sonidoAlRecibirMensaje_CheckedChanged(object sender, EventArgs e)
        {
            this.btnGuardar.Enabled = true;
        }

        private void sonidoEnConexion_CheckedChanged(object sender, EventArgs e)
        {
            this.btnGuardar.Enabled = true;
        }

        private void cmbxEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            modelo.Actualizar = true;
            this.Actualizar();
        }

        private void ocultarABandeja_CheckedChanged(object sender, EventArgs e)
        {
            this.btnGuardar.Enabled = true;
        }

        private void titilarEnMensaje_CheckedChanged(object sender, EventArgs e)
        {
            this.btnGuardar.Enabled = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            modelo.Guardar();
        }

        internal void CambiosAplicados()
        {
            this.btnGuardar.Enabled = false;
        }

        private void pContenedor_VisibleChanged(object sender, EventArgs e)
        {
            modelo.Actualizar = true;
            this.Actualizar();

        }
        
        internal void Actualizar()
        {
            modelo.ActualizarControlPerfil();
        }
    }
}
