﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Modelo;

namespace CodoChat.Forms
{
    public partial class FormEmoticon : Form
    {
        public string Abreviatura
        {
            get
            {
                return this.tBoxAbreviatura.Text;
            }
            set
            {
                this.tBoxAbreviatura.Text = value;
            }
        }
        public bool Aceptado;
        public Image Imagen
        {
            get
            {
                return this.pictureBox1.Image;
            }
            set
            {
                this.pictureBox1.Image = value;
            }
        }
        public FormEmoticon()
        {
            InitializeComponent();
            Abreviatura = "";
            Aceptado = false;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;
            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;
            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Aceptado = true;
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Image image = Image.FromFile(openFileDialog1.FileName);
                    this.tBoxAbreviatura.Text = openFileDialog1.SafeFileName;
                    this.pictureBox1.Image = image;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al leer la imagen: " + ex.Message);
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tBoxAbreviatura.Text))
            {
                this.btnGuardar.Enabled = false;
            }
            else
            {
                this.btnGuardar.Enabled = true;
            }
        }

        public void setEdicion()
        {
            this.btnBuscarImagen.Enabled = false;
        }
    }
}
