﻿namespace CodoChat.Forms
{
    partial class ControlPerfil
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pContenedor = new System.Windows.Forms.Panel();
            this.titilarEnMensaje = new System.Windows.Forms.CheckBox();
            this.ocultarABandeja = new System.Windows.Forms.CheckBox();
            this.sonidoEnConexion = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.sonidoAlRecibirMensaje = new System.Windows.Forms.CheckBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.pContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pContenedor
            // 
            this.pContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenedor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pContenedor.Controls.Add(this.titilarEnMensaje);
            this.pContenedor.Controls.Add(this.ocultarABandeja);
            this.pContenedor.Controls.Add(this.sonidoEnConexion);
            this.pContenedor.Controls.Add(this.label1);
            this.pContenedor.Controls.Add(this.sonidoAlRecibirMensaje);
            this.pContenedor.Controls.Add(this.btnGuardar);
            this.pContenedor.Location = new System.Drawing.Point(3, 3);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(754, 267);
            this.pContenedor.TabIndex = 0;
            this.pContenedor.VisibleChanged += new System.EventHandler(this.pContenedor_VisibleChanged);
            // 
            // titilarEnMensaje
            // 
            this.titilarEnMensaje.AutoSize = true;
            this.titilarEnMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titilarEnMensaje.Location = new System.Drawing.Point(12, 134);
            this.titilarEnMensaje.Name = "titilarEnMensaje";
            this.titilarEnMensaje.Size = new System.Drawing.Size(286, 24);
            this.titilarEnMensaje.TabIndex = 12;
            this.titilarEnMensaje.Text = "Titilar ventana al recibir mensaje";
            this.titilarEnMensaje.UseVisualStyleBackColor = true;
            this.titilarEnMensaje.CheckedChanged += new System.EventHandler(this.titilarEnMensaje_CheckedChanged);
            // 
            // ocultarABandeja
            // 
            this.ocultarABandeja.AutoSize = true;
            this.ocultarABandeja.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ocultarABandeja.Location = new System.Drawing.Point(12, 104);
            this.ocultarABandeja.Name = "ocultarABandeja";
            this.ocultarABandeja.Size = new System.Drawing.Size(269, 24);
            this.ocultarABandeja.TabIndex = 11;
            this.ocultarABandeja.Text = "Ocultar a bandeja al minimizar";
            this.ocultarABandeja.UseVisualStyleBackColor = true;
            this.ocultarABandeja.CheckedChanged += new System.EventHandler(this.ocultarABandeja_CheckedChanged);
            // 
            // sonidoEnConexion
            // 
            this.sonidoEnConexion.AutoSize = true;
            this.sonidoEnConexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sonidoEnConexion.Location = new System.Drawing.Point(12, 74);
            this.sonidoEnConexion.Name = "sonidoEnConexion";
            this.sonidoEnConexion.Size = new System.Drawing.Size(272, 24);
            this.sonidoEnConexion.TabIndex = 10;
            this.sonidoEnConexion.Text = "Sonido al conectarse contacto";
            this.sonidoEnConexion.UseVisualStyleBackColor = true;
            this.sonidoEnConexion.CheckedChanged += new System.EventHandler(this.sonidoEnConexion_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(747, 23);
            this.label1.TabIndex = 7;
            this.label1.Text = "Preferencias";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sonidoAlRecibirMensaje
            // 
            this.sonidoAlRecibirMensaje.AutoSize = true;
            this.sonidoAlRecibirMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sonidoAlRecibirMensaje.Location = new System.Drawing.Point(12, 44);
            this.sonidoAlRecibirMensaje.Name = "sonidoAlRecibirMensaje";
            this.sonidoAlRecibirMensaje.Size = new System.Drawing.Size(229, 24);
            this.sonidoAlRecibirMensaje.TabIndex = 6;
            this.sonidoAlRecibirMensaje.Text = "Sonido al recibir mensaje";
            this.sonidoAlRecibirMensaje.UseVisualStyleBackColor = true;
            this.sonidoAlRecibirMensaje.CheckedChanged += new System.EventHandler(this.sonidoAlRecibirMensaje_CheckedChanged);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Location = new System.Drawing.Point(50, 185);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(84, 45);
            this.btnGuardar.TabIndex = 5;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // ControlPerfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pContenedor);
            this.Name = "ControlPerfil";
            this.Size = new System.Drawing.Size(760, 273);
            this.pContenedor.ResumeLayout(false);
            this.pContenedor.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pContenedor;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.CheckBox sonidoAlRecibirMensaje;
        public System.Windows.Forms.CheckBox sonidoEnConexion;
        public System.Windows.Forms.CheckBox titilarEnMensaje;
        public System.Windows.Forms.CheckBox ocultarABandeja;

    }
}
