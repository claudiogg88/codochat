﻿namespace CodoChat.Forms
{
	partial class ControlImagenPerfilChica
	{
		/// <summary> 
		/// Variable del diseñador requerida.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Limpiar los recursos que se estén utilizando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de componentes

		/// <summary> 
		/// Método necesario para admitir el Diseñador. No se puede modificar 
		/// el contenido del método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.panelPerfil = new System.Windows.Forms.Panel();
            this.pictureBoxPerfil = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panelPerfil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPerfil)).BeginInit();
            this.SuspendLayout();
            // 
            // panelPerfil
            // 
            this.panelPerfil.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPerfil.BackgroundImage = global::CodoChat.Properties.Resources.desconectado;
            this.panelPerfil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelPerfil.Controls.Add(this.pictureBoxPerfil);
            this.panelPerfil.Location = new System.Drawing.Point(0, 0);
            this.panelPerfil.Margin = new System.Windows.Forms.Padding(0);
            this.panelPerfil.Name = "panelPerfil";
            this.panelPerfil.Size = new System.Drawing.Size(80, 73);
            this.panelPerfil.TabIndex = 0;
            // 
            // pictureBoxPerfil
            // 
            this.pictureBoxPerfil.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPerfil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPerfil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxPerfil.Image = global::CodoChat.Properties.Resources.imagen_default;
            this.pictureBoxPerfil.Location = new System.Drawing.Point(9, 8);
            this.pictureBoxPerfil.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBoxPerfil.Name = "pictureBoxPerfil";
            this.pictureBoxPerfil.Size = new System.Drawing.Size(62, 57);
            this.pictureBoxPerfil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPerfil.TabIndex = 1;
            this.pictureBoxPerfil.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBoxPerfil, "");



            // 
            // ControlImagenPerfilChica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.panelPerfil);
            this.DoubleBuffered = true;
            this.Name = "ControlImagenPerfilChica";
            this.Size = new System.Drawing.Size(80, 73);
            this.panelPerfil.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPerfil)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.Panel panelPerfil;
        private System.Windows.Forms.PictureBox pictureBoxPerfil;
        private System.Windows.Forms.ToolTip toolTip1;




    }
}
