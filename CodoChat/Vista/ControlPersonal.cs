﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Modelo;
using CodoChat.ModeloVista;

namespace CodoChat.Forms
{
    public partial class ControlPersonal : UserControl
    {
        public Boolean NickCambio { get; set; }
        public Boolean SubnickCambio { get; set; }
        public Boolean FuenteCambio { get; set; }
        public Boolean EstadoCambio { get; set; }
        public Boolean ImagenCambio { get; set; }
        public String Ruta;

        ModeloControlPersonal modelo;
        public ControlPersonal()
        {
            InitializeComponent();
            Ruta = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            this.FuenteCambio = false;
            this.EstadoCambio = false;
            this.ImagenCambio = false;
            this.NickCambio = false;
            this.SubnickCambio = false;
            this.btnGuardar.Enabled = false;
            this.modelo = new  ModeloControlPersonal(this);
        }
        public void Actualizar()
        {
            if(this.modelo!=null)
            {
                this.modelo.ActualizarControlPersonal();
            }
           
        }
        public string Nick
        {
            get
            {
                return this.rtbNick.Rtf;
            }
            set
            {
                this.rtbNick.Rtf = value;
            }
        }
        public string TextoNick
        {
            get
            {
                return this.rtbNick.Text;
            }
            set
            {
                this.rtbNick.Text = value;
            }
        }
        public string Subnick
        {
            get
            {
                return this.rtbSubnick.Rtf;
            }
            set
            {
                this.rtbSubnick.Rtf = value;
            }
        }

        public Image ImagenPerfil
        {
            get
            {
                return this.imagenPerfil1.Imagen;
            }
            set
            {
                this.imagenPerfil1.Imagen = value;
            }
        }
        public Font Fuente
        {
            get
            {
                return this.lblFuente.Font;
            }
            set
            {
                this.lblFuente.Font = value;
                this.lblFuente.Text = "Fuente";
            }
        }
        public Color ColorFuente
        {
            get
            {
                return this.lblFuente.ForeColor;
            }
            set
            {
                this.lblFuente.ForeColor = value;
            }
        }
        public String Estado
        {
            get
            {
                return this.comboEstado.Text;
            }
            set
            {
                Estado estado;
                if (String.IsNullOrEmpty(value))
                {
                    estado = CodoChat.Estado.Desconocido;
                }
                else
                {
                    estado = (CodoChat.Estado)Enum.Parse(typeof(CodoChat.Estado), value);
                }
                this.imagenPerfil1.ImagenEstado = Adornos.ObtenerBordeEstado(estado);
                this.comboEstado.Text = value;
            }
        }

        private void btnFuente_Click(object sender, EventArgs e)
        {
            FontDialog dialog = Utiles.CrearFormDialogFuente();
            DialogResult result = dialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                this.Fuente = dialog.Font;
                this.ColorFuente = dialog.Color;
                FuenteCambio = true;
                this.btnGuardar.Enabled = true;
            }
        }

        private void btnImagen_Click(object sender, EventArgs e)
        {
            FormSeleccionarImagen form = new FormSeleccionarImagen();
            form.Imagen = this.ImagenPerfil;
            form.ShowDialog(this);
            if (form.result == DialogResult.OK)
            {
                this.ImagenPerfil = form.Imagen;
                ImagenCambio = true;
                this.btnGuardar.Enabled = true;
            }
        }

        private void rtbNick_TextChanged(object sender, EventArgs e)
        {
            this.NickCambio = true;
            this.btnGuardar.Enabled = true;
        }

        private void rtbSubnick_TextChanged(object sender, EventArgs e)
        {
            this.SubnickCambio = true;
            this.btnGuardar.Enabled = true;
        }

        private void menuFuenteNick_Click(object sender, EventArgs e)
        {
            this.cmsNick.Hide();
            FontDialog form = Utiles.CrearFormDialogNick(this.rtbNick.SelectionFont, this.rtbNick.SelectionColor);

            DialogResult result = form.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                this.rtbNick.ChangeFont(form.Font, form.Color);
                this.NickCambio = true;
                this.btnGuardar.Enabled = true;
            }
        }

        private void menuFuenteSubnick_Click(object sender, EventArgs e)
        {
            this.cmsSubnick.Hide();
            FontDialog form = Utiles.CrearFormDialogSubNick(this.rtbSubnick.SelectionFont, this.rtbSubnick.SelectionColor);
            DialogResult result = form.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                this.rtbSubnick.ChangeFont(form.Font, form.Color);
                this.SubnickCambio = true;
                this.btnGuardar.Enabled = true;
            }
        }

        private void comboEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.imagenPerfil1.ImagenEstado = Adornos.ObtenerBordeEstado((Estado)Enum.Parse(typeof(Estado), this.comboEstado.SelectedItem.ToString()));
            this.EstadoCambio = true;
            this.btnGuardar.Enabled = true;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            GuardarCambios();
        }

        internal void CambiosAplicados()
        {
            this.FuenteCambio = false;
            this.EstadoCambio = false;
            this.ImagenCambio = false;
            this.NickCambio = false;
            this.SubnickCambio = false;
            this.btnGuardar.Enabled = false;
        }

        private void GuardarCambios()
        {
            this.modelo.Guardar();
        }

        private void rtbNick_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GuardarCambios();
            }
            
        }

        private void rtbSubnick_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GuardarCambios();
            }
        }

        private void Personal_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GuardarCambios();
            }
        }

        private void comboEstado_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                GuardarCambios();
            }
        }
        private void Personal_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Actualizar();
            }
        }

        private void btnReiniciar_Click(object sender, EventArgs e)
        {
            modelo.ReiniciarCuenta();
        }

    }
}
