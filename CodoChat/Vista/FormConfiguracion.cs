﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Modelo;
using CodoChat.ModeloVista;

namespace CodoChat.Forms
{
    public partial class FormConfiguracion : Form
    {
        ModeloFormConfiguracion modelo;
        public FormConfiguracion()
        {
            //this.perfil1 = new CodoChat.Forms.ControlPerfil(new ModeloControlPerfil());
            //this.logs1 = new CodoChat.Forms.ControlLogs(new ModeloControlLogs());
            //this.listaCuenta1 = new CodoChat.Forms.ControlListaCuenta(new ModeloControlListaCuenta());
            //this.personal1 = new CodoChat.Forms.ControlPersonal(new ModeloControlPersonal());
            //this.controlContactos1 = new CodoChat.Forms.ControlContactos(new ModeloControlContactos());
            InitializeComponent();
            this.modelo = new ModeloFormConfiguracion(this);
        }


        public void Mostrar(string cual)
        {
            this.ocultarTodos();

            switch (cual)
            {
                case "Personal": this.mostrarPersonal(); break;
                case "Logs": this.mostrarLogs(); break;
                case "Cuentas": this.mostrarCuentas(); break;
                case "Preferencias": this.mostrarPreferencias(); break;
                case "Contactos": this.mostrarContactos(); break;
                default: this.mostrarPersonal(); break;
            }
        }

        private void mostrarContactos()
        {
            this.ocultarTodos();
            this.controlContactos1.Visible = true;
        }
        private void btnNick_Click(object sender, EventArgs e)
        {
            this.mostrarPersonal();
        }
        private void btnPerfil_Click(object sender, EventArgs e)
        {

            this.mostrarPreferencias();

        }
        private void Cuentas_Click(object sender, EventArgs e)
        {
            this.mostrarCuentas();
        }
        private void btnLogs_Click(object sender, EventArgs e)
        {
            this.mostrarLogs();
        }
        private void btnContactos_Click(object sender, EventArgs e)
        {
            this.mostrarContactos();
        }
        private void mostrarCuentas()
        {
            this.ocultarTodos();
            this.listaCuenta1.Visible = true;
        }
        private void mostrarPreferencias()
        {
            this.ocultarTodos();
            this.perfil1.Visible = true;
        }
        private void mostrarPersonal()
        {
            this.ocultarTodos();
            this.personal1.Visible = true;
        }
        private void mostrarLogs()
        {
            this.ocultarTodos();
            this.logs1.Visible = true;
        }
        private void ocultarTodos()
        {
            this.personal1.Visible = false;
            this.listaCuenta1.Visible = false;
            this.logs1.Visible = false;
            this.perfil1.Visible = false;
            this.controlContactos1.Visible = false;
        }



        private void FormConfiguracion_FormClosing(object sender, FormClosingEventArgs e)
        {
            modelo.FormCerrandose(e);
        }

        internal  void Actualizar()
        {
            this.personal1.Actualizar();
            this.listaCuenta1.Actualizar();
            this.logs1.Actualizar();
            this.perfil1.Actualizar();
            this.controlContactos1.Actualizar();
        }
    }
}
