﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodoChat.Modelo;
using CodoChat.ModeloVista;
using System.Collections;

namespace CodoChat.Forms
{
    public partial class ControlContactos : UserControl
    {

        ModeloControlContactos modelo;

        public ControlContactos()
        {
            this.modelo = new ModeloControlContactos(this);
            InitializeComponent();
        }

        internal void Actualizar()
        {
            if (modelo != null)
            {
                modelo.ActualizarControlContactos();
            }
        }

        private void cmbGrupos_SelectedIndexChanged(object sender, EventArgs e)
        {
            modelo.CambioGrupoSeleccionado();
        }

        private void btnFusionar_Click(object sender, EventArgs e)
        {
            modelo.FusionarContactos();
        }

        private void btnSeparar_Click(object sender, EventArgs e)
        {
            modelo.SepararContactos();
        }

        private void btnNuevoGrupo_Click(object sender, EventArgs e)
        {
            modelo.NuevoGrupo();
        }

        private void btnEditarGrupo_Click(object sender, EventArgs e)
        {
            modelo.EditarGrupo();
        }

        internal string GrupoSeleccionado()
        {
            return cmbGrupos.SelectedValue != null ? ((Grupo)cmbGrupos.SelectedValue).IdGrupo : null;
        }

        private void btnOcultarContactos_Click(object sender, EventArgs e)
        {
            modelo.OcultarDesocultarContactos(((Button)sender).Text);
        }

        private void nuevoGrupoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.NuevoGrupo();
        }

        private void editarGrupoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.EditarGrupo();
        }

        private void eliminarGrupoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.EliminarGrupo();
        }

        private void copiarAToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void moverAToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void fusionarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.FusionarContactos();
        }

        private void separarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.SepararContactos();
        }

        private void ocultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.OcultarDesocultarContactos(((ToolStripMenuItem)sender).Text);
        }

        private void btnEliminarGrupo_Click(object sender, EventArgs e)
        {
            modelo.EliminarGrupo();
        }

        private void removertoolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.RemoverContactos();
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            modelo.RemoverContactos();
        }

        private void lstContactos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lstContactos_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            this.lstContactos.ListViewItemSorter = new ListViewItemComparer(e.Column);
        }
        class ListViewItemComparer : IComparer
        {
            private int col;
            public ListViewItemComparer()
            {
                col = 0;
            }
            public ListViewItemComparer(int column)
            {
                col = column;
            }
            public int Compare(object x, object y)
            {
                int returnVal = -1;
                returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                ((ListViewItem)y).SubItems[col].Text);
                return returnVal;
            }
        }
    }
}
