﻿using CodoChat.Modelo;
namespace CodoChat
{
    partial class VentanaChat
    {
        //this.controlConversacion1 = new CodoChat.Forms.ControlConversacion(Repositorio.ContactoColorCol[this.IdVentana]);
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaChat));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnFuente = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pNickYSubnick = new System.Windows.Forms.Panel();
            this.rtbSubnick = new System.Windows.Forms.RichTextBox();
            this.rtbNick = new System.Windows.Forms.RichTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.imagenContacto = new CodoChat.Forms.ControlImagenPerfil();
            this.imagenPropia = new CodoChat.Forms.ControlImagenPerfil();
            this.pConversacion = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblInfoMensaje = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboRedes = new System.Windows.Forms.ComboBox();
            //this.controlConversacion1 = new CodoChat.Forms.ControlConversacion();
            this.controlConversacion1 = new CodoChat.Forms.ControlConversacion(Repositorio.ContactoColorCol[this.IdVentana]);
            this.textoDecir = new System.Windows.Forms.RichTextBox();
            this.toolTipRed = new System.Windows.Forms.ToolTip(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.pNickYSubnick.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pConversacion.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.pNickYSubnick);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.pConversacion);
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(935, 472);
            this.panel1.TabIndex = 7;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.btnFuente);
            this.panel5.Controls.Add(this.button1);
            this.panel5.Location = new System.Drawing.Point(136, 440);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(799, 32);
            this.panel5.TabIndex = 29;
            // 
            // btnFuente
            // 
            this.btnFuente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFuente.Image = global::CodoChat.Properties.Resources.font_icon;
            this.btnFuente.Location = new System.Drawing.Point(36, 1);
            this.btnFuente.Name = "btnFuente";
            this.btnFuente.Size = new System.Drawing.Size(27, 28);
            this.btnFuente.TabIndex = 28;
            this.btnFuente.UseVisualStyleBackColor = true;
            this.btnFuente.Click += new System.EventHandler(this.btnFuente_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.AutoSize = true;
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Image = global::CodoChat.Properties.Resources.botonemoticon;
            this.button1.Location = new System.Drawing.Point(6, 1);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 28);
            this.button1.TabIndex = 27;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.clickEmoticones);
            // 
            // pNickYSubnick
            // 
            this.pNickYSubnick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pNickYSubnick.Controls.Add(this.rtbSubnick);
            this.pNickYSubnick.Controls.Add(this.rtbNick);
            this.pNickYSubnick.Location = new System.Drawing.Point(136, 0);
            this.pNickYSubnick.Name = "pNickYSubnick";
            this.pNickYSubnick.Size = new System.Drawing.Size(795, 61);
            this.pNickYSubnick.TabIndex = 28;
            // 
            // rtbSubnick
            // 
            this.rtbSubnick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbSubnick.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtbSubnick.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbSubnick.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbSubnick.Location = new System.Drawing.Point(5, 31);
            this.rtbSubnick.Multiline = false;
            this.rtbSubnick.Name = "rtbSubnick";
            this.rtbSubnick.ReadOnly = true;
            this.rtbSubnick.Size = new System.Drawing.Size(794, 30);
            this.rtbSubnick.TabIndex = 3;
            this.rtbSubnick.Text = "";
            // 
            // rtbNick
            // 
            this.rtbNick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbNick.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtbNick.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbNick.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbNick.Location = new System.Drawing.Point(5, 0);
            this.rtbNick.Multiline = false;
            this.rtbNick.Name = "rtbNick";
            this.rtbNick.ReadOnly = true;
            this.rtbNick.Size = new System.Drawing.Size(793, 30);
            this.rtbNick.TabIndex = 2;
            this.rtbNick.Text = "";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.imagenContacto);
            this.panel3.Controls.Add(this.imagenPropia);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(134, 472);
            this.panel3.TabIndex = 27;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panel2.Location = new System.Drawing.Point(3, 135);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(132, 208);
            this.panel2.TabIndex = 33;
            // 
            // imagenContacto
            // 
            this.imagenContacto.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imagenContacto.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.imagenContacto.Imagen = ((System.Drawing.Image)(resources.GetObject("imagenContacto.Imagen")));
            this.imagenContacto.ImagenEstado = ((System.Drawing.Image)(resources.GetObject("imagenContacto.ImagenEstado")));
            this.imagenContacto.Location = new System.Drawing.Point(7, 9);
            this.imagenContacto.Name = "imagenContacto";
            this.imagenContacto.Size = new System.Drawing.Size(120, 120);
            this.imagenContacto.TabIndex = 30;
            // 
            // imagenPropia
            // 
            this.imagenPropia.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.imagenPropia.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.imagenPropia.Imagen = ((System.Drawing.Image)(resources.GetObject("imagenPropia.Imagen")));
            this.imagenPropia.ImagenEstado = ((System.Drawing.Image)(resources.GetObject("imagenPropia.ImagenEstado")));
            this.imagenPropia.Location = new System.Drawing.Point(3, 349);
            this.imagenPropia.Name = "imagenPropia";
            this.imagenPropia.Size = new System.Drawing.Size(120, 120);
            this.imagenPropia.TabIndex = 29;
            // 
            // pConversacion
            // 
            this.pConversacion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pConversacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pConversacion.Controls.Add(this.panel4);
            this.pConversacion.Controls.Add(this.controlConversacion1);
            this.pConversacion.Controls.Add(this.textoDecir);
            this.pConversacion.Location = new System.Drawing.Point(136, 63);
            this.pConversacion.Name = "pConversacion";
            this.pConversacion.Size = new System.Drawing.Size(799, 375);
            this.pConversacion.TabIndex = 25;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblInfoMensaje);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.comboRedes);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 175);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(797, 29);
            this.panel4.TabIndex = 44;
            // 
            // lblInfoMensaje
            // 
            this.lblInfoMensaje.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfoMensaje.Location = new System.Drawing.Point(0, 0);
            this.lblInfoMensaje.MaximumSize = new System.Drawing.Size(0, 20);
            this.lblInfoMensaje.MinimumSize = new System.Drawing.Size(0, 20);
            this.lblInfoMensaje.Name = "lblInfoMensaje";
            this.lblInfoMensaje.Size = new System.Drawing.Size(447, 20);
            this.lblInfoMensaje.TabIndex = 48;
            this.lblInfoMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Right;
            this.label1.Location = new System.Drawing.Point(447, 0);
            this.label1.MaximumSize = new System.Drawing.Size(100, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 46;
            this.label1.Text = "Hablar Por";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboRedes
            // 
            this.comboRedes.Dock = System.Windows.Forms.DockStyle.Right;
            this.comboRedes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRedes.FormattingEnabled = true;
            this.comboRedes.ItemHeight = 13;
            this.comboRedes.Location = new System.Drawing.Point(547, 0);
            this.comboRedes.MaximumSize = new System.Drawing.Size(250, 0);
            this.comboRedes.MinimumSize = new System.Drawing.Size(250, 0);
            this.comboRedes.Name = "comboRedes";
            this.comboRedes.Size = new System.Drawing.Size(250, 21);
            this.comboRedes.TabIndex = 45;
            this.comboRedes.SelectedIndexChanged += new System.EventHandler(this.comboRedes_SelectedIndexChanged);
            // 
            // controlConversacion1
            // 
            this.controlConversacion1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlConversacion1.IdVentana = null;
            this.controlConversacion1.Location = new System.Drawing.Point(0, 0);
            this.controlConversacion1.Name = "controlConversacion1";
            this.controlConversacion1.Size = new System.Drawing.Size(797, 172);
            this.controlConversacion1.TabIndex = 19;
            // 
            // textoDecir
            // 
            this.textoDecir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textoDecir.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textoDecir.Location = new System.Drawing.Point(0, 204);
            this.textoDecir.MinimumSize = new System.Drawing.Size(20, 20);
            this.textoDecir.Name = "textoDecir";
            this.textoDecir.Size = new System.Drawing.Size(797, 169);
            this.textoDecir.TabIndex = 17;
            this.textoDecir.Text = "";
            this.textoDecir.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textoDecir_KeyUp);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "codochaticon.ico");
            this.imageList1.Images.SetKeyName(1, "facebookicon.png");
            this.imageList1.Images.SetKeyName(2, "gtalkicon.png");
            // 
            // VentanaChat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(935, 499);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VentanaChat";
            this.Activated += new System.EventHandler(this.VentanaChat_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VentanaChatCerrando);
            this.VisibleChanged += new System.EventHandler(this.VentanaChat_VisibleChanged);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.VentanaChat_KeyUp);
            this.Leave += new System.EventHandler(this.VentanaChat_Leave);
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.pNickYSubnick.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pConversacion.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel3;
		private Forms.ControlImagenPerfil imagenContacto;
        private Forms.ControlImagenPerfil imagenPropia;
        private System.Windows.Forms.Panel pConversacion;
        private System.Windows.Forms.RichTextBox textoDecir;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Button btnFuente;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pNickYSubnick;
        private System.Windows.Forms.RichTextBox rtbSubnick;
        private System.Windows.Forms.RichTextBox rtbNick;
        private Forms.ControlConversacion controlConversacion1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolTip toolTipRed;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.ComboBox comboRedes;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblInfoMensaje;

    }
}