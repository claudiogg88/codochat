﻿namespace CodoChat.Forms
{
	partial class FormSeleccionarImagen
	{
		/// <summary>
		/// Variable del diseñador requerida.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén utilizando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido del método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSeleccionarImagen));
            this.pContenedor = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.imagenPerfil1 = new CodoChat.Forms.ControlImagenPerfil();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.pImagenCompleta = new System.Windows.Forms.Panel();
            this.pbCompleto = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pContenedor.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pImagenCompleta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCompleto)).BeginInit();
            this.SuspendLayout();
            // 
            // pContenedor
            // 
            this.pContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenedor.Controls.Add(this.panel2);
            this.pContenedor.Controls.Add(this.pImagenCompleta);
            this.pContenedor.Location = new System.Drawing.Point(0, 1);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(852, 642);
            this.pContenedor.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.imagenPerfil1);
            this.panel2.Controls.Add(this.btnAceptar);
            this.panel2.Controls.Add(this.btnExaminar);
            this.panel2.Location = new System.Drawing.Point(3, 480);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(846, 159);
            this.panel2.TabIndex = 4;
            // 
            // imagenPerfil1
            // 
            this.imagenPerfil1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.imagenPerfil1.ImagenEstado = ((System.Drawing.Image)(resources.GetObject("imagenPerfil1.Borde")));
            this.imagenPerfil1.Imagen = ((System.Drawing.Image)(resources.GetObject("imagenPerfil1.Imagen")));
            this.imagenPerfil1.Location = new System.Drawing.Point(1, 4);
            this.imagenPerfil1.Name = "imagenPerfil1";
            this.imagenPerfil1.Size = new System.Drawing.Size(123, 122);
            this.imagenPerfil1.TabIndex = 6;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnAceptar.Location = new System.Drawing.Point(273, 51);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(235, 44);
            this.btnAceptar.TabIndex = 5;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnExaminar
            // 
            this.btnExaminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnExaminar.Location = new System.Drawing.Point(273, 3);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(235, 42);
            this.btnExaminar.TabIndex = 4;
            this.btnExaminar.Text = "Examinar";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // pImagenCompleta
            // 
            this.pImagenCompleta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pImagenCompleta.AutoScroll = true;
            this.pImagenCompleta.Controls.Add(this.pbCompleto);
            this.pImagenCompleta.Location = new System.Drawing.Point(0, 0);
            this.pImagenCompleta.Name = "pImagenCompleta";
            this.pImagenCompleta.Size = new System.Drawing.Size(849, 477);
            this.pImagenCompleta.TabIndex = 3;
            // 
            // pbCompleto
            // 
            this.pbCompleto.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pbCompleto.Location = new System.Drawing.Point(1, 3);
            this.pbCompleto.Name = "pbCompleto";
            this.pbCompleto.Size = new System.Drawing.Size(861, 467);
            this.pbCompleto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbCompleto.TabIndex = 1;
            this.pbCompleto.TabStop = false;
            this.pbCompleto.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pbCompleto.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pbCompleto.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pbCompleto.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormSeleccionarImagen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 642);
            this.Controls.Add(this.pContenedor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormSeleccionarImagen";
            this.Text = "Seleccionar Imagen";
            this.pContenedor.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pImagenCompleta.ResumeLayout(false);
            this.pImagenCompleta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCompleto)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pContenedor;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Panel pImagenCompleta;
		private System.Windows.Forms.PictureBox pbCompleto;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btnAceptar;
		private System.Windows.Forms.Button btnExaminar;
		private ControlImagenPerfil imagenPerfil1;
	}
}