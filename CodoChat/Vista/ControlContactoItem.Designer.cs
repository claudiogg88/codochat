﻿namespace CodoChat.Forms
{
	partial class ControlContactoItem
	{
		/// <summary> 
		/// Variable del diseñador requerida.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Limpiar los recursos que se estén utilizando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de componentes

		/// <summary> 
		/// Método necesario para admitir el Diseñador. No se puede modificar 
		/// el contenido del método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.pContenedor = new System.Windows.Forms.Panel();
            this.pNickYSubnick = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.flowLPLogos = new System.Windows.Forms.FlowLayoutPanel();
            this.lblCodoChatIcon = new System.Windows.Forms.Label();
            this.lblGTalkIcon = new System.Windows.Forms.Label();
            this.lblFacebookIcon = new System.Windows.Forms.Label();
            this.Subnick = new System.Windows.Forms.RichTextBox();
            this.Nick = new System.Windows.Forms.RichTextBox();
            this.pImagenPerfil = new System.Windows.Forms.Panel();
            this.imagenPerfil1 = new CodoChat.Forms.ControlImagenPerfilChica();
            this.menuItemContacto = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.moverAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoGrupoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.copiarAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pContenedor.SuspendLayout();
            this.pNickYSubnick.SuspendLayout();
            this.panel2.SuspendLayout();
            this.flowLPLogos.SuspendLayout();
            this.pImagenPerfil.SuspendLayout();
            this.menuItemContacto.SuspendLayout();
            this.SuspendLayout();
            // 
            // pContenedor
            // 
            this.pContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenedor.Controls.Add(this.pNickYSubnick);
            this.pContenedor.Controls.Add(this.pImagenPerfil);
            this.pContenedor.Location = new System.Drawing.Point(-1, 0);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(800, 86);
            this.pContenedor.TabIndex = 0;
            // 
            // pNickYSubnick
            // 
            this.pNickYSubnick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pNickYSubnick.Controls.Add(this.panel2);
            this.pNickYSubnick.Controls.Add(this.Nick);
            this.pNickYSubnick.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.pNickYSubnick.Location = new System.Drawing.Point(114, 0);
            this.pNickYSubnick.Name = "pNickYSubnick";
            this.pNickYSubnick.Size = new System.Drawing.Size(686, 86);
            this.pNickYSubnick.TabIndex = 17;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.flowLPLogos);
            this.panel2.Controls.Add(this.Subnick);
            this.panel2.Location = new System.Drawing.Point(3, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(683, 32);
            this.panel2.TabIndex = 26;
            // 
            // flowLPLogos
            // 
            this.flowLPLogos.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.flowLPLogos.Controls.Add(this.lblCodoChatIcon);
            this.flowLPLogos.Controls.Add(this.lblGTalkIcon);
            this.flowLPLogos.Controls.Add(this.lblFacebookIcon);
            this.flowLPLogos.Location = new System.Drawing.Point(0, 0);
            this.flowLPLogos.Name = "flowLPLogos";
            this.flowLPLogos.Size = new System.Drawing.Size(118, 32);
            this.flowLPLogos.TabIndex = 32;
            // 
            // lblCodoChatIcon
            // 
            this.lblCodoChatIcon.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCodoChatIcon.Image = global::CodoChat.Properties.Resources.codochaticon;
            this.lblCodoChatIcon.Location = new System.Drawing.Point(3, 0);
            this.lblCodoChatIcon.Name = "lblCodoChatIcon";
            this.lblCodoChatIcon.Size = new System.Drawing.Size(32, 32);
            this.lblCodoChatIcon.TabIndex = 34;
            this.lblCodoChatIcon.Visible = false;
            // 
            // lblGTalkIcon
            // 
            this.lblGTalkIcon.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblGTalkIcon.Image = global::CodoChat.Properties.Resources.gtalkicon;
            this.lblGTalkIcon.Location = new System.Drawing.Point(41, 0);
            this.lblGTalkIcon.Name = "lblGTalkIcon";
            this.lblGTalkIcon.Size = new System.Drawing.Size(32, 32);
            this.lblGTalkIcon.TabIndex = 33;
            this.lblGTalkIcon.Visible = false;
            // 
            // lblFacebookIcon
            // 
            this.lblFacebookIcon.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFacebookIcon.Image = global::CodoChat.Properties.Resources.facebookicon;
            this.lblFacebookIcon.Location = new System.Drawing.Point(79, 0);
            this.lblFacebookIcon.Name = "lblFacebookIcon";
            this.lblFacebookIcon.Size = new System.Drawing.Size(32, 32);
            this.lblFacebookIcon.TabIndex = 32;
            this.lblFacebookIcon.Visible = false;
            // 
            // Subnick
            // 
            this.Subnick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Subnick.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Subnick.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Subnick.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Subnick.Location = new System.Drawing.Point(124, 0);
            this.Subnick.Multiline = false;
            this.Subnick.Name = "Subnick";
            this.Subnick.ReadOnly = true;
            this.Subnick.Size = new System.Drawing.Size(560, 29);
            this.Subnick.TabIndex = 29;
            this.Subnick.Text = "";
            // 
            // Nick
            // 
            this.Nick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Nick.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Nick.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Nick.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nick.Location = new System.Drawing.Point(3, 15);
            this.Nick.Multiline = false;
            this.Nick.Name = "Nick";
            this.Nick.ReadOnly = true;
            this.Nick.Size = new System.Drawing.Size(683, 29);
            this.Nick.TabIndex = 25;
            this.Nick.Text = "";
            // 
            // pImagenPerfil
            // 
            this.pImagenPerfil.Controls.Add(this.imagenPerfil1);
            this.pImagenPerfil.Location = new System.Drawing.Point(3, 0);
            this.pImagenPerfil.Name = "pImagenPerfil";
            this.pImagenPerfil.Size = new System.Drawing.Size(108, 86);
            this.pImagenPerfil.TabIndex = 16;
            // 
            // imagenPerfil1
            // 
            this.imagenPerfil1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.imagenPerfil1.Imagen = global::CodoChat.Properties.Resources.imagen_default;
            this.imagenPerfil1.ImagenEstado = global::CodoChat.Properties.Resources.desconectado;
            this.imagenPerfil1.Location = new System.Drawing.Point(15, 5);
            this.imagenPerfil1.Name = "imagenPerfil1";
            this.imagenPerfil1.Size = new System.Drawing.Size(80, 73);
            this.imagenPerfil1.TabIndex = 0;
            // 
            // menuItemContacto
            // 
            this.menuItemContacto.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moverAToolStripMenuItem,
            this.copiarAToolStripMenuItem});
            this.menuItemContacto.Name = "contextMenuStrip1";
            this.menuItemContacto.Size = new System.Drawing.Size(121, 48);
            // 
            // moverAToolStripMenuItem
            // 
            this.moverAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoGrupoToolStripMenuItem,
            this.toolStripSeparator1});
            this.moverAToolStripMenuItem.Name = "moverAToolStripMenuItem";
            this.moverAToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.moverAToolStripMenuItem.Text = "Mover A";
            // 
            // nuevoGrupoToolStripMenuItem
            // 
            this.nuevoGrupoToolStripMenuItem.Name = "nuevoGrupoToolStripMenuItem";
            this.nuevoGrupoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.nuevoGrupoToolStripMenuItem.Text = "Nuevo Grupo";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // copiarAToolStripMenuItem
            // 
            this.copiarAToolStripMenuItem.Name = "copiarAToolStripMenuItem";
            this.copiarAToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.copiarAToolStripMenuItem.Text = "Copiar A";
            // 
            // ControlContactoItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ContextMenuStrip = this.menuItemContacto;
            this.Controls.Add(this.pContenedor);
            this.DoubleBuffered = true;
            this.Name = "ControlContactoItem";
            this.Size = new System.Drawing.Size(798, 86);
            this.Load += new System.EventHandler(this.ContactoItem_Load);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ContactoItem_MouseDoubleClick);
            this.pContenedor.ResumeLayout(false);
            this.pNickYSubnick.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.flowLPLogos.ResumeLayout(false);
            this.pImagenPerfil.ResumeLayout(false);
            this.menuItemContacto.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pContenedor;
		private System.Windows.Forms.Panel pImagenPerfil;
		private ControlImagenPerfilChica imagenPerfil1;
        private System.Windows.Forms.Panel pNickYSubnick;
        private System.Windows.Forms.RichTextBox Nick;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox Subnick;
        private System.Windows.Forms.FlowLayoutPanel flowLPLogos;
        private System.Windows.Forms.Label lblGTalkIcon;
        private System.Windows.Forms.Label lblFacebookIcon;
        private System.Windows.Forms.ContextMenuStrip menuItemContacto;
        private System.Windows.Forms.Label lblCodoChatIcon;
        private System.Windows.Forms.ToolStripMenuItem moverAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoGrupoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem copiarAToolStripMenuItem;





	}
}
