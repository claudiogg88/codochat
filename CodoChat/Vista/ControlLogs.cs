﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Modelo;
using CodoChat.ModeloVista;

namespace CodoChat.Forms
{
    public partial class ControlLogs : UserControl
    {
        ModeloControlLogs modelo;
        public ControlLogs()
        {
            
            InitializeComponent();
            this.modelo = new ModeloControlLogs(this);
        }
        public void Actualizar()
        {
            if (modelo != null)
            {
                modelo.ActualizarControlLogs();
            }
        }

        private void limpiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.LimpiarLogs();
        }


        private void txbLogs_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Actualizar();
            }
        }


        private void scrollLockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.ScrollLock = !modelo.ScrollLock;
            this.scrollLockToolStripMenuItem.Checked = modelo.ScrollLock;
            this.scrollLockToolStripMenuItem.CheckState = modelo.ScrollLock ? System.Windows.Forms.CheckState.Checked : System.Windows.Forms.CheckState.Unchecked; 
        }
    }
}
