﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Forms;
using CodoChat.ModeloVista;

namespace CodoChat.Forms
{
    public partial class ControlListaContactos : UserControl
    {
        ModeloControlListaContactos modelo;
        public ControlListaContactos()
        {
          
            InitializeComponent();
            this.modelo = new ModeloControlListaContactos(this); ;
            this.MouseWheel += new MouseEventHandler(Panel1_MouseWheel);
        }
        public void Actualizar()
        {
            modelo.ActualizarListaContactos();
            foreach (ControlGrupoContactos grupo in this.FLPGrupos.Controls)
            {
                grupo.Actualizar();
            }
        }

      
        internal void Filtrar(string texto)
        {
            foreach (ControlGrupoContactos grupoContactos in this.FLPGrupos.Controls)
            {
                grupoContactos.SuspendLayout();
                grupoContactos.Filtrar(texto);
                grupoContactos.ResumeLayout();
            }
        }

        /// <summary>
        /// Para que asegurar que se ensanchen los item contactos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FPLGrupos_Resize(object sender, EventArgs e)
        {
            foreach (Control grupo in FLPGrupos.Controls)
            {
                grupo.Size = new Size(FLPGrupos.Size.Width, grupo.Height);
            }
        }

        /// <summary>
        /// Para asegurar que se ensanche la lista de contactos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FPLGrupos_ControlAdded(object sender, ControlEventArgs e)
        {
            Control grupo = e.Control;
            grupo.Size = new Size(FLPGrupos.Size.Width, grupo.Height);
            addOnMouseEnter(grupo);
        }


        /// <summary>
        /// para habilitar el scroll weel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FLPGrupos_MouseEnter(object sender, EventArgs e)
        {
            addOnMouseEnter(this);
        }
        public Boolean TopMost;
        private void addOnMouseEnter(Control control)
        {
            foreach (Control child in control.Controls)
            {
                addOnMouseEnter(child);
            }
            control.MouseEnter += new EventHandler(control_MouseEnter);
        }

        void control_MouseEnter(object sender, EventArgs e)
        {
            //si el flpgrupos no tiene foco no scrollea
            //asi que le metemos foco a el y a sus elementos
            //pero por ahi hay otra ventana arriba y se focusea el hdp
            //asi q antes hay q verificar si no hay otra ventana arriba 
            if (GUI.VentanaPrincipal == Form.ActiveForm)
                this.FLPGrupos.Focus();
        }


        private void Panel1_MouseWheel(object sender, MouseEventArgs e)
        {
            this.FLPGrupos.Focus();
        }


    }
}
