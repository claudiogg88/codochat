﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using CodoChat.Forms;
using CodoChat.Modelo;
using System.Media;
using CodoChat.ModeloVista;

namespace CodoChat
{
    class GUI
    {

        public static VentanaPrincipal VentanaPrincipal;
        public static FormConfiguracion FormConfiguracion;
        public static Dictionary<string, VentanaChat> VentanasChat;
        public static FormEmoticones FormEmoticones;

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<VENTANACHAT<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<	

        public static VentanaChat ObtenerVentanaChat(string idVentanaChat)
        {
            VentanaChat salida;
            VentanasChat.TryGetValue(idVentanaChat, out salida);
            return salida;
        }
        public static VentanaChat CrearVentanaChat(String idVentanaChat)
        {
            ContactoColor receptor = Repositorio.ContactoColorCol[idVentanaChat];
            VentanaChat chat = new VentanaChat(receptor);
            VentanasChat.Add(idVentanaChat, chat);
            return chat;
        }




        public static void CerrarCodoChat()
        {
            MainColorMSN.Cerrar();
            Cerrar();
        }


        public static void Iniciar()
        {
            VentanaPrincipal = new VentanaPrincipal();
            VentanasChat = new Dictionary<string, VentanaChat>();
            FormEmoticones = new FormEmoticones();
            FormConfiguracion = new FormConfiguracion();
        }
        public static void Run()
        {
            Application.Run(VentanaPrincipal);
        }
        public static void Cerrar()
        {
            foreach (VentanaChat chat in VentanasChat.Values)
            {
                chat.Dispose();
            }
            FormEmoticones.Dispose();
            VentanaPrincipal.Dispose();
        }



        public static void ContactoDebeRecibirMensaje(String clave, String mensaje)
        {
            VentanaChat ventana = ObtenerVentanaChat(clave);
            ventana.AgregarMensajePropio(mensaje);

        }



        public static void ContactoEnvioMensaje(String clave, String mensaje)
        {

            VentanaChat ventana = ObtenerVentanaChat(clave);

            if (ventana == null)
            {

                VentanaPrincipal.UIThreadInvoke(delegate
                {

                    ventana = CrearVentanaChat(clave);
                    ventana.AgregarMensajeDeContacto(mensaje);
                    ventana.Show();
                });

            }
            else
            {
                ventana.UIThreadInvoke(delegate
                {
                    ventana.AgregarMensajeDeContacto(mensaje);
                    ventana.Show();
                });
            }
        }
        

        internal static void TitilarVentana(string claveContacto)
        {
            VentanaChat ventana = ObtenerVentanaChat(claveContacto);

            if (ventana != null)
            {
                ventana.Titilar();
            }
        }




        static Timer Actualizador;
        internal static void IniciarActualizaciones()
        {
            Actualizador = new System.Windows.Forms.Timer();
            Actualizador.Tick += new EventHandler(ActualizarVentanas);
            Actualizador.Interval = (1000) * (1); //enviar datos
            Actualizador.Enabled = true;
            Actualizador.Start();
            ActualizarVentanas(null, null);
        }

        private static void ActualizarVentanas(object sender, EventArgs e)
        {
            VentanaPrincipal.Actualizar();
            FormConfiguracion.Actualizar();
            FormEmoticones.Actualizar();
            foreach(VentanaChat ventana in VentanasChat.Values)
            {
                ventana.Actualizar();
            }
        }
    }
}
