﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Modelo;

namespace CodoChat
{
    public partial class FormGrupo : Form
    {
		public String NombreGrupo;
        public String Orden;
		public bool Aceptado;
        public FormGrupo()
        {
			Aceptado = false;
            InitializeComponent();

        }

		private void button1_Click(object sender, EventArgs e)
		{
            Aceptar();
		}

        private void Aceptar()
        {
            NombreGrupo = this.txtNombre.Text;
            Orden = this.txtOrden.Text;
            Aceptado = true;
            this.Hide();
        }
        private void FormGrupo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Aceptar();
            }
          
        }

        private void txtOrden_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Aceptar();
            }
        }

        private void txtNombre_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Aceptar();
            }
        }

        private void btnAceptar_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Aceptar();
            }
        }


        		
	}
}
