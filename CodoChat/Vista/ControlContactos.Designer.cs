﻿namespace CodoChat.Forms
{
    partial class ControlContactos
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlContactos));
            this.pContenedor = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnRemover = new System.Windows.Forms.Button();
            this.btnEliminarGrupo = new System.Windows.Forms.Button();
            this.btnOcultarContactos = new System.Windows.Forms.Button();
            this.btnEditarGrupo = new System.Windows.Forms.Button();
            this.btnNuevoGrupo = new System.Windows.Forms.Button();
            this.btnSeparar = new System.Windows.Forms.Button();
            this.btnFusionar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmsListaContactos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copiarAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moverAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tlsRemoverDeGrupo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fusionarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ocultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbGrupos = new System.Windows.Forms.ComboBox();
            this.cmsComboGrupos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.nuevoGrupoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarGrupoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarGrupoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.iconosRedes = new System.Windows.Forms.ImageList(this.components);
            this.lstContactos = new System.Windows.Forms.ListView();
            this.columnaNombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnaRedes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pContenedor.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.cmsListaContactos.SuspendLayout();
            this.cmsComboGrupos.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pContenedor
            // 
            this.pContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenedor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pContenedor.Controls.Add(this.panel3);
            this.pContenedor.Controls.Add(this.panel2);
            this.pContenedor.Controls.Add(this.panel1);
            this.pContenedor.Location = new System.Drawing.Point(3, 3);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(891, 533);
            this.pContenedor.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnRemover);
            this.panel3.Controls.Add(this.btnEliminarGrupo);
            this.panel3.Controls.Add(this.btnOcultarContactos);
            this.panel3.Controls.Add(this.btnEditarGrupo);
            this.panel3.Controls.Add(this.btnNuevoGrupo);
            this.panel3.Controls.Add(this.btnSeparar);
            this.panel3.Controls.Add(this.btnFusionar);
            this.panel3.Location = new System.Drawing.Point(689, 43);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(168, 217);
            this.panel3.TabIndex = 2;
            // 
            // btnRemover
            // 
            this.btnRemover.Location = new System.Drawing.Point(1, 60);
            this.btnRemover.Name = "btnRemover";
            this.btnRemover.Size = new System.Drawing.Size(165, 31);
            this.btnRemover.TabIndex = 7;
            this.btnRemover.Text = "Remover De Grupo ";
            this.btnRemover.UseVisualStyleBackColor = true;
            this.btnRemover.Click += new System.EventHandler(this.btnRemover_Click);
            // 
            // btnEliminarGrupo
            // 
            this.btnEliminarGrupo.Location = new System.Drawing.Point(1, 180);
            this.btnEliminarGrupo.Name = "btnEliminarGrupo";
            this.btnEliminarGrupo.Size = new System.Drawing.Size(165, 31);
            this.btnEliminarGrupo.TabIndex = 6;
            this.btnEliminarGrupo.Text = "Eliminar Grupo";
            this.btnEliminarGrupo.UseVisualStyleBackColor = true;
            this.btnEliminarGrupo.Click += new System.EventHandler(this.btnEliminarGrupo_Click);
            // 
            // btnOcultarContactos
            // 
            this.btnOcultarContactos.Location = new System.Drawing.Point(1, 90);
            this.btnOcultarContactos.Name = "btnOcultarContactos";
            this.btnOcultarContactos.Size = new System.Drawing.Size(165, 31);
            this.btnOcultarContactos.TabIndex = 5;
            this.btnOcultarContactos.Text = "Ocultar Contactos";
            this.btnOcultarContactos.UseVisualStyleBackColor = true;
            this.btnOcultarContactos.Click += new System.EventHandler(this.btnOcultarContactos_Click);
            // 
            // btnEditarGrupo
            // 
            this.btnEditarGrupo.Location = new System.Drawing.Point(1, 150);
            this.btnEditarGrupo.Name = "btnEditarGrupo";
            this.btnEditarGrupo.Size = new System.Drawing.Size(165, 31);
            this.btnEditarGrupo.TabIndex = 3;
            this.btnEditarGrupo.Text = "Editar Grupo";
            this.btnEditarGrupo.UseVisualStyleBackColor = true;
            this.btnEditarGrupo.Click += new System.EventHandler(this.btnEditarGrupo_Click);
            // 
            // btnNuevoGrupo
            // 
            this.btnNuevoGrupo.Location = new System.Drawing.Point(1, 120);
            this.btnNuevoGrupo.Name = "btnNuevoGrupo";
            this.btnNuevoGrupo.Size = new System.Drawing.Size(165, 31);
            this.btnNuevoGrupo.TabIndex = 2;
            this.btnNuevoGrupo.Text = "Nuevo Grupo";
            this.btnNuevoGrupo.UseVisualStyleBackColor = true;
            this.btnNuevoGrupo.Click += new System.EventHandler(this.btnNuevoGrupo_Click);
            // 
            // btnSeparar
            // 
            this.btnSeparar.Location = new System.Drawing.Point(1, 30);
            this.btnSeparar.Name = "btnSeparar";
            this.btnSeparar.Size = new System.Drawing.Size(165, 31);
            this.btnSeparar.TabIndex = 1;
            this.btnSeparar.Text = "Separar Contactos";
            this.btnSeparar.UseVisualStyleBackColor = true;
            this.btnSeparar.Click += new System.EventHandler(this.btnSeparar_Click);
            // 
            // btnFusionar
            // 
            this.btnFusionar.Location = new System.Drawing.Point(1, 0);
            this.btnFusionar.Name = "btnFusionar";
            this.btnFusionar.Size = new System.Drawing.Size(165, 31);
            this.btnFusionar.TabIndex = 0;
            this.btnFusionar.Text = "Fusionar Contactos";
            this.btnFusionar.UseVisualStyleBackColor = true;
            this.btnFusionar.Click += new System.EventHandler(this.btnFusionar_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.Controls.Add(this.lstContactos);
            this.panel2.Controls.Add(this.cmbGrupos);
            this.panel2.Location = new System.Drawing.Point(3, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(681, 487);
            this.panel2.TabIndex = 1;
            // 
            // cmsListaContactos
            // 
            this.cmsListaContactos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copiarAToolStripMenuItem,
            this.moverAToolStripMenuItem,
            this.tlsRemoverDeGrupo,
            this.toolStripSeparator1,
            this.fusionarToolStripMenuItem,
            this.separarToolStripMenuItem,
            this.ocultarToolStripMenuItem});
            this.cmsListaContactos.Name = "cmsListaContactos";
            this.cmsListaContactos.Size = new System.Drawing.Size(175, 142);
            // 
            // copiarAToolStripMenuItem
            // 
            this.copiarAToolStripMenuItem.Name = "copiarAToolStripMenuItem";
            this.copiarAToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.copiarAToolStripMenuItem.Text = "Copiar A";
            this.copiarAToolStripMenuItem.Click += new System.EventHandler(this.copiarAToolStripMenuItem_Click);
            // 
            // moverAToolStripMenuItem
            // 
            this.moverAToolStripMenuItem.Name = "moverAToolStripMenuItem";
            this.moverAToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.moverAToolStripMenuItem.Text = "Mover A";
            this.moverAToolStripMenuItem.Click += new System.EventHandler(this.moverAToolStripMenuItem_Click);
            // 
            // tlsRemoverDeGrupo
            // 
            this.tlsRemoverDeGrupo.Name = "tlsRemoverDeGrupo";
            this.tlsRemoverDeGrupo.Size = new System.Drawing.Size(174, 22);
            this.tlsRemoverDeGrupo.Text = "Remover De Grupo";
            this.tlsRemoverDeGrupo.Click += new System.EventHandler(this.removertoolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(171, 6);
            // 
            // fusionarToolStripMenuItem
            // 
            this.fusionarToolStripMenuItem.Name = "fusionarToolStripMenuItem";
            this.fusionarToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.fusionarToolStripMenuItem.Text = "Fusionar";
            this.fusionarToolStripMenuItem.Click += new System.EventHandler(this.fusionarToolStripMenuItem_Click);
            // 
            // separarToolStripMenuItem
            // 
            this.separarToolStripMenuItem.Name = "separarToolStripMenuItem";
            this.separarToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.separarToolStripMenuItem.Text = "Separar";
            this.separarToolStripMenuItem.Click += new System.EventHandler(this.separarToolStripMenuItem_Click);
            // 
            // ocultarToolStripMenuItem
            // 
            this.ocultarToolStripMenuItem.Name = "ocultarToolStripMenuItem";
            this.ocultarToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.ocultarToolStripMenuItem.Text = "Ocultar";
            this.ocultarToolStripMenuItem.Click += new System.EventHandler(this.ocultarToolStripMenuItem_Click);
            // 
            // cmbGrupos
            // 
            this.cmbGrupos.ContextMenuStrip = this.cmsComboGrupos;
            this.cmbGrupos.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbGrupos.FormattingEnabled = true;
            this.cmbGrupos.Location = new System.Drawing.Point(0, 0);
            this.cmbGrupos.Name = "cmbGrupos";
            this.cmbGrupos.Size = new System.Drawing.Size(681, 21);
            this.cmbGrupos.TabIndex = 0;
            this.cmbGrupos.SelectedIndexChanged += new System.EventHandler(this.cmbGrupos_SelectedIndexChanged);
            // 
            // cmsComboGrupos
            // 
            this.cmsComboGrupos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoGrupoToolStripMenuItem,
            this.editarGrupoToolStripMenuItem,
            this.eliminarGrupoToolStripMenuItem});
            this.cmsComboGrupos.Name = "cmsComboGrupos";
            this.cmsComboGrupos.Size = new System.Drawing.Size(154, 70);
            // 
            // nuevoGrupoToolStripMenuItem
            // 
            this.nuevoGrupoToolStripMenuItem.Name = "nuevoGrupoToolStripMenuItem";
            this.nuevoGrupoToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.nuevoGrupoToolStripMenuItem.Text = "Nuevo Grupo";
            this.nuevoGrupoToolStripMenuItem.Click += new System.EventHandler(this.nuevoGrupoToolStripMenuItem_Click);
            // 
            // editarGrupoToolStripMenuItem
            // 
            this.editarGrupoToolStripMenuItem.Name = "editarGrupoToolStripMenuItem";
            this.editarGrupoToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.editarGrupoToolStripMenuItem.Text = "Editar Grupo";
            this.editarGrupoToolStripMenuItem.Click += new System.EventHandler(this.editarGrupoToolStripMenuItem_Click);
            // 
            // eliminarGrupoToolStripMenuItem
            // 
            this.eliminarGrupoToolStripMenuItem.Name = "eliminarGrupoToolStripMenuItem";
            this.eliminarGrupoToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.eliminarGrupoToolStripMenuItem.Text = "Eliminar Grupo";
            this.eliminarGrupoToolStripMenuItem.Click += new System.EventHandler(this.eliminarGrupoToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(885, 34);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(879, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Contactos";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // iconosRedes
            // 
            this.iconosRedes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iconosRedes.ImageStream")));
            this.iconosRedes.TransparentColor = System.Drawing.Color.Transparent;
            this.iconosRedes.Images.SetKeyName(0, "codochaticon.ico");
            this.iconosRedes.Images.SetKeyName(1, "facebookicon.png");
            this.iconosRedes.Images.SetKeyName(2, "gtalkicon.png");
            // 
            // lstContactos
            // 
            this.lstContactos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnaNombre,
            this.columnaRedes});
            this.lstContactos.ContextMenuStrip = this.cmsListaContactos;
            this.lstContactos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstContactos.Location = new System.Drawing.Point(0, 21);
            this.lstContactos.Name = "lstContactos";
            this.lstContactos.ShowItemToolTips = true;
            this.lstContactos.Size = new System.Drawing.Size(681, 466);
            this.lstContactos.TabIndex = 1;
            this.lstContactos.UseCompatibleStateImageBehavior = false;
            this.lstContactos.View = System.Windows.Forms.View.Details;
            this.lstContactos.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstContactos_ColumnClick);
            this.lstContactos.SelectedIndexChanged += new System.EventHandler(this.lstContactos_SelectedIndexChanged);
            // 
            // columnaNombre
            // 
            this.columnaNombre.Text = "Nombre";
            this.columnaNombre.Width = 200;
            // 
            // columnaRedes
            // 
            this.columnaRedes.Text = "Redes";
            this.columnaRedes.Width = 200;
            // 
            // ControlContactos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pContenedor);
            this.Name = "ControlContactos";
            this.Size = new System.Drawing.Size(897, 539);
            this.pContenedor.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.cmsListaContactos.ResumeLayout(false);
            this.cmsComboGrupos.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pContenedor;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.ComboBox cmbGrupos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip cmsComboGrupos;
        private System.Windows.Forms.ToolStripMenuItem nuevoGrupoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarGrupoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarGrupoToolStripMenuItem;
        public System.Windows.Forms.ImageList iconosRedes;
        public System.Windows.Forms.ToolStripMenuItem moverAToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem copiarAToolStripMenuItem;
        public System.Windows.Forms.ContextMenuStrip cmsListaContactos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.ToolStripMenuItem fusionarToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem separarToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem ocultarToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem tlsRemoverDeGrupo;
        public System.Windows.Forms.Button btnEditarGrupo;
        public System.Windows.Forms.Button btnNuevoGrupo;
        public System.Windows.Forms.Button btnSeparar;
        public System.Windows.Forms.Button btnOcultarContactos;
        public System.Windows.Forms.Button btnEliminarGrupo;
        public System.Windows.Forms.Button btnRemover;
        private System.Windows.Forms.Button btnFusionar;
        public System.Windows.Forms.ListView lstContactos;
        public System.Windows.Forms.ColumnHeader columnaNombre;
        public System.Windows.Forms.ColumnHeader columnaRedes;

    }
}
