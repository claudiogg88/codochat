﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Forms;
using CodoChat.Modelo;
using CodoChat.ModeloVista;

namespace CodoChat
{
    public partial class VentanaPrincipal : Form
    {
        ModeloVentanaPrincipal modelo;

        public VentanaPrincipal()
        {

            this.modelo = new ModeloVentanaPrincipal(this);
            InitializeComponent();
        }
        public void Actualizar()
        {
            modelo.ActualizarFormPrincipal();
            this.listaContactos.Actualizar();
        }
        public Image ImagenPerfil
        {
            get
            {
                return this.imagenPerfil.Imagen;
            }
            set
            {
                this.imagenPerfil.Imagen = value;
            }
        }
        public Image ImagenEstado
        {
            get
            {
                return this.imagenPerfil.ImagenEstado;
            }
            set
            {
                this.imagenPerfil.ImagenEstado = value;
            }
        }
        public String Nick
        {
            get
            {
                return this.rtbNick.Rtf;
            }
            set
            {
                this.rtbNick.Rtf = value;
            }
        }
        public String SubNick
        {
            get
            {
                return this.rtbSubnick.Rtf;
            }
            set
            {
                this.rtbSubnick.Rtf = value;
            }
        }

        private void txtFiltroContacto_TextChanged(object sender, EventArgs e)
        {
            this.SuspendLayout();
            this.listaContactos.Filtrar(((TextBox)sender).Text);
            this.ResumeLayout();
        }


        private void VentanaPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            modelo.FormCerrandose(e);
        }


        private void perfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.MenuPerfiles((ToolStripMenuItem)sender);
        }

        private void logsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            modelo.MenuLogs((ToolStripMenuItem)sender);
        }

        private void cuentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.MenuCuentas( (ToolStripMenuItem)sender);

        }

        private void personalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.MenuPersonal((ToolStripMenuItem)sender);

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.MenuCerrar();
        }

        private void trayCodoChat_MouseDoubleClick(object sender, EventArgs e)
        {
            modelo.TryIconDobleClick();

        }

        private void cerrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            modelo.MenuCerrar();
        }


        private void VentanaPrincipal_Load(object sender, EventArgs e)
        {
            GUI.IniciarActualizaciones();
        }

        private void contactosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.MenuContactos((ToolStripMenuItem)sender);
        }


    }
}
