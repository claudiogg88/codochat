﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Modelo;
using System.Diagnostics;
using CodoChat.ModeloVista;

namespace CodoChat
{

    public partial class VentanaChat : Form
    {
        public string IdVentana { get; private set; }
        public ModeloVentanaChat modelo;
        public int ContadorMensajesNoLeidos = 0;

        public VentanaChat(ContactoColor receptor)
        {
            this.modelo = new ModeloVentanaChat(this, receptor);
            this.IdVentana = receptor.IdContactoColor;

            InitializeComponent();
            //this.controlConversacion1 = new CodoChat.Forms.ControlConversacion(Repositorio.ContactoColorCol[this.IdVentana]);
            this.controlConversacion1.IdVentana = receptor.IdContactoColor;

        }


        public void Actualizar()
        {
            modelo.ActualizarVentanaChat();
        }

        public Image ImagenEstadoContacto
        {
            set
            {
                this.imagenContacto.ImagenEstado = value;
            }
        }
        public Image ImagenEstadoPropia
        {
            set
            {
                this.imagenPropia.ImagenEstado = value;
            }
        }
        public Image ImagenPerfilContacto
        {
            set
            {
                this.imagenContacto.Imagen = value;
            }
        }

        public Image ImagenPerfilPropia
        {


            set
            {
                this.imagenPropia.Imagen = value;
            }
        }
        public String NickContacto
        {

            set
            {

                this.rtbNick.Rtf = value;
            }
        }
        public String SubNickContacto
        {
            set
            {
                this.rtbSubnick.Rtf = value;
            }
        }


        public String TextoDiciendo
        {
            get
            {
                return this.textoDecir.Text;
            }
            set
            {
                this.textoDecir.Text = value;
            }
        }


        internal void SetFuente(Font fuente, Color color)
        {
            this.textoDecir.ChangeFont(fuente, color);
        }



        internal void BorrarTextoDecir()
        {
            this.textoDecir.Text = "";
            this.textoDecir.Rtf = "";
        }

        internal void AgregarMensajeDeContacto(string mensaje)
        {
            this.controlConversacion1.AgregarMensajeDeContacto(mensaje);
            if(this.WindowState == FormWindowState.Minimized)
            {
                ContadorMensajesNoLeidos++;
                this.Text = "(" + ContadorMensajesNoLeidos + ")" + this.rtbNick.Text;
            }
            Console.WriteLine(this.TopLevel);
            Console.WriteLine(this.WindowState);
            Console.WriteLine(this.TopMost);

            modelo.MensajeRecibido();
        }
        internal void AgregarMensajePropio(string mensaje)
        {
            this.controlConversacion1.AgregarMensajePropio(mensaje);
            modelo.MensajeEnviado();
        }

        private void VentanaChatCerrando(object sender, FormClosingEventArgs e)
        {
            modelo.VentanaChatCerrandose(e);
        }

        private void VentanaChat_KeyUp(object sender, KeyEventArgs e)
        {
            modelo.SoltoTecla(e);
        }

        private void clickEmoticones(object sender, EventArgs e)
        {
            modelo.ClickMenuEmoticones();
        }

        private void textoDecir_KeyUp(object sender, KeyEventArgs e)
        {
            modelo.SoltoTeclaControlTextoDecir(e);
        }
        private void btnFuente_Click(object sender, EventArgs e)
        {
            modelo.EditarFuente();
        }




        private void VentanaChat_VisibleChanged(object sender, EventArgs e)
        {

            if (this.Visible)
            {
                ContadorMensajesNoLeidos = 0;
                this.Actualizar();
                Console.WriteLine("ventana chat visible");
            }
        }



        private void VentanaChat_Activated(object sender, EventArgs e)
        {
            //maquen con lunarcitos y flores, de los chiquitios que entran en el bolso
            this.ContadorMensajesNoLeidos = 0;
            this.Text = this.rtbNick.Text;
            Console.WriteLine("ventana chat activada");
            Console.WriteLine(this.TopLevel);
            Console.WriteLine(this.WindowState);
            Console.WriteLine(this.TopMost);
        }


        public void SetRutaSeparador(string ruta)
        {
            this.controlConversacion1.SetRutaSeparador = ruta;
        }
        public void SetNickPropioConversacion(String nuevoNick)
        {
            this.controlConversacion1.MiNick = nuevoNick;
        }
        public void SetFuentePropiaConversacion(Font fuente, Color color)
        {
            this.controlConversacion1.SetMiFuenteConversacion(fuente, color);
            SetFuente(fuente, color);
        }
        public void SetFuenteContactoConversacion(Font fuente, Color color)
        {
            this.controlConversacion1.SetSuFuenteConversacion(fuente, color);
        }



        private void comboRedes_SelectedIndexChanged(object sender, EventArgs e)
        {
            modelo.CambioRed();
        }

        private void VentanaChat_Leave(object sender, EventArgs e)
        {
            Console.WriteLine("ventana chat leave");
            Console.WriteLine(this.TopLevel);
            Console.WriteLine(this.WindowState);
            Console.WriteLine(this.TopMost);
        }

    }



}
