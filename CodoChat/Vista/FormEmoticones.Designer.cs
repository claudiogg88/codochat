﻿namespace CodoChat
{
    partial class FormEmoticones
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEmoticones));
            this.imagenesEmoticonesPropios = new System.Windows.Forms.ImageList(this.components);
            this.pContenedor = new System.Windows.Forms.Panel();
            this.btnRobar = new System.Windows.Forms.Button();
            this.btnImportarCarpeta = new System.Windows.Forms.Button();
            this.botonExportar = new System.Windows.Forms.Button();
            this.botonImportar = new System.Windows.Forms.Button();
            this.pEmoticones = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listaEmoticonesAjenos = new System.Windows.Forms.ListView();
            this.cmsEmoticonesPropios = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaEmoticonesPropios = new System.Windows.Forms.ListView();
            this.listaEmoticonesDefault = new System.Windows.Forms.ListView();
            this.cmsEmoticonesDefaults = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.soltarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagenesEmoticonesDefault = new System.Windows.Forms.ImageList(this.components);
            this.botonSoltar = new System.Windows.Forms.Button();
            this.botonEditar = new System.Windows.Forms.Button();
            this.botonEliminar = new System.Windows.Forms.Button();
            this.botonAgregar = new System.Windows.Forms.Button();
            this.imagenesEmoticonesAjenos = new System.Windows.Forms.ImageList(this.components);
            this.cmsEmoticonesAjenos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.robarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pContenedor.SuspendLayout();
            this.pEmoticones.SuspendLayout();
            this.cmsEmoticonesPropios.SuspendLayout();
            this.cmsEmoticonesDefaults.SuspendLayout();
            this.cmsEmoticonesAjenos.SuspendLayout();
            this.SuspendLayout();
            // 
            // imagenesEmoticonesPropios
            // 
            this.imagenesEmoticonesPropios.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imagenesEmoticonesPropios.ImageSize = new System.Drawing.Size(19, 19);
            this.imagenesEmoticonesPropios.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // pContenedor
            // 
            this.pContenedor.Controls.Add(this.btnRobar);
            this.pContenedor.Controls.Add(this.btnImportarCarpeta);
            this.pContenedor.Controls.Add(this.botonExportar);
            this.pContenedor.Controls.Add(this.botonImportar);
            this.pContenedor.Controls.Add(this.pEmoticones);
            this.pContenedor.Controls.Add(this.botonSoltar);
            this.pContenedor.Controls.Add(this.botonEditar);
            this.pContenedor.Controls.Add(this.botonEliminar);
            this.pContenedor.Controls.Add(this.botonAgregar);
            this.pContenedor.Dock = System.Windows.Forms.DockStyle.Left;
            this.pContenedor.Location = new System.Drawing.Point(0, 0);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(477, 640);
            this.pContenedor.TabIndex = 4;
            // 
            // btnRobar
            // 
            this.btnRobar.Location = new System.Drawing.Point(395, 354);
            this.btnRobar.Name = "btnRobar";
            this.btnRobar.Size = new System.Drawing.Size(75, 23);
            this.btnRobar.TabIndex = 19;
            this.btnRobar.Text = "Robar";
            this.btnRobar.UseVisualStyleBackColor = true;
            this.btnRobar.Click += new System.EventHandler(this.btnRobar_Click);
            // 
            // btnImportarCarpeta
            // 
            this.btnImportarCarpeta.Location = new System.Drawing.Point(395, 325);
            this.btnImportarCarpeta.Name = "btnImportarCarpeta";
            this.btnImportarCarpeta.Size = new System.Drawing.Size(75, 23);
            this.btnImportarCarpeta.TabIndex = 18;
            this.btnImportarCarpeta.Text = "Carpeta";
            this.btnImportarCarpeta.UseVisualStyleBackColor = true;
            this.btnImportarCarpeta.Click += new System.EventHandler(this.btnImportarCarpeta_Click);
            // 
            // botonExportar
            // 
            this.botonExportar.Location = new System.Drawing.Point(395, 296);
            this.botonExportar.Name = "botonExportar";
            this.botonExportar.Size = new System.Drawing.Size(75, 23);
            this.botonExportar.TabIndex = 17;
            this.botonExportar.Text = "Exportar";
            this.botonExportar.UseVisualStyleBackColor = true;
            this.botonExportar.Click += new System.EventHandler(this.botonExportar_Click);
            // 
            // botonImportar
            // 
            this.botonImportar.Location = new System.Drawing.Point(395, 267);
            this.botonImportar.Name = "botonImportar";
            this.botonImportar.Size = new System.Drawing.Size(75, 23);
            this.botonImportar.TabIndex = 16;
            this.botonImportar.Text = "Importar";
            this.botonImportar.UseVisualStyleBackColor = true;
            this.botonImportar.Click += new System.EventHandler(this.botonImportar_Click);
            // 
            // pEmoticones
            // 
            this.pEmoticones.AutoScroll = true;
            this.pEmoticones.Controls.Add(this.label3);
            this.pEmoticones.Controls.Add(this.label2);
            this.pEmoticones.Controls.Add(this.label1);
            this.pEmoticones.Controls.Add(this.listaEmoticonesAjenos);
            this.pEmoticones.Controls.Add(this.listaEmoticonesPropios);
            this.pEmoticones.Controls.Add(this.listaEmoticonesDefault);
            this.pEmoticones.Location = new System.Drawing.Point(0, 3);
            this.pEmoticones.Name = "pEmoticones";
            this.pEmoticones.Size = new System.Drawing.Size(389, 627);
            this.pEmoticones.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 551);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Emoticones Ajenos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 261);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Emoticones Personalizados";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Emoticones Default";
            // 
            // listaEmoticonesAjenos
            // 
            this.listaEmoticonesAjenos.AllowDrop = true;
            this.listaEmoticonesAjenos.ContextMenuStrip = this.cmsEmoticonesAjenos;
            this.listaEmoticonesAjenos.HideSelection = false;
            this.listaEmoticonesAjenos.LargeImageList = this.imagenesEmoticonesPropios;
            this.listaEmoticonesAjenos.Location = new System.Drawing.Point(0, 572);
            this.listaEmoticonesAjenos.Name = "listaEmoticonesAjenos";
            this.listaEmoticonesAjenos.ShowItemToolTips = true;
            this.listaEmoticonesAjenos.Size = new System.Drawing.Size(366, 276);
            this.listaEmoticonesAjenos.SmallImageList = this.imagenesEmoticonesPropios;
            this.listaEmoticonesAjenos.TabIndex = 14;
            this.listaEmoticonesAjenos.UseCompatibleStateImageBehavior = false;
            this.listaEmoticonesAjenos.SelectedIndexChanged += new System.EventHandler(this.listaEmoticonesAjenos_SelectedIndexChanged);
            // 
            // cmsEmoticonesPropios
            // 
            this.cmsEmoticonesPropios.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.editarToolStripMenuItem,
            this.agregarToolStripMenuItem,
            this.eliminarToolStripMenuItem});
            this.cmsEmoticonesPropios.Name = "cmsEmoticonesDefaults";
            this.cmsEmoticonesPropios.Size = new System.Drawing.Size(118, 92);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.toolStripMenuItem1.Text = "Soltar";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.editarToolStripMenuItem.Text = "Editar";
            this.editarToolStripMenuItem.Click += new System.EventHandler(this.editarToolStripMenuItem_Click);
            // 
            // agregarToolStripMenuItem
            // 
            this.agregarToolStripMenuItem.Name = "agregarToolStripMenuItem";
            this.agregarToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.agregarToolStripMenuItem.Text = "Agregar";
            this.agregarToolStripMenuItem.Click += new System.EventHandler(this.agregarToolStripMenuItem_Click);
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.eliminarToolStripMenuItem.Text = "Eliminar";
            this.eliminarToolStripMenuItem.Click += new System.EventHandler(this.eliminarToolStripMenuItem_Click);
            // 
            // listaEmoticonesPropios
            // 
            this.listaEmoticonesPropios.AllowDrop = true;
            this.listaEmoticonesPropios.ContextMenuStrip = this.cmsEmoticonesPropios;
            this.listaEmoticonesPropios.HideSelection = false;
            this.listaEmoticonesPropios.LargeImageList = this.imagenesEmoticonesPropios;
            this.listaEmoticonesPropios.Location = new System.Drawing.Point(0, 278);
            this.listaEmoticonesPropios.Name = "listaEmoticonesPropios";
            this.listaEmoticonesPropios.ShowItemToolTips = true;
            this.listaEmoticonesPropios.Size = new System.Drawing.Size(366, 270);
            this.listaEmoticonesPropios.SmallImageList = this.imagenesEmoticonesPropios;
            this.listaEmoticonesPropios.TabIndex = 13;
            this.listaEmoticonesPropios.UseCompatibleStateImageBehavior = false;
            this.listaEmoticonesPropios.SelectedIndexChanged += new System.EventHandler(this.listaEmoticonesPropios_SelectedIndexChanged);
            this.listaEmoticonesPropios.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listaEmoticonesPropios_MouseDoubleClick);
            // 
            // listaEmoticonesDefault
            // 
            this.listaEmoticonesDefault.AllowDrop = true;
            this.listaEmoticonesDefault.ContextMenuStrip = this.cmsEmoticonesDefaults;
            this.listaEmoticonesDefault.HideSelection = false;
            this.listaEmoticonesDefault.LargeImageList = this.imagenesEmoticonesDefault;
            this.listaEmoticonesDefault.Location = new System.Drawing.Point(0, 22);
            this.listaEmoticonesDefault.Name = "listaEmoticonesDefault";
            this.listaEmoticonesDefault.ShowItemToolTips = true;
            this.listaEmoticonesDefault.Size = new System.Drawing.Size(366, 236);
            this.listaEmoticonesDefault.SmallImageList = this.imagenesEmoticonesDefault;
            this.listaEmoticonesDefault.TabIndex = 11;
            this.listaEmoticonesDefault.TileSize = new System.Drawing.Size(1, 1);
            this.listaEmoticonesDefault.UseCompatibleStateImageBehavior = false;
            this.listaEmoticonesDefault.SelectedIndexChanged += new System.EventHandler(this.listaEmoticonesDefault_SelectedIndexChanged);
            this.listaEmoticonesDefault.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listaEmoticonesDefault_MouseDoubleClick);
            // 
            // cmsEmoticonesDefaults
            // 
            this.cmsEmoticonesDefaults.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.soltarToolStripMenuItem});
            this.cmsEmoticonesDefaults.Name = "cmsEmoticonesDefaults";
            this.cmsEmoticonesDefaults.Size = new System.Drawing.Size(105, 26);
            // 
            // soltarToolStripMenuItem
            // 
            this.soltarToolStripMenuItem.Name = "soltarToolStripMenuItem";
            this.soltarToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.soltarToolStripMenuItem.Text = "Soltar";
            this.soltarToolStripMenuItem.Click += new System.EventHandler(this.soltarToolStripMenuItem_Click);
            // 
            // imagenesEmoticonesDefault
            // 
            this.imagenesEmoticonesDefault.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imagenesEmoticonesDefault.ImageSize = new System.Drawing.Size(19, 19);
            this.imagenesEmoticonesDefault.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // botonSoltar
            // 
            this.botonSoltar.Location = new System.Drawing.Point(395, 90);
            this.botonSoltar.Name = "botonSoltar";
            this.botonSoltar.Size = new System.Drawing.Size(75, 23);
            this.botonSoltar.TabIndex = 14;
            this.botonSoltar.Text = "Soltar";
            this.botonSoltar.UseVisualStyleBackColor = true;
            this.botonSoltar.Click += new System.EventHandler(this.botonSoltar_Click);
            // 
            // botonEditar
            // 
            this.botonEditar.Location = new System.Drawing.Point(395, 32);
            this.botonEditar.Name = "botonEditar";
            this.botonEditar.Size = new System.Drawing.Size(75, 23);
            this.botonEditar.TabIndex = 12;
            this.botonEditar.Text = "Editar";
            this.botonEditar.UseVisualStyleBackColor = true;
            this.botonEditar.Click += new System.EventHandler(this.botonEditar_Click);
            // 
            // botonEliminar
            // 
            this.botonEliminar.Location = new System.Drawing.Point(395, 61);
            this.botonEliminar.Name = "botonEliminar";
            this.botonEliminar.Size = new System.Drawing.Size(75, 23);
            this.botonEliminar.TabIndex = 13;
            this.botonEliminar.Text = "Eliminar";
            this.botonEliminar.UseVisualStyleBackColor = true;
            this.botonEliminar.Click += new System.EventHandler(this.botonEliminar_Click);
            // 
            // botonAgregar
            // 
            this.botonAgregar.Location = new System.Drawing.Point(395, 3);
            this.botonAgregar.Name = "botonAgregar";
            this.botonAgregar.Size = new System.Drawing.Size(75, 23);
            this.botonAgregar.TabIndex = 11;
            this.botonAgregar.Text = "Agregar";
            this.botonAgregar.UseVisualStyleBackColor = true;
            this.botonAgregar.Click += new System.EventHandler(this.botonAgregar_Click);
            // 
            // imagenesEmoticonesAjenos
            // 
            this.imagenesEmoticonesAjenos.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imagenesEmoticonesAjenos.ImageSize = new System.Drawing.Size(19, 19);
            this.imagenesEmoticonesAjenos.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cmsEmoticonesAjenos
            // 
            this.cmsEmoticonesAjenos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.robarToolStripMenuItem});
            this.cmsEmoticonesAjenos.Name = "cmsEmoticonesDefaults";
            this.cmsEmoticonesAjenos.Size = new System.Drawing.Size(106, 26);
            // 
            // robarToolStripMenuItem
            // 
            this.robarToolStripMenuItem.Name = "robarToolStripMenuItem";
            this.robarToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.robarToolStripMenuItem.Text = "Robar";
            this.robarToolStripMenuItem.Click += new System.EventHandler(this.robarToolStripMenuItem_Click);
            // 
            // FormEmoticones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 640);
            this.Controls.Add(this.pContenedor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormEmoticones";
            this.Text = "Emoticones";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormEmoticones_FormClosing);
            this.VisibleChanged += new System.EventHandler(this.FormEmoticones_VisibleChanged);
            this.pContenedor.ResumeLayout(false);
            this.pEmoticones.ResumeLayout(false);
            this.pEmoticones.PerformLayout();
            this.cmsEmoticonesPropios.ResumeLayout(false);
            this.cmsEmoticonesDefaults.ResumeLayout(false);
            this.cmsEmoticonesAjenos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

		public System.Windows.Forms.ImageList imagenesEmoticonesPropios;
		private System.Windows.Forms.Panel pContenedor;
        private System.Windows.Forms.Panel pEmoticones;
		public System.Windows.Forms.Button botonSoltar;
		public System.Windows.Forms.Button botonEditar;
		public System.Windows.Forms.Button botonEliminar;
		public System.Windows.Forms.Button botonAgregar;
        public System.Windows.Forms.ImageList imagenesEmoticonesDefault;
        public System.Windows.Forms.ListView listaEmoticonesDefault;
        public System.Windows.Forms.ListView listaEmoticonesPropios;
        public System.Windows.Forms.Button botonExportar;
        public System.Windows.Forms.Button botonImportar;
        public System.Windows.Forms.Button btnImportarCarpeta;
        public System.Windows.Forms.Button btnRobar;
        private System.Windows.Forms.ContextMenuStrip cmsEmoticonesDefaults;
        private System.Windows.Forms.ToolStripMenuItem soltarToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsEmoticonesPropios;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        public System.Windows.Forms.ListView listaEmoticonesAjenos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ImageList imagenesEmoticonesAjenos;
        private System.Windows.Forms.ContextMenuStrip cmsEmoticonesAjenos;
        private System.Windows.Forms.ToolStripMenuItem robarToolStripMenuItem;
    }
}