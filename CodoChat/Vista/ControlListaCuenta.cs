﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CodoChat.Modelo;
using CodoChat.ModeloVista;

namespace CodoChat.Forms
{
    public partial class ControlListaCuenta : UserControl
    {

        ModeloControlListaCuenta modelo;

        public ControlListaCuenta()
        {
            
            InitializeComponent();
            this.modelo = new ModeloControlListaCuenta(this);
        }

        public void Actualizar()
        {
            if (modelo != null)
            {
                this.modelo.ActualizarListaCuenta();
            }
        }


        public String CuentaSeleccionada()
        {
            if (this.lwCuentas.SelectedItems.Count > 0)
            {
                return this.lwCuentas.SelectedItems[0].Name;
            }

            else
            {
                return null;
            }
        }

        private void lwCuentas_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.modelo.Actualizar = true;
            this.Actualizar();
        }
        private void btnConectar_Click(object sender, EventArgs e)
        {
            modelo.Conectar();
        }


        private void btnEliminar_Click(object sender, EventArgs e)
        {
            modelo.Eliminar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            modelo.Crear();

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            modelo.Editar();
        }

        private void ControlListaCuenta_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Actualizar();
            }
        }



    }
}
