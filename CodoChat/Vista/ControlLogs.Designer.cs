﻿namespace CodoChat.Forms
{
    partial class ControlLogs
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.limpiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scrollLockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pContenedor = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.etqTitulo = new System.Windows.Forms.Label();
            this.txbLogs = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip1.SuspendLayout();
            this.pContenedor.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.limpiarToolStripMenuItem,
            this.scrollLockToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(132, 48);
            // 
            // limpiarToolStripMenuItem
            // 
            this.limpiarToolStripMenuItem.Name = "limpiarToolStripMenuItem";
            this.limpiarToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.limpiarToolStripMenuItem.Text = "Limpiar";
            this.limpiarToolStripMenuItem.Click += new System.EventHandler(this.limpiarToolStripMenuItem_Click);
            // 
            // scrollLockToolStripMenuItem
            // 
            this.scrollLockToolStripMenuItem.Name = "scrollLockToolStripMenuItem";
            this.scrollLockToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.scrollLockToolStripMenuItem.Text = "Scroll Lock";
            this.scrollLockToolStripMenuItem.Click += new System.EventHandler(this.scrollLockToolStripMenuItem_Click);
            // 
            // pContenedor
            // 
            this.pContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenedor.AutoScroll = true;
            this.pContenedor.Controls.Add(this.panel1);
            this.pContenedor.Controls.Add(this.txbLogs);
            this.pContenedor.Location = new System.Drawing.Point(0, 0);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(768, 495);
            this.pContenedor.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.etqTitulo);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(765, 35);
            this.panel1.TabIndex = 1;
            // 
            // etqTitulo
            // 
            this.etqTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.etqTitulo.Location = new System.Drawing.Point(0, 0);
            this.etqTitulo.Name = "etqTitulo";
            this.etqTitulo.Size = new System.Drawing.Size(765, 31);
            this.etqTitulo.TabIndex = 2;
            this.etqTitulo.Text = "Logs";
            this.etqTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txbLogs
            // 
            this.txbLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbLogs.ContextMenuStrip = this.contextMenuStrip1;
            this.txbLogs.Location = new System.Drawing.Point(0, 34);
            this.txbLogs.Name = "txbLogs";
            this.txbLogs.Size = new System.Drawing.Size(765, 458);
            this.txbLogs.TabIndex = 0;
            this.txbLogs.Text = "";
            this.txbLogs.VisibleChanged += new System.EventHandler(this.txbLogs_VisibleChanged);
            // 
            // ControlLogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pContenedor);
            this.Name = "ControlLogs";
            this.Size = new System.Drawing.Size(771, 495);
            this.contextMenuStrip1.ResumeLayout(false);
            this.pContenedor.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem limpiarToolStripMenuItem;
        private System.Windows.Forms.Panel pContenedor;
        public System.Windows.Forms.RichTextBox txbLogs;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label etqTitulo;
        public System.Windows.Forms.ToolStripMenuItem scrollLockToolStripMenuItem;
    }
}
