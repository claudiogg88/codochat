﻿using CodoChat.ModeloVista;
namespace CodoChat.Forms
{
    partial class FormConfiguracion
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfiguracion));
            this.perfil1 = new CodoChat.Forms.ControlPerfil();
            this.logs1 = new CodoChat.Forms.ControlLogs();
            this.listaCuenta1 = new CodoChat.Forms.ControlListaCuenta();
            this.personal1 = new CodoChat.Forms.ControlPersonal();
            this.controlContactos1 = new CodoChat.Forms.ControlContactos();
            this.pContenedor = new System.Windows.Forms.Panel();
            this.pConfiguraciones = new System.Windows.Forms.Panel();
            this.pBotones = new System.Windows.Forms.Panel();
            this.btnPerfil = new System.Windows.Forms.Button();
            this.btnLogs = new System.Windows.Forms.Button();
            this.btnCuentas = new System.Windows.Forms.Button();
            this.btnNick = new System.Windows.Forms.Button();
            this.btnContactos = new System.Windows.Forms.Button();
            this.pContenedor.SuspendLayout();
            this.pConfiguraciones.SuspendLayout();
            this.pBotones.SuspendLayout();
            this.SuspendLayout();
            // 
            // pContenedor
            // 
            this.pContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenedor.Controls.Add(this.pConfiguraciones);
            this.pContenedor.Controls.Add(this.pBotones);
            this.pContenedor.Location = new System.Drawing.Point(-1, 2);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(1039, 618);
            this.pContenedor.TabIndex = 0;
            // 
            // pConfiguraciones
            // 
            this.pConfiguraciones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pConfiguraciones.Controls.Add(this.perfil1);
            this.pConfiguraciones.Controls.Add(this.logs1);
            this.pConfiguraciones.Controls.Add(this.listaCuenta1);
            this.pConfiguraciones.Controls.Add(this.personal1);
            this.pConfiguraciones.Controls.Add(this.controlContactos1);
            this.pConfiguraciones.Location = new System.Drawing.Point(134, 4);
            this.pConfiguraciones.Name = "pConfiguraciones";
            this.pConfiguraciones.Size = new System.Drawing.Size(905, 614);
            this.pConfiguraciones.TabIndex = 1;
            // 
            // perfil1
            // 
            this.perfil1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)
                | System.Windows.Forms.AnchorStyles.Right)));
            this.perfil1.Location = new System.Drawing.Point(8, 6);
            this.perfil1.Name = "perfil1";
            this.perfil1.Size = new System.Drawing.Size(897, 608);
            this.perfil1.TabIndex = 4;
            this.perfil1.Visible = false;
            // 
            // logs1
            // 
            this.logs1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logs1.Location = new System.Drawing.Point(3, 3);
            this.logs1.Name = "logs1";
            this.logs1.Size = new System.Drawing.Size(899, 608);
            this.logs1.TabIndex = 3;
            // 
            // listaCuenta1
            // 
            this.listaCuenta1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listaCuenta1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.listaCuenta1.Location = new System.Drawing.Point(8, 6);
            this.listaCuenta1.Name = "listaCuenta1";
            this.listaCuenta1.Size = new System.Drawing.Size(894, 605);
            this.listaCuenta1.TabIndex = 1;
            this.listaCuenta1.Visible = false;
            // 
            // personal1
            // 
            this.personal1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.personal1.ColorFuente = System.Drawing.SystemColors.ControlText;
            this.personal1.Estado = "";
            this.personal1.EstadoCambio = false;
            this.personal1.Fuente = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.personal1.FuenteCambio = false;
            this.personal1.ImagenCambio = false;
            this.personal1.ImagenPerfil = ((System.Drawing.Image)(resources.GetObject("personal1.ImagenPerfil")));
            this.personal1.Location = new System.Drawing.Point(0, 0);
            this.personal1.Name = "personal1";
            this.personal1.Nick = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang3082{\\fonttbl{\\f0\\fnil\\fcharset0 Microsoft S" +
    "ans Serif;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs17\\par\r\n}\r\n";
            this.personal1.NickCambio = false;
            this.personal1.Size = new System.Drawing.Size(886, 593);
            this.personal1.Subnick = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang3082{\\fonttbl{\\f0\\fnil\\fcharset0 Microsoft S" +
    "ans Serif;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs17\\par\r\n}\r\n";
            this.personal1.SubnickCambio = false;
            this.personal1.TabIndex = 0;
            this.personal1.TextoNick = "";
            // 
            // pBotones
            // 
            this.pBotones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pBotones.Controls.Add(this.btnContactos);
            this.pBotones.Controls.Add(this.btnPerfil);
            this.pBotones.Controls.Add(this.btnLogs);
            this.pBotones.Controls.Add(this.btnCuentas);
            this.pBotones.Controls.Add(this.btnNick);
            this.pBotones.Location = new System.Drawing.Point(4, 4);
            this.pBotones.Name = "pBotones";
            this.pBotones.Size = new System.Drawing.Size(132, 614);
            this.pBotones.TabIndex = 0;
            // 
            // btnPerfil
            // 
            this.btnPerfil.Location = new System.Drawing.Point(1, 249);
            this.btnPerfil.Name = "btnPerfil";
            this.btnPerfil.Size = new System.Drawing.Size(130, 85);
            this.btnPerfil.TabIndex = 3;
            this.btnPerfil.Text = "Preferencias";
            this.btnPerfil.UseVisualStyleBackColor = true;
            this.btnPerfil.Click += new System.EventHandler(this.btnPerfil_Click);
            // 
            // btnLogs
            // 
            this.btnLogs.Location = new System.Drawing.Point(1, 166);
            this.btnLogs.Name = "btnLogs";
            this.btnLogs.Size = new System.Drawing.Size(130, 85);
            this.btnLogs.TabIndex = 2;
            this.btnLogs.Text = "Logs";
            this.btnLogs.UseVisualStyleBackColor = true;
            this.btnLogs.Click += new System.EventHandler(this.btnLogs_Click);
            // 
            // btnCuentas
            // 
            this.btnCuentas.Location = new System.Drawing.Point(1, 84);
            this.btnCuentas.Name = "btnCuentas";
            this.btnCuentas.Size = new System.Drawing.Size(130, 85);
            this.btnCuentas.TabIndex = 1;
            this.btnCuentas.Text = "Cuentas";
            this.btnCuentas.UseVisualStyleBackColor = true;
            this.btnCuentas.Click += new System.EventHandler(this.Cuentas_Click);
            // 
            // btnNick
            // 
            this.btnNick.Location = new System.Drawing.Point(1, 0);
            this.btnNick.Name = "btnNick";
            this.btnNick.Size = new System.Drawing.Size(130, 85);
            this.btnNick.TabIndex = 0;
            this.btnNick.Text = "Personal";
            this.btnNick.UseVisualStyleBackColor = true;
            this.btnNick.Click += new System.EventHandler(this.btnNick_Click);
            // 
            // btnContactos
            // 
            this.btnContactos.Location = new System.Drawing.Point(1, 334);
            this.btnContactos.Name = "btnContactos";
            this.btnContactos.Size = new System.Drawing.Size(130, 85);
            this.btnContactos.TabIndex = 4;
            this.btnContactos.Text = "Contactos";
            this.btnContactos.UseVisualStyleBackColor = true;
            this.btnContactos.Click += new System.EventHandler(this.btnContactos_Click);
            // 
            // controlContactos1
            // 
            //             
            this.controlContactos1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlContactos1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.controlContactos1.Location = new System.Drawing.Point(8, 6);
            this.controlContactos1.Name = "controlContactos1";
            this.controlContactos1.Size = new System.Drawing.Size(894, 605);
            this.controlContactos1.TabIndex = 1;
            this.controlContactos1.Visible = false;

            // FormConfiguracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 632);
            this.Controls.Add(this.pContenedor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormConfiguracion";
            this.Text = "Configuracion";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormConfiguracion_FormClosing);
            this.pContenedor.ResumeLayout(false);
            this.pConfiguraciones.ResumeLayout(false);
            this.pBotones.ResumeLayout(false);
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.Panel pContenedor;
        private System.Windows.Forms.Panel pBotones;
        private System.Windows.Forms.Button btnNick;
        private System.Windows.Forms.Button btnCuentas;
        private System.Windows.Forms.Panel pConfiguraciones;
        private ControlPersonal personal1;
        public ControlListaCuenta listaCuenta1;
        public ControlContactos controlContactos1;
        private System.Windows.Forms.Button btnLogs;
        public ControlLogs logs1;
        private ControlPerfil perfil1;
        private System.Windows.Forms.Button btnPerfil;
        private System.Windows.Forms.Button btnContactos;
    }
}