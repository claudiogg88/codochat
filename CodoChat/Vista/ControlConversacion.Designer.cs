﻿namespace CodoChat.Forms
{
    partial class ControlConversacion
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textoConversacion = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // textoConversacion
            // 
            this.textoConversacion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textoConversacion.Location = new System.Drawing.Point(3, 3);
            this.textoConversacion.MinimumSize = new System.Drawing.Size(20, 20);
            this.textoConversacion.Name = "textoConversacion";
            this.textoConversacion.Size = new System.Drawing.Size(672, 384);
            this.textoConversacion.TabIndex = 17;
            this.textoConversacion.Url = new System.Uri("", System.UriKind.Relative);
            this.textoConversacion.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.textoConversacion_DocumentCompleted);
            this.textoConversacion.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.textoConversacion_Navigating);
            // 
            // ControlConversacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textoConversacion);
            this.Name = "ControlConversacion";
            this.Size = new System.Drawing.Size(678, 390);
            this.Resize += new System.EventHandler(this.ControlConversacion_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser textoConversacion;
    }
}
