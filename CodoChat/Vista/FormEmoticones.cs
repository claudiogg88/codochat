﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Forms;

using CodoChat.Modelo;
using CodoChat.ModeloVista;


namespace CodoChat
{
    public partial class FormEmoticones : Form
    {
        ModeloFormEmoticones modelo;
        public ListView activa;
        public string IdContactoReceptor { get; set; }
        public FormEmoticones()
        {

            InitializeComponent();
            this.listaEmoticonesDefault.ListViewItem_SetSpacing(19 + 15, 19 + 20);
            this.listaEmoticonesPropios.ListViewItem_SetSpacing(19 + 15, 19 + 20);
            this.listaEmoticonesAjenos.ListViewItem_SetSpacing(19 + 15, 19 + 20);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;
            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;
            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;
            modelo = new ModeloFormEmoticones(this);
            activa = this.listaEmoticonesDefault;
        }
        public void Actualizar()
        {
            modelo.ActualizarVentanaEmoticones();
        }




        private void FormEmoticones_FormClosing(object sender, FormClosingEventArgs e)
        {
            modelo.FormCerrandose(e);
        }

        private void botonAgregar_Click(object sender, EventArgs e)
        {
            modelo.Crear();

        }

        private void botonSoltar_Click(object sender, EventArgs e)
        {
            if (activa == this.listaEmoticonesDefault)
            {
                modelo.SoltarEmoticonDefault();
            }
            else
            {
                modelo.SoltarEmoticonPropio();
            }

        }

        private void botonEliminar_Click(object sender, EventArgs e)
        {
            modelo.Eliminar();
        }

        private void botonEditar_Click(object sender, EventArgs e)
        {
            modelo.Editar();
        }

        private void listaEmoticonesDefault_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            modelo.SoltarEmoticonDefault();

        }

        private void listaEmoticonesPropios_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            modelo.SoltarEmoticonPropio();
        }

        private void FormEmoticones_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Actualizar();
                if (this.Owner != null)
                {
                    string idReceptor = ((VentanaChat)this.Owner).IdVentana;
                    if (this.IdContactoReceptor != idReceptor)
                    {
                        this.IdContactoReceptor = idReceptor;
                        modelo.RellenarEmoticonesAjenos();
                    }
                }

            }
        }

        private void listaEmoticonesDefault_SelectedIndexChanged(object sender, EventArgs e)
        {
            activa = this.listaEmoticonesDefault;
            modelo.ActualizarBotones();
        }


        private void listaEmoticonesPropios_SelectedIndexChanged(object sender, EventArgs e)
        {
            activa = this.listaEmoticonesPropios;
            modelo.ActualizarBotones();
        }

        private void botonImportar_Click(object sender, EventArgs e)
        {
            modelo.ImportarArchivo();
        }
        private void btnImportarCarpeta_Click(object sender, EventArgs e)
        {
            modelo.ImportarCarpeta();
        }
        private void botonExportar_Click(object sender, EventArgs e)
        {
            modelo.ExportarEmoticones();
        }

        private void editarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.Editar();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.Eliminar();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            modelo.SoltarEmoticonDefault();
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.Crear();
        }

        private void soltarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.SoltarEmoticonPropio();
        }

        private void robarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            modelo.RobarEmoticones();
        }

        private void btnRobar_Click(object sender, EventArgs e)
        {
            modelo.RobarEmoticones();
        }

        private void listaEmoticonesAjenos_SelectedIndexChanged(object sender, EventArgs e)
        {
            activa = this.listaEmoticonesAjenos;
            modelo.ActualizarBotones();
        }






    }
}
