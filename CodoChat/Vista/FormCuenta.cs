﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Modelo;

namespace CodoChat
{
    public partial class FormCuenta : Form
    {
		public String IdCliente;
		public String Usuario;
		public String Clave;
		public bool Aceptado;
        public FormCuenta()
        {
			Aceptado = false;
            InitializeComponent();
			this.textBoxClave.UseSystemPasswordChar = true;
        }

		private void button1_Click(object sender, EventArgs e)
		{
            Aceptar();
		}

        private void Aceptar()
        {
            Aceptado = true;
            IdCliente = this.comboBoxServidores.SelectedText;
            Usuario = this.textBoxUsuario.Text;
            Clave = this.textBoxClave.Text;
            this.Hide();
        }
        private void FormCuenta_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Aceptar();
            }
          
        }

        private void comboBoxServidores_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Aceptar();
            }
        }

        private void textBoxUsuario_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Aceptar();
            }
        }

        private void textBoxClave_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Aceptar();
            }
        }

        		
	}
}
