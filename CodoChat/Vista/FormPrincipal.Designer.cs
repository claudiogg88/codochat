﻿using System.Drawing;
using CodoChat;
using CodoChat.ModeloVista;
namespace CodoChat
   
{
	partial class VentanaPrincipal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        //
        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaPrincipal));
            this.menuVentanaPrincipal = new System.Windows.Forms.MenuStrip();
            this.listaContactos = new Forms.ControlListaContactos();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConfiguracion = new System.Windows.Forms.ToolStripMenuItem();
            this.personalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cuentasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pListaContactos = new System.Windows.Forms.Panel();
            this.panelBusqueda = new System.Windows.Forms.Panel();
            this.txtFiltroContacto = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pNickSubnick = new System.Windows.Forms.Panel();
            this.rtbSubnick = new System.Windows.Forms.RichTextBox();
            this.rtbNick = new System.Windows.Forms.RichTextBox();
            this.imagenPerfil = new CodoChat.Forms.ControlImagenPerfil();
            this.trayCodoChat = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuTryIcon = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cerrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contactosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuVentanaPrincipal.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pListaContactos.SuspendLayout();
            this.panelBusqueda.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pNickSubnick.SuspendLayout();
            this.menuTryIcon.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuVentanaPrincipal
            // 
            this.menuVentanaPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.menuConfiguracion});
            this.menuVentanaPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuVentanaPrincipal.Name = "menuVentanaPrincipal";
            this.menuVentanaPrincipal.Size = new System.Drawing.Size(742, 24);
            this.menuVentanaPrincipal.TabIndex = 5;
            this.menuVentanaPrincipal.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cerrarToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            this.cerrarToolStripMenuItem.Click += new System.EventHandler(this.cerrarToolStripMenuItem_Click);
            // 
            // menuConfiguracion
            // 
            this.menuConfiguracion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.personalToolStripMenuItem,
            this.cuentasToolStripMenuItem,
            this.logsToolStripMenuItem,
            this.perfilesToolStripMenuItem,
            this.contactosToolStripMenuItem});
            this.menuConfiguracion.Name = "menuConfiguracion";
            this.menuConfiguracion.Size = new System.Drawing.Size(95, 20);
            this.menuConfiguracion.Text = "Configuracion";
            // 
            // personalToolStripMenuItem
            // 
            this.personalToolStripMenuItem.Name = "personalToolStripMenuItem";
            this.personalToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.personalToolStripMenuItem.Text = "Personal";
            this.personalToolStripMenuItem.Click += new System.EventHandler(this.personalToolStripMenuItem_Click);
            // 
            // cuentasToolStripMenuItem
            // 
            this.cuentasToolStripMenuItem.Name = "cuentasToolStripMenuItem";
            this.cuentasToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cuentasToolStripMenuItem.Text = "Cuentas";
            this.cuentasToolStripMenuItem.Click += new System.EventHandler(this.cuentasToolStripMenuItem_Click);
            // 
            // logsToolStripMenuItem
            // 
            this.logsToolStripMenuItem.Name = "logsToolStripMenuItem";
            this.logsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.logsToolStripMenuItem.Text = "Logs";
            this.logsToolStripMenuItem.Click += new System.EventHandler(this.logsToolStripMenuItem_Click_1);
            // 
            // perfilesToolStripMenuItem
            // 
            this.perfilesToolStripMenuItem.Name = "perfilesToolStripMenuItem";
            this.perfilesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.perfilesToolStripMenuItem.Text = "Preferencias";
            this.perfilesToolStripMenuItem.Click += new System.EventHandler(this.perfilesToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.pListaContactos);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(742, 577);
            this.panel1.TabIndex = 6;
            // 
            // pListaContactos
            // 
            this.pListaContactos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pListaContactos.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pListaContactos.Controls.Add(this.panelBusqueda);
            this.pListaContactos.Controls.Add(this.listaContactos);
            this.pListaContactos.Location = new System.Drawing.Point(0, 163);
            this.pListaContactos.Name = "pListaContactos";
            this.pListaContactos.Size = new System.Drawing.Size(742, 411);
            this.pListaContactos.TabIndex = 13;
            // 
            // panelBusqueda
            // 
            this.panelBusqueda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBusqueda.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelBusqueda.Controls.Add(this.txtFiltroContacto);
            this.panelBusqueda.Controls.Add(this.lblBuscar);
            this.panelBusqueda.Location = new System.Drawing.Point(0, 0);
            this.panelBusqueda.Name = "panelBusqueda";
            this.panelBusqueda.Size = new System.Drawing.Size(742, 37);
            this.panelBusqueda.TabIndex = 15;
            // 
            // txtFiltroContacto
            // 
            this.txtFiltroContacto.Location = new System.Drawing.Point(54, 7);
            this.txtFiltroContacto.Name = "txtFiltroContacto";
            this.txtFiltroContacto.Size = new System.Drawing.Size(373, 20);
            this.txtFiltroContacto.TabIndex = 1;
            this.txtFiltroContacto.TextChanged += new System.EventHandler(this.txtFiltroContacto_TextChanged);
            // 
            // lblBuscar
            // 
            this.lblBuscar.Location = new System.Drawing.Point(7, 7);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(41, 23);
            this.lblBuscar.TabIndex = 0;
            this.lblBuscar.Text = "Filtrar";
            // 
            // listaContactos
            // 
            this.listaContactos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listaContactos.AutoScroll = true;
            this.listaContactos.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.listaContactos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listaContactos.Location = new System.Drawing.Point(0, 33);
            this.listaContactos.Name = "listaContactos";
            this.listaContactos.Size = new System.Drawing.Size(742, 378);
            this.listaContactos.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.Controls.Add(this.pNickSubnick);
            this.panel2.Controls.Add(this.imagenPerfil);
            this.panel2.Location = new System.Drawing.Point(0, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(742, 163);
            this.panel2.TabIndex = 12;
            // 
            // pNickSubnick
            // 
            this.pNickSubnick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pNickSubnick.Controls.Add(this.rtbSubnick);
            this.pNickSubnick.Controls.Add(this.rtbNick);
            this.pNickSubnick.Location = new System.Drawing.Point(131, 17);
            this.pNickSubnick.Name = "pNickSubnick";
            this.pNickSubnick.Size = new System.Drawing.Size(608, 100);
            this.pNickSubnick.TabIndex = 10;
            // 
            // rtbSubnick
            // 
            this.rtbSubnick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbSubnick.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtbSubnick.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbSubnick.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbSubnick.Location = new System.Drawing.Point(2, 35);
            this.rtbSubnick.Multiline = false;
            this.rtbSubnick.Name = "rtbSubnick";
            this.rtbSubnick.ReadOnly = true;
            this.rtbSubnick.Size = new System.Drawing.Size(604, 30);
            this.rtbSubnick.TabIndex = 1;
            this.rtbSubnick.Text = "";
            // 
            // rtbNick
            // 
            this.rtbNick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbNick.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtbNick.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbNick.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbNick.Location = new System.Drawing.Point(2, 4);
            this.rtbNick.Multiline = false;
            this.rtbNick.Name = "rtbNick";
            this.rtbNick.ReadOnly = true;
            this.rtbNick.Size = new System.Drawing.Size(604, 30);
            this.rtbNick.TabIndex = 0;
            this.rtbNick.Text = "";
            // 
            // imagenPerfil
            // 
            this.imagenPerfil.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.imagenPerfil.Imagen = ((System.Drawing.Image)(resources.GetObject("imagenPerfil.Imagen")));
            this.imagenPerfil.ImagenEstado = ((System.Drawing.Image)(resources.GetObject("imagenPerfil.ImagenEstado")));
            this.imagenPerfil.Location = new System.Drawing.Point(5, 17);
            this.imagenPerfil.Name = "imagenPerfil";
            this.imagenPerfil.Size = new System.Drawing.Size(120, 120);
            this.imagenPerfil.TabIndex = 9;
            // 
            // trayCodoChat
            // 
            this.trayCodoChat.ContextMenuStrip = this.menuTryIcon;
            this.trayCodoChat.Icon = ((System.Drawing.Icon)(resources.GetObject("trayCodoChat.Icon")));
            this.trayCodoChat.Text = "CodoChat";
            this.trayCodoChat.Visible = true;
            this.trayCodoChat.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trayCodoChat_MouseDoubleClick);
            // 
            // menuTryIcon
            // 
            this.menuTryIcon.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cerrarToolStripMenuItem1});
            this.menuTryIcon.Name = "menuTryIcon";
            this.menuTryIcon.Size = new System.Drawing.Size(107, 26);
            // 
            // cerrarToolStripMenuItem1
            // 
            this.cerrarToolStripMenuItem1.Name = "cerrarToolStripMenuItem1";
            this.cerrarToolStripMenuItem1.Size = new System.Drawing.Size(106, 22);
            this.cerrarToolStripMenuItem1.Text = "Cerrar";
            this.cerrarToolStripMenuItem1.Click += new System.EventHandler(this.cerrarToolStripMenuItem1_Click);
            // 
            // contactosToolStripMenuItem
            // 
            this.contactosToolStripMenuItem.Name = "contactosToolStripMenuItem";
            this.contactosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.contactosToolStripMenuItem.Text = "Contactos";
            this.contactosToolStripMenuItem.Click += new System.EventHandler(this.contactosToolStripMenuItem_Click);
            // 
            // VentanaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(742, 602);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuVentanaPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuVentanaPrincipal;
            this.Name = "VentanaPrincipal";
            this.Text = "Codo Chat";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VentanaPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.VentanaPrincipal_Load);
            this.menuVentanaPrincipal.ResumeLayout(false);
            this.menuVentanaPrincipal.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.pListaContactos.ResumeLayout(false);
            this.panelBusqueda.ResumeLayout(false);
            this.panelBusqueda.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pNickSubnick.ResumeLayout(false);
            this.menuTryIcon.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.MenuStrip menuVentanaPrincipal;
		private System.Windows.Forms.ToolStripMenuItem menuConfiguracion;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private Forms.ControlImagenPerfil imagenPerfil;
		private System.Windows.Forms.Panel pListaContactos;
        private CodoChat.Forms.ControlListaContactos listaContactos;
		private System.Windows.Forms.Panel pNickSubnick;
        private System.Windows.Forms.RichTextBox rtbSubnick;
        private System.Windows.Forms.RichTextBox rtbNick;
        private System.Windows.Forms.ToolStripMenuItem personalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cuentasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perfilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon trayCodoChat;
        private System.Windows.Forms.ContextMenuStrip menuTryIcon;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem1;
        private System.Windows.Forms.Panel panelBusqueda;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.TextBox txtFiltroContacto;
        private System.Windows.Forms.ToolStripMenuItem contactosToolStripMenuItem;
        


    }
}

