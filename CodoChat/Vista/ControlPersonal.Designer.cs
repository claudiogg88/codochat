﻿namespace CodoChat.Forms
{
	partial class ControlPersonal
	{
		/// <summary> 
		/// Variable del diseñador requerida.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Limpiar los recursos que se estén utilizando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de componentes

		/// <summary> 
		/// Método necesario para admitir el Diseñador. No se puede modificar 
		/// el contenido del método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPersonal));
            this.pContenedor = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnReiniciar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.imagenPerfil1 = new CodoChat.Forms.ControlImagenPerfil();
            this.btnImagen = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnFuente = new System.Windows.Forms.Button();
            this.lblFuente = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblEstado = new System.Windows.Forms.Label();
            this.comboEstado = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rtbSubnick = new System.Windows.Forms.RichTextBox();
            this.cmsSubnick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuFuenteSubnick = new System.Windows.Forms.ToolStripMenuItem();
            this.lblSubnick = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rtbNick = new System.Windows.Forms.RichTextBox();
            this.cmsNick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuFuenteNick = new System.Windows.Forms.ToolStripMenuItem();
            this.lblNick = new System.Windows.Forms.Label();
            this.pContenedor.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.cmsSubnick.SuspendLayout();
            this.panel3.SuspendLayout();
            this.cmsNick.SuspendLayout();
            this.SuspendLayout();
            // 
            // pContenedor
            // 
            this.pContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenedor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pContenedor.Controls.Add(this.panel6);
            this.pContenedor.Controls.Add(this.panel5);
            this.pContenedor.Controls.Add(this.panel4);
            this.pContenedor.Controls.Add(this.panel1);
            this.pContenedor.Controls.Add(this.panel2);
            this.pContenedor.Controls.Add(this.panel3);
            this.pContenedor.Location = new System.Drawing.Point(3, 0);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(830, 537);
            this.pContenedor.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.Controls.Add(this.btnReiniciar);
            this.panel6.Controls.Add(this.btnGuardar);
            this.panel6.Location = new System.Drawing.Point(136, 133);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(692, 46);
            this.panel6.TabIndex = 14;
            // 
            // btnReiniciar
            // 
            this.btnReiniciar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReiniciar.Location = new System.Drawing.Point(542, 6);
            this.btnReiniciar.Name = "btnReiniciar";
            this.btnReiniciar.Size = new System.Drawing.Size(149, 36);
            this.btnReiniciar.TabIndex = 6;
            this.btnReiniciar.Text = "Reiniciar Cuenta";
            this.btnReiniciar.UseVisualStyleBackColor = true;
            this.btnReiniciar.Click += new System.EventHandler(this.btnReiniciar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnGuardar.Enabled = false;
            this.btnGuardar.Location = new System.Drawing.Point(3, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(115, 39);
            this.btnGuardar.TabIndex = 8;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.imagenPerfil1);
            this.panel5.Controls.Add(this.btnImagen);
            this.panel5.Location = new System.Drawing.Point(3, 5);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(132, 174);
            this.panel5.TabIndex = 13;
            // 
            // imagenPerfil1
            // 
            this.imagenPerfil1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.imagenPerfil1.Imagen = ((System.Drawing.Image)(resources.GetObject("imagenPerfil1.Imagen")));
            this.imagenPerfil1.ImagenEstado = ((System.Drawing.Image)(resources.GetObject("imagenPerfil1.ImagenEstado")));
            this.imagenPerfil1.Location = new System.Drawing.Point(5, 0);
            this.imagenPerfil1.Name = "imagenPerfil1";
            this.imagenPerfil1.Size = new System.Drawing.Size(123, 122);
            this.imagenPerfil1.TabIndex = 6;
            // 
            // btnImagen
            // 
            this.btnImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImagen.Location = new System.Drawing.Point(6, 128);
            this.btnImagen.Name = "btnImagen";
            this.btnImagen.Size = new System.Drawing.Size(123, 40);
            this.btnImagen.TabIndex = 5;
            this.btnImagen.Text = "Cambiar";
            this.btnImagen.UseVisualStyleBackColor = true;
            this.btnImagen.Click += new System.EventHandler(this.btnImagen_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnFuente);
            this.panel4.Controls.Add(this.lblFuente);
            this.panel4.Location = new System.Drawing.Point(485, 93);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(346, 41);
            this.panel4.TabIndex = 12;
            // 
            // btnFuente
            // 
            this.btnFuente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFuente.Location = new System.Drawing.Point(214, 3);
            this.btnFuente.Name = "btnFuente";
            this.btnFuente.Size = new System.Drawing.Size(129, 36);
            this.btnFuente.TabIndex = 5;
            this.btnFuente.Text = "Cambiar";
            this.btnFuente.UseVisualStyleBackColor = true;
            this.btnFuente.Click += new System.EventHandler(this.btnFuente_Click);
            // 
            // lblFuente
            // 
            this.lblFuente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFuente.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuente.Location = new System.Drawing.Point(3, 3);
            this.lblFuente.Name = "lblFuente";
            this.lblFuente.Size = new System.Drawing.Size(209, 35);
            this.lblFuente.TabIndex = 3;
            this.lblFuente.Text = "Fuente";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblEstado);
            this.panel1.Controls.Add(this.comboEstado);
            this.panel1.Location = new System.Drawing.Point(137, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(343, 41);
            this.panel1.TabIndex = 11;
            // 
            // lblEstado
            // 
            this.lblEstado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEstado.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.Location = new System.Drawing.Point(3, 1);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(121, 35);
            this.lblEstado.TabIndex = 3;
            this.lblEstado.Text = "Estado";
            // 
            // comboEstado
            // 
            this.comboEstado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboEstado.FormattingEnabled = true;
            this.comboEstado.Location = new System.Drawing.Point(133, 3);
            this.comboEstado.Name = "comboEstado";
            this.comboEstado.Size = new System.Drawing.Size(207, 33);
            this.comboEstado.TabIndex = 10;
            this.comboEstado.SelectedIndexChanged += new System.EventHandler(this.comboEstado_SelectedIndexChanged);
            this.comboEstado.KeyUp += new System.Windows.Forms.KeyEventHandler(this.comboEstado_KeyUp);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.rtbSubnick);
            this.panel2.Controls.Add(this.lblSubnick);
            this.panel2.Location = new System.Drawing.Point(136, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(691, 40);
            this.panel2.TabIndex = 9;
            // 
            // rtbSubnick
            // 
            this.rtbSubnick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbSubnick.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbSubnick.ContextMenuStrip = this.cmsSubnick;
            this.rtbSubnick.Location = new System.Drawing.Point(130, 1);
            this.rtbSubnick.Multiline = false;
            this.rtbSubnick.Name = "rtbSubnick";
            this.rtbSubnick.Size = new System.Drawing.Size(558, 35);
            this.rtbSubnick.TabIndex = 4;
            this.rtbSubnick.Text = "";
            this.rtbSubnick.TextChanged += new System.EventHandler(this.rtbSubnick_TextChanged);
            this.rtbSubnick.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rtbSubnick_KeyUp);
            // 
            // cmsSubnick
            // 
            this.cmsSubnick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFuenteSubnick});
            this.cmsSubnick.Name = "contextMenuStrip1";
            this.cmsSubnick.Size = new System.Drawing.Size(111, 26);
            // 
            // menuFuenteSubnick
            // 
            this.menuFuenteSubnick.Name = "menuFuenteSubnick";
            this.menuFuenteSubnick.Size = new System.Drawing.Size(110, 22);
            this.menuFuenteSubnick.Text = "Fuente";
            this.menuFuenteSubnick.Click += new System.EventHandler(this.menuFuenteSubnick_Click);
            // 
            // lblSubnick
            // 
            this.lblSubnick.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSubnick.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubnick.Location = new System.Drawing.Point(3, 1);
            this.lblSubnick.Name = "lblSubnick";
            this.lblSubnick.Size = new System.Drawing.Size(121, 35);
            this.lblSubnick.TabIndex = 3;
            this.lblSubnick.Text = "Subnick";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.rtbNick);
            this.panel3.Controls.Add(this.lblNick);
            this.panel3.Location = new System.Drawing.Point(136, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(691, 40);
            this.panel3.TabIndex = 7;
            // 
            // rtbNick
            // 
            this.rtbNick.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbNick.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbNick.ContextMenuStrip = this.cmsNick;
            this.rtbNick.Location = new System.Drawing.Point(130, 1);
            this.rtbNick.Multiline = false;
            this.rtbNick.Name = "rtbNick";
            this.rtbNick.Size = new System.Drawing.Size(558, 35);
            this.rtbNick.TabIndex = 4;
            this.rtbNick.Text = "";
            this.rtbNick.TextChanged += new System.EventHandler(this.rtbNick_TextChanged);
            this.rtbNick.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rtbNick_KeyUp);
            // 
            // cmsNick
            // 
            this.cmsNick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFuenteNick});
            this.cmsNick.Name = "contextMenuStrip1";
            this.cmsNick.Size = new System.Drawing.Size(111, 26);
            this.cmsNick.Click += new System.EventHandler(this.menuFuenteNick_Click);
            // 
            // menuFuenteNick
            // 
            this.menuFuenteNick.Name = "menuFuenteNick";
            this.menuFuenteNick.Size = new System.Drawing.Size(110, 22);
            this.menuFuenteNick.Text = "Fuente";
            // 
            // lblNick
            // 
            this.lblNick.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNick.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNick.Location = new System.Drawing.Point(3, 1);
            this.lblNick.Name = "lblNick";
            this.lblNick.Size = new System.Drawing.Size(121, 35);
            this.lblNick.TabIndex = 3;
            this.lblNick.Text = "Nick";
            // 
            // ControlPersonal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pContenedor);
            this.Name = "ControlPersonal";
            this.Size = new System.Drawing.Size(833, 540);
            this.VisibleChanged += new System.EventHandler(this.Personal_VisibleChanged);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Personal_KeyUp);
            this.pContenedor.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.cmsSubnick.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.cmsNick.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pContenedor;
		private System.Windows.Forms.Panel panel5;
		public ControlImagenPerfil imagenPerfil1;
		private System.Windows.Forms.Button btnImagen;
		private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnFuente;
		private System.Windows.Forms.Label lblFuente;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblEstado;
		public System.Windows.Forms.ComboBox comboEstado;
		private System.Windows.Forms.Button btnGuardar;
		private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox rtbSubnick;
		private System.Windows.Forms.Label lblSubnick;
		private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox rtbNick;
		private System.Windows.Forms.Label lblNick;
		private System.Windows.Forms.ContextMenuStrip cmsSubnick;
        private System.Windows.Forms.ToolStripMenuItem menuFuenteSubnick;
		private System.Windows.Forms.ContextMenuStrip cmsNick;
        private System.Windows.Forms.ToolStripMenuItem menuFuenteNick;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnReiniciar;

	}
}
