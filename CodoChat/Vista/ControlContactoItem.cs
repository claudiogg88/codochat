﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.ModeloVista;
using CodoChat.Modelo;

namespace CodoChat.Forms
{
	public partial class ControlContactoItem : UserControl
	{

        public String Clave;
        ModeloControlContactoItem modelo;

        public ControlContactoItem(ContactoColor contacto, Grupo grupo)
        {

            InitializeComponent();
            this.modelo = new ModeloControlContactoItem(this,contacto, grupo);
            this.Clave = modelo.Id;
        }

        public string Id { get { return this.modelo.Id;  } }
        public void Actualizar()
        {
            modelo.ActualizarContactoItem();
        }

		public Image ImagenEstado
		{
			set
			{
				this.imagenPerfil1.ImagenEstado = value;
			}
		}
		public Image ImagenPerfil
		{
			set
			{
				this.imagenPerfil1.Imagen = value;
			}
		}
		public String TextoNick
		{
			set
			{
				this.Nick.Rtf = value;
			}
            get
            {
                return this.Nick.Text;
            }
		}
		public String TextoSubnick
		{

			set
			{
				this.Subnick.Rtf = value;
			}
            get
            {
                return this.Subnick.Text;
            }
		}

        public void LogoFacebook(Boolean visible)
        {
            this.lblFacebookIcon.Visible = visible;
        }
        public void LogoGoogle(Boolean visible)
        {
            this.lblGTalkIcon.Visible = visible;
        }
        public void LogoCodoChat(Boolean visible)
        {
            this.lblCodoChatIcon.Visible = visible;
        }
        public string Redes
        {
            set
            {
                this.imagenPerfil1.ToolTipTexto = value;
            }
        }

		private void ContactoItem_Load(object sender, EventArgs e)
		{
            this.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            addOnMouseClick(this);
            addOnMouseEnter(this);
            addOnMouseLeave(this);
		}


		private void addOnMouseClick(Control control)
		{
            foreach (Control child in control.Controls)
            {
                addOnMouseClick(child);
            }
            control.MouseDoubleClick += new MouseEventHandler(this.ContactoItem_MouseDoubleClick);
		}

		private void ContactoItem_MouseDoubleClick(object sender, MouseEventArgs e)
		{
            modelo.DobleClickItemContacto();
		}


		private void addOnMouseEnter(Control control)
        {
            foreach (Control child in control.Controls)
            {
                addOnMouseEnter(child);
            }
            control.MouseEnter += new EventHandler(control_MouseEnter);
		}


		private void addOnMouseLeave(Control control)
		{
            foreach (Control child in control.Controls)
            {
                addOnMouseLeave(child);
            }
            control.MouseLeave += new EventHandler(control_MouseLeave);
		}

		private void Colorear(Control control)
		{
            foreach (Control child in control.Controls)
            {
                Colorear(child);
            }
            control.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			
		}
		private void Descolorear(Control control)
		{
            foreach (Control child in control.Controls)
            {
                Descolorear(child);
            }
            control.BackColor = System.Drawing.SystemColors.ControlLightLight;
		}
		void control_MouseEnter(object sender, EventArgs e)
		{
            Colorear(this);
		}
		void control_MouseLeave(object sender, EventArgs e)
		{
            Descolorear(this);
		}




    }
}
