﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodoChat.Forms
{
	public partial class FormSeleccionarImagen : Form
	{
		public String Ruta;
		private Point RectStartPoint;
		private Rectangle Rect;
		private Brush selectionBrush = new SolidBrush(Color.FromArgb(128, 72, 145, 220));
		public DialogResult result;
		public Image Imagen
		{
			get
			{
				return this.imagenPerfil1.Imagen;
			}
			set
			{
				this.imagenPerfil1.Imagen = value;
			}
		}
		public FormSeleccionarImagen()
		{
			InitializeComponent();
			Ruta = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			Rect = new Rectangle();
		}

		private void btnExaminar_Click(object sender, EventArgs e)
		{
			openFileDialog1 = new OpenFileDialog();

			openFileDialog1.InitialDirectory = Ruta;
			//openFileDialog1.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
			//openFileDialog1.FilterIndex = 0;
			//openFileDialog1.RestoreDirectory = true;

			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				try
				{
					Image image = Image.FromFile(openFileDialog1.FileName);

					this.pbCompleto.Image = image;
					this.Imagen = Adornos.CuadrarImagen(image);
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
				}
			}
		}


		// Start Rectangle
		//
		private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (pbCompleto.Image == null)
				return;
			// Determine the initial rectangle coordinates...
			RectStartPoint = e.Location;
			Invalidate();
		}

		// Draw Rectangle
		//
		private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (pbCompleto.Image == null)
				return;
			if (e.Button != MouseButtons.Left)
				return;
			Point tempEndPoint = e.Location;
			Rect.Location = new Point(
				Math.Min(RectStartPoint.X, tempEndPoint.X),
				Math.Min(RectStartPoint.Y, tempEndPoint.Y));
			Rect.Size = new Size(
				Math.Abs(RectStartPoint.X - tempEndPoint.X),
				Math.Abs(RectStartPoint.Y - tempEndPoint.Y));
			pbCompleto.Invalidate();
		}

		// Draw Area
		//
		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if (pbCompleto.Image == null)
				return;
			// Draw the rectangle...
			if (pbCompleto.Image != null)
			{
				if (Rect != null && Rect.Width > 0 && Rect.Height > 0)
				{
					e.Graphics.FillRectangle(selectionBrush, Rect);
				}
			}
		}

		private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
		{
			if (pbCompleto.Image == null)
				return;
			if (e.Button == MouseButtons.Right)
			{
				if (Rect.Contains(e.Location))
				{
					//Debug.WriteLine("Right click");
				}
			}
			else
			{
				if (Rect.Width > 0 && Rect.Height > 0)
				{
					this.Imagen = Adornos.CuadrarImagen(Adornos.ExtraerRectangulo(this.pbCompleto.Image, Rect)); //;
				
				}
			}
		}
		private void btnAceptar_Click(object sender, EventArgs e)
		{
			this.Hide();
			result = DialogResult.OK;
		}

	}
}
