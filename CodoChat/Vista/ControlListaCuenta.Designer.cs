﻿namespace CodoChat.Forms
{
    partial class ControlListaCuenta
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlListaCuenta));
            this.ilLogos = new System.Windows.Forms.ImageList(this.components);
            this.pContenedor = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.botonConectar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.pLista = new System.Windows.Forms.Panel();
            this.lwCuentas = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pContenedor.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pLista.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilLogos
            // 
            this.ilLogos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLogos.ImageStream")));
            this.ilLogos.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLogos.Images.SetKeyName(0, "Facebook");
            this.ilLogos.Images.SetKeyName(1, "Google");
            // 
            // pContenedor
            // 
            this.pContenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pContenedor.Controls.Add(this.panel3);
            this.pContenedor.Controls.Add(this.pLista);
            this.pContenedor.Location = new System.Drawing.Point(3, 4);
            this.pContenedor.Name = "pContenedor";
            this.pContenedor.Size = new System.Drawing.Size(716, 411);
            this.pContenedor.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.btnEditar);
            this.panel3.Controls.Add(this.btnAgregar);
            this.panel3.Controls.Add(this.botonConectar);
            this.panel3.Controls.Add(this.btnEliminar);
            this.panel3.Location = new System.Drawing.Point(347, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(158, 394);
            this.panel3.TabIndex = 1;
            // 
            // btnEditar
            // 
            this.btnEditar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar.Location = new System.Drawing.Point(0, 32);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(155, 23);
            this.btnEditar.TabIndex = 3;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAgregar.Location = new System.Drawing.Point(0, 3);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(155, 23);
            this.btnAgregar.TabIndex = 2;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // botonConectar
            // 
            this.botonConectar.Location = new System.Drawing.Point(0, 90);
            this.botonConectar.Name = "botonConectar";
            this.botonConectar.Size = new System.Drawing.Size(155, 23);
            this.botonConectar.TabIndex = 1;
            this.botonConectar.Text = "Conectar";
            this.botonConectar.UseVisualStyleBackColor = true;
            this.botonConectar.Click += new System.EventHandler(this.btnConectar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminar.Location = new System.Drawing.Point(0, 61);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(155, 23);
            this.btnEliminar.TabIndex = 0;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // pLista
            // 
            this.pLista.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pLista.Controls.Add(this.lwCuentas);
            this.pLista.Location = new System.Drawing.Point(0, 3);
            this.pLista.Name = "pLista";
            this.pLista.Size = new System.Drawing.Size(342, 394);
            this.pLista.TabIndex = 0;
            // 
            // lwCuentas
            // 
            this.lwCuentas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lwCuentas.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lwCuentas.HideSelection = false;
            this.lwCuentas.LargeImageList = this.ilLogos;
            this.lwCuentas.Location = new System.Drawing.Point(0, 0);
            this.lwCuentas.Name = "lwCuentas";
            this.lwCuentas.Size = new System.Drawing.Size(339, 391);
            this.lwCuentas.SmallImageList = this.ilLogos;
            this.lwCuentas.TabIndex = 0;
            this.lwCuentas.TileSize = new System.Drawing.Size(232, 400);
            this.lwCuentas.UseCompatibleStateImageBehavior = false;
            this.lwCuentas.View = System.Windows.Forms.View.List;
            this.lwCuentas.SelectedIndexChanged += new System.EventHandler(this.lwCuentas_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 400;
            // 
            // ControlListaCuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.pContenedor);
            this.Name = "ControlListaCuenta";
            this.Size = new System.Drawing.Size(722, 418);
            this.VisibleChanged += new System.EventHandler(this.ControlListaCuenta_VisibleChanged);
            this.pContenedor.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pLista.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ImageList ilLogos;
        private System.Windows.Forms.Panel pContenedor;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Button btnEditar;
        public System.Windows.Forms.Button btnAgregar;
        public System.Windows.Forms.Button botonConectar;
        public System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Panel pLista;
        public System.Windows.Forms.ListView lwCuentas;
        private System.Windows.Forms.ColumnHeader columnHeader1;
    }
}
