﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodoChat.Modelo;
using System.Diagnostics;
using System.Web;
using CodoChat.ModeloVista;

namespace CodoChat.Forms
{
    public partial class ControlConversacion : UserControl
    {

        public string IdVentana { get; set; }
        private List<String> MensajesEnCola;
        private Boolean ListaParaAgregarMensajes;
        public Boolean Preparada;
        private float TamañoMaximoLetra = 22;
        ModeloControlConversacion modelo;
        public ControlConversacion(ContactoColor receptor)
        {
            MensajesEnCola = new List<string>();
            ListaParaAgregarMensajes = false;
            Preparada = false;
            InitializeComponent();
            this.textoConversacion.Navigate("about:blank");
            this.textoConversacion.Document.Write(String.Empty);
            this.textoConversacion.DocumentText = CodoChat.Properties.Resources.conversacion;
            this.modelo = new ModeloVista.ModeloControlConversacion(this, receptor);
        }
        public ControlConversacion()
        {

            InitializeComponent();

        }


        public void Actualizar()
        {
            this.modelo.ActualizarControlConversacion();
        }

        public void AgregarMensajePropio(String mensaje)
        {
            //mensaje =  "(" + DateTime.Now.ToShortDateString() + ") "+mensaje;
            object[] parametros = new object[] { mensaje };
            this.textoConversacion.Document.InvokeScript("addMiDecir", parametros);
        }

        public void AgregarMensajeDeContacto(String mensaje)
        {
            //mensaje = "(" + DateTime.Now.ToShortDateString() + ") " + mensaje;
            if (ListaParaAgregarMensajes)
            {
                object[] parametros = new object[] { mensaje };
                this.textoConversacion.Document.InvokeScript("addSuDecir", parametros);
            }
            else
            {
                this.MensajesEnCola.Add(mensaje);
            }
        }

        public String MiNick
        {
            set
            {
                object[] parametros = new object[] { value };
                this.textoConversacion.Document.InvokeScript("setMiNick", parametros);
            }
        }
        public String SuNick
        {
            set
            {
                object[] parametros = new object[] { value };
                this.textoConversacion.Document.InvokeScript("setSuNick", parametros);
            }
        }
        public String SetRutaSeparador
        {
            set
            {
                object[] parametros = new object[] { value };
                this.textoConversacion.Document.InvokeScript("setRutaSeparador", parametros);

            }
        }

        public void SetMiFuenteConversacion(Font fuente, Color color)
        {
            TamañoMaximoLetra = fuente.Size;
            object[] parametros = new object[] { fuente.Size.ToString(), fuente.Italic, fuente.Bold, fuente.FontFamily.Name, ColorTranslator.ToHtml(color) };
            this.textoConversacion.Document.InvokeScript("addMiEstilo", parametros);
        }
        public void SetSuFuenteConversacion(Font fuente, Color color)
        {
            object[] parametros = new object[] { TamañoMaximoLetra.ToString(), fuente.Italic, fuente.Bold, fuente.FontFamily.Name, ColorTranslator.ToHtml(color) };
            this.textoConversacion.Document.InvokeScript("addSuEstilo", parametros);
        }

        private void textoConversacion_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            this.Actualizar();
            ListaParaAgregarMensajes = true;
            Preparada = true;
            if (MensajesEnCola.Count() > 0)
            {
                foreach (String mensaje in MensajesEnCola)
                {
                    AgregarMensajeDeContacto(mensaje);
                }
            }
        }

        private void textoConversacion_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (!e.Url.ToString().Equals("about:blank"))
            {
                e.Cancel = true;
                Process.Start(e.Url.ToString());
            }
        }
        private void ControlConversacion_Resize(object sender, EventArgs e)
        {
            if (ListaParaAgregarMensajes)
            {
                this.textoConversacion.Document.InvokeScript("scrollAbajo");
            }
        }
    }
}
