﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodoChat.Modelo;
using CodoChat.ModeloVista;

namespace CodoChat.Forms
{
    public partial class ControlGrupoContactos : UserControl
    {
        private Image contraer;
        private Image expandir;
        public ModeloControlGrupoContactos modelo;
        private Grupo grupo;

        public ControlGrupoContactos(Grupo grupo)
        {
            contraer = CodoChat.Properties.Resources.contraer;
            expandir = CodoChat.Properties.Resources.expandir;
            InitializeComponent();
            this.pictureBox1.Image = contraer;
            this.modelo = new ModeloControlGrupoContactos(this, grupo);
        }



        internal void Actualizar()
        {
            this.modelo.ActualizarControlGrupoContactos();
            foreach (ControlContactoItem contacto in this.TLPContactos.Controls)
            {
                contacto.Actualizar();
            }
        }

        public string Titulo
        {
            get
            {
                return this.rtbTitulo.Text;
            }
            set
            {
                this.rtbTitulo.Text = value;
            }
        }

        internal void Filtrar(string texto)
        {
            var lista = this.TLPContactos.Controls;
            foreach (ControlContactoItem item in lista)
            {
                if (texto == "")
                {
                    item.Visible = true;
                    continue;
                }
                if (item.TextoNick.ToLower().Contains(texto.ToLower()) || item.TextoSubnick.ToLower().Contains(texto.ToLower()))
                {
                    item.Visible = true;
                }
                else
                {
                    item.Visible = false;
                }
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.modelo.Oculto = !this.modelo.Oculto;
            this.TLPContactos.SuspendLayout();

            if (this.modelo.Oculto)
            {
                this.OcultarContactos();
            }
            else
            {
                this.MostrarContactos();
            }
            if (this.modelo.Oculto)
                pictureBox1.Image = expandir;
            else
                pictureBox1.Image = contraer;

            this.TLPContactos.ResumeLayout();
        }




        public void OcultarContactos()
        {
            foreach (ControlContactoItem item in this.TLPContactos.Controls)
            {
                item.Hide();
            }
        }


        public void MostrarContactos()
        {
            foreach (ControlContactoItem item in this.TLPContactos.Controls)
            {
                item.Show();
            }
        }



        public string Id { get { return this.modelo.Id; } }
    }
}
