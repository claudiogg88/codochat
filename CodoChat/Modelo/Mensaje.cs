﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodoChat.Modelo
{
    [Serializable()]
    public class Mensaje
    {
        public string Texto { get; set; }
        public DateTime Tiempo { get; set; }

        public TipoMensaje Tipo { get; set; }
        public enum TipoMensaje
        {
            Recibido,
            Enviado,
            Error
        }
        public Mensaje(string mensaje, TipoMensaje tipo)
        {
            Texto = mensaje;
            Tiempo = DateTime.Now;
            Tipo = tipo;
        }
    }
}
