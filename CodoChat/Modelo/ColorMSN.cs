﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using CodoChat.ModeloVista;

namespace CodoChat.Modelo
{
    [Serializable()]
    partial class ColorMSN
    {

        public List<ContactoColor> Contactos { get; set; }
        public ContactoColor Yo { get; set; }
        public DateTime ActualizacionListaContactos { get; set; }
        public List<Cuenta> Asociadas { get; set; }
        public String NombreColorMSN { get { return NombreCliente.ColorMSN; } set { } }
        public List<Grupo> GruposContactos { get; set; }
        public List<EventoLog> Logs { get; set; }
        public String Token { get; set; }
        Preferencias Preferencias { get; set; }
        public Dictionary<string, Emoticon> Emoticones { get; set; }
        public Dictionary<String, Historial> Historiales { get; set; }

        private static ColorMSN instancia;
        public static ColorMSN Instancia { get { return instancia; } set { } }

        [NonSerialized]
        private Servidor Servidor;

        public Boolean Fixed = false;

        public ColorMSN()
        {
            Preferencias = new Preferencias();
            Preferencias.OcultarABandeja = false;
            Preferencias.SonidoAlRecibirMensaje = false;
            Preferencias.SonidoEnConexion = false;
            Preferencias.TitilarEnMensaje = true;
            Preferencias.ContactosDesconectadosAparte = true;

            Logs = new List<EventoLog>();
            GruposContactos = Utiles.CrearGrupos();
            Contactos = new List<ContactoColor>();

            Yo = new ContactoColor();
            Yo.Nick = Utiles.NickDefaultRTF("Mi nick :)");
            Yo.Subnick = Utiles.SubNickDefaultRTF("Mi subnick :O");
            Yo.Estado = Estado.Online;
            Yo.Fuente = new Fuente(new Font(SystemFonts.DefaultFont.Name, 18, FontStyle.Regular), Color.Blue);
            Yo.UsuarioColor = Utiles.GenerarClave();

            Asociadas = new List<Cuenta>();
            Emoticones = Adornos.IniciarEmoticonesDefault();
            Historiales = new Dictionary<String, Historial>();
            Fixed = true;
        }

        public void Iniciar()
        {
            instancia = this;
            if (!Fixed)
            {
                MessageBox.Show(GUI.VentanaPrincipal, "Esta Cuenta sera reiniciada para ser compatible con la nueva version");
                Reparar();
            }
            Repositorio.Agregar(Contactos);
            Repositorio.Agregar(Asociadas);
            Repositorio.Agregar(Yo);
            Repositorio.Agregar(GruposContactos);
            
            foreach (ContactoColor contacto in Contactos)
            {
                contacto.Estado = CodoChat.Estado.Desconectado;
                bool cambio = this.GetPreferencias().AsignarGrupo(contacto);
            }
            foreach (Cuenta cuenta in Asociadas)
            {
                cuenta.Estado = CodoChat.EstadoCuenta.Desconectado;
                Repositorio.Agregar(cuenta.Contactos);
                Repositorio.Agregar(cuenta.Yo);
            }
            Utiles.ChequearEmoticones();
            Servidor = new Servidor();
            Yo.Estado = Estado.Online;

            this.Login();
            Modelos.modeloFormPrincipal.Actualizar = true;
            Modelos.SetActualizarGruposContactos();
            Modelos.SetActualizarListaContactos();

        }

        private void Reparar()
        {
            Preferencias = new Preferencias();


            Logs = new List<EventoLog>();
            GruposContactos = Utiles.CrearGrupos();
            Contactos = new List<ContactoColor>();

            Yo.ReiniciarYo();

            Emoticones = Adornos.IniciarEmoticonesDefault();

            foreach (Cuenta cuenta in Asociadas)
            {
                cuenta.Limpiar();
            }
            Historiales = new Dictionary<String, Historial>();
            Fixed = true;
        }



        private ContactoColor BuscarPorContactoSimple(ContactoSimple contactoSimple)
        {
            return Contactos.Where(c => c.IdContactoColor == contactoSimple.IdContactoColor).FirstOrDefault();
        }

        /////////////////////////////////////////LLAMADAS HACIA LOS CLIENTES ORIGINALES: FACEBOOK, GOOGLE Y EL PROPIO/////////////////////////////////////////
        public void Login()
        {
            Servidor.Login();
            foreach (Cuenta cuenta in Asociadas)
            {
                cuenta.Login();
            }
        }
        internal void Login(string idCuenta)
        {
            Cuenta cuenta = GetCuenta(idCuenta);
            cuenta.Login();
        }
        /// <summary>
        /// Logout de la cuenta especificada
        /// </summary>
        /// <param name="idCuenta"></param>
        internal void Logout(string idCuenta)
        {
            Cuenta cuenta = GetCuenta(idCuenta);
            cuenta.Desconectar();
        }
        internal void Logout()
        {
            Yo.Estado = Estado.Desconectado;
            Servidor.Logout();
            foreach (Cuenta cuenta in Asociadas)
            {
                cuenta.Desconectar();
            }
        }

        public void addLog(object mensaje, params object[] args)
        {
            string sMensaje = mensaje.ToString();
            sMensaje = String.Format(sMensaje, args);

            EventoLog log = new EventoLog(sMensaje);
            //evitar que se acumulen demasiados
            if (Logs.Count > 3000)
            {
                Logs.Clear();
            }
            Logs.Add(log);
            Modelos.modeloControlLogs.Actualizar = true;
        }
        internal Emoticon GetEmoticon(string idElemento)
        {
            Emoticon salida;
            bool encontrado = Emoticones.TryGetValue(idElemento, out salida);
            if (!encontrado)
            {
                Yo.Emoticones.TryGetValue(idElemento, out salida);
            }
            return salida;
        }

        internal Fuente GetFuente()
        {
            return Yo.Fuente;
        }


        /// <summary>
        /// Busca un contacto color segun su idContactoColor
        /// </summary>
        /// <param name="idContactoColor"></param>
        /// <returns></returns>
        public ContactoColor GetContacto(String idContactoColor)
        {
            if (Yo.IdContactoColor.Equals(idContactoColor))
            {
                throw new Exception("Estas tratando de actualizar el contacto desde un lugar equivocado");
            }
            return Contactos.Where(contacto => contacto.IdContactoColor.Equals(idContactoColor)).FirstOrDefault();

        }
        public Cuenta GetCuenta(string idCuenta)
        {
            if (Repositorio.CuentaCol.ContainsKey(idCuenta))
            {
                return Repositorio.CuentaCol[idCuenta];
            }
            return null;
        }




        internal void AddCuenta(Cuenta cuenta)
        {
            Asociadas.Add(cuenta);
            Modelos.modeloListaCuenta.Actualizar = true;
            cuenta.Login();
            addLog("Cuenta " + cuenta.IdCuenta + " Agregada");
        }



        public Preferencias GetPreferencias()
        {
            return Preferencias;
        }

        internal bool HayCuentasAsociadas()
        {
            return this.Asociadas.Where(c => c.Validada).Count() > 0;
        }




        public Grupo GetGrupoPorNombre(string nombreGrupo)
        {
            return this.GruposContactos.Find(d => d.Nombre == nombreGrupo);
        }
        public Grupo GetGrupo(string idGrupo)
        {
            return this.GruposContactos.Find(d => d.IdGrupo == idGrupo);
        }
        internal void NuevoGrupo(Grupo grupo)
        {
            this.GruposContactos.Add(grupo);
            Modelos.SetActualizarListaContactos();
            Modelos.SetActualizarGruposContactos();
        }

        internal void GrupoEditado(Grupo grupo)
        {
            Modelos.SetActualizarListaContactos();
            Modelos.SetActualizarGruposContactos();
        }

        internal void EliminarGrupo(Grupo grupo)
        {
            foreach (ContactoColor contacto in Contactos)
            {
                if (contacto.Grupos.Remove(grupo.IdGrupo))
                {
                    this.Preferencias.AsignarGrupo(contacto);
                }
            }
            Repositorio.GrupoCol.Remove(grupo.IdGrupo);
            this.GruposContactos.Remove(grupo);
            Modelos.SetActualizarListaContactos();
            Modelos.SetActualizarGruposContactos();
        }

        internal void CopiarContactosAGrupo(List<ContactoColor> contactos, Grupo grupo)
        {
            foreach (ContactoColor contacto in contactos)
            {
                contacto.Grupos.Add(grupo.IdGrupo);
            }
            Modelos.SetActualizarGruposContactos();
        }

        internal void MoverContactosAGrupo(List<ContactoColor> contactos, Grupo grupoOrigen, Grupo grupoDest)
        {
            foreach (ContactoColor contacto in contactos)
            {
                contacto.Grupos.Add(grupoDest.IdGrupo);
                contacto.Grupos.Remove(grupoOrigen.IdGrupo);
            }
            Modelos.SetActualizarGruposContactos();
        }

        internal void OcultarContactos(List<ContactoColor> contactos)
        {
            Grupo oculto = GetGrupoPorNombre(NombreGrupo.Ocultos);
            foreach (ContactoColor contacto in contactos)
            {
                contacto.Grupos.Clear();
                contacto.Grupos.Add(oculto.IdGrupo);
            }
            Modelos.SetActualizarGruposContactos();
        }

        internal void DesocultarContactos(List<ContactoColor> contactos)
        {

            Grupo todos = GetGrupoPorNombre(NombreGrupo.Todos);
            foreach (ContactoColor contacto in contactos)
            {
                contacto.Grupos.Clear();
                contacto.Grupos.Add(todos.IdGrupo);
                this.Preferencias.AsignarGrupo(contacto);
            }
            Modelos.SetActualizarGruposContactos();
        }

        internal void RemoverContactosDeGrupo(List<ContactoColor> contactos, Grupo grupoRemover)
        {
            foreach (ContactoColor contacto in contactos)
            {
                contacto.Grupos.Remove(grupoRemover.IdGrupo);
                this.Preferencias.AsignarGrupo(contacto);
            }
            Modelos.SetActualizarGruposContactos();
        }

        internal void FusionarContactos(List<ContactoColor> contactos)
        {
            ContactoColor contactoJuntador = null;
            //se selecciona el primero
            if (contactos.Count > 0)
            {
                contactoJuntador = contactos[0];
            }
            for (int i = 1; i < contactos.Count; i++)
            {
                ContactoColor contactoFusion = contactos[i];
                foreach (String idContactoSimple in contactoFusion.ContactosSimple)
                {
                    ContactoSimple contactoSimple = Repositorio.ContactoSimpleCol[idContactoSimple];
                    if (!contactoJuntador.ContactosSimple.Contains(contactoSimple.IdContactoSimple))
                    {
                        contactoJuntador.ContactosSimple.Add(contactoSimple.IdContactoSimple);
                        contactoSimple.IdContactoColor = contactoJuntador.IdContactoColor;

                    }

                }
                Contactos.Remove(contactoFusion);
                Repositorio.ContactoColorCol.Remove(contactoFusion.IdContactoColor);
                addLog("Contacto fusionado " + contactoFusion.IdContactoColor);
                Modelos.SetActualizarContacto(contactoJuntador);
            }
            Modelos.SetActualizarListaContactos();
            Modelos.SetActualizarGruposContactos();
        }

        internal void SepararContactos(List<ContactoColor> contactos)
        {
            List<String> idCuentasActualizar = new List<string>();
            for (int i = 0; i < contactos.Count; i++)
            {

                ContactoColor contactoFusionado = contactos[i];
                if (contactoFusionado.ContactosSimple.Count > 1)
                {


                    ContactoSimple contactoSimple = Repositorio.ContactoSimpleCol[contactoFusionado.ContactosSimple[0]];

                    //separamos el resto
                    List<String> desFusionados = new List<String>(contactoFusionado.ContactosSimple);
                    desFusionados.RemoveAt(0);

                    //le dejamos solo el primero
                    contactoFusionado.ContactosSimple.Clear();
                    contactoFusionado.ContactosSimple.Add(contactoSimple.IdContactoSimple);

                    foreach (String idContactoSimple in desFusionados)
                    {
                        contactoSimple = Repositorio.ContactoSimpleCol[idContactoSimple];
                        NuevoContacto(contactoSimple);
                        idCuentasActualizar.Add(contactoSimple.IdCuenta);
                    }
                    Modelos.SetActualizarContacto(contactoFusionado);
                }
                addLog("Contacto separado " + contactoFusionado.IdContactoColor);
            }
            foreach (string idCuenta in idCuentasActualizar)
            {
                GetCuenta(idCuenta).ActualizarListaContactos();
            }
            Modelos.SetActualizarListaContactos();
            Modelos.SetActualizarGruposContactos();

        }

        
        internal string GetInfoUltimosMensajes(ContactoColor receptor)
        {
            Historial historialEnviado = Historiales[receptor.IdContactoSimpleEnviado];
            Historial historialRecibido = Historiales[receptor.IdContactoSimpleRecibido];
            var ultimoRecibido = historialRecibido.GetUltimoRecibido();
            var ultimoEnviado = historialEnviado.GetUltimoEnviado();


            var hacia = Repositorio.ContactoSimpleCol[receptor.IdContactoSimpleEnviado].GetNombrePublicoOJID();
            var desde = Repositorio.ContactoSimpleCol[receptor.IdContactoSimpleRecibido].GetNombrePublicoOJID();
            string info = "";
            if (ultimoRecibido != null)
            {
                info = string.Format("Ultimo Recibido: {0} ({1})", desde, ultimoRecibido.Tiempo.ToShortTimeString());
            }
            if (ultimoEnviado != null)
            {
                info += string.Format(" Ultimo Enviado: {0} ({1})", hacia, ultimoEnviado.Tiempo.ToShortTimeString());
            }
            return info;
        }
    }


}
