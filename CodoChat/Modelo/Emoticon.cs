﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CodoChat
{
    [Serializable()]
    public class Emoticon
    {
        public String Nombre { get; set; }
        public String Abreviatura { get; set; }
        public int Alto { get; set; }
        public int Ancho { get; set; }
        public String MD5 { get; set; }
        public bool Editable { get; set; }
        public string RTF { get; set; }
        public Image Imagen { get; set; }
        public static String directorio = Utiles.GetRutaApp()+"emoticones" + Path.DirectorySeparatorChar;
        public static String ext = ".png";

        public string IdEmoticon { get; set; }
        public Emoticon(string nombre, string abreviatura, Image imagen, bool editable)
        {
            this.Imagen = imagen;
            this.Nombre = nombre;
            this.Abreviatura = abreviatura;
            this.Alto = imagen.Height;
            this.Ancho = imagen.Width;
            this.Editable = editable;
            this.RTF = Utiles.CalcularRTF(imagen);
            this.MD5 = Utiles.CalcularMD5(imagen);
            this.IdEmoticon = Utiles.GenerarClave();
            imagen.Save(directorio + MD5);
        }
        internal void GuardarImagen()
        {
            Imagen.Save(directorio + MD5);
        }
        public bool Guardada()
        {
            return File.Exists(this.GetRuta());
        }

        public string GetRuta()
        {
            return directorio + MD5;
        }

    }
}
