﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using CodoChat.Modelo;
using System.Media;
namespace CodoChat
{
    [Serializable()]
    public class Preferencias
    {
        public bool OcultarABandeja { get; set; }

        public bool TitilarEnMensaje { get; set; }

        public bool SonidoEnConexion { get; set; }

        public bool SonidoAlRecibirMensaje { get; set; }
        public bool ContactosDesconectadosAparte { get; set; }

        public  Preferencias(){
            this.OcultarABandeja = false;
            this.SonidoAlRecibirMensaje = false;
            this.SonidoEnConexion = false;
            this.TitilarEnMensaje = true;
            this.ContactosDesconectadosAparte = true;
        }
        internal void ContactoConectado()
        {
            if (SonidoEnConexion)
            {
                SoundPlayer sndplayr = new SoundPlayer(CodoChat.Properties.Resources.online);
                sndplayr.Play();
            }
        }



        internal void MensajeRecibido(ContactoColor contacto)
        {
            if (SonidoAlRecibirMensaje)
            {
                VentanaChat ventana = GUI.ObtenerVentanaChat(contacto.IdContactoColor);
                bool sonar = false;
                if(ventana!=null)
                {
                   
                    if(ventana.WindowState== System.Windows.Forms.FormWindowState.Minimized)
                    {
                        sonar = true;
                    }
                }
                else
                {
                    sonar = true;
                }
                if(sonar)
                {
                    SoundPlayer sndplayr = new SoundPlayer(CodoChat.Properties.Resources.GTalkNotify);
                    sndplayr.Play();
                }

            }
            if(TitilarEnMensaje)
            {
                GUI.TitilarVentana(contacto.IdContactoColor);
            }
        }

        internal void MensajeErrorRecibido()
        {
            if (SonidoAlRecibirMensaje)
            {
                System.Media.SystemSounds.Exclamation.Play();
            }
        }


        internal bool AsignarGrupo(ContactoColor contacto)
        {
            Grupo grupoConectados = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Conectados);
            Grupo grupoDesconectados = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Desconectados);
            Grupo Todos = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Todos);
            Grupo Ocultos = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Ocultos);

            bool cambio = false;

            //no se mueve de este grupo
            if(contacto.Grupos.Contains(Ocultos.IdGrupo))
            {
                return false;
            }

            if (contacto.Estado != Estado.Desconectado)
            {
                if (contacto.Grupos.Contains(grupoDesconectados.IdGrupo))
                {
                    contacto.Grupos.Remove(grupoDesconectados.IdGrupo);
                    cambio = true;
                }

                if (!contacto.Grupos.Contains(grupoConectados.IdGrupo))
                {

                    contacto.Grupos.Add(grupoConectados.IdGrupo);
                    cambio = true;
                }

            }
            else
            {
                if (contacto.Grupos.Contains(grupoConectados.IdGrupo))
                {
                    contacto.Grupos.Remove(grupoConectados.IdGrupo);
                    cambio = true;
                }

                if (!contacto.Grupos.Contains(grupoDesconectados.IdGrupo))
                {
                    contacto.Grupos.Add(grupoDesconectados.IdGrupo);
                    cambio = true;
                }
            }
            return cambio;
        }
    }
}
