﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace CodoChat
{
	[Serializable()]
	public class Fuente
	{
		public Font Tipo { get; private set; }
		public Color Color { get; private set; }
		public float Size
		{
			get
			{
				return Tipo == null ? 0 : Tipo.Size;
			}
		}
		public Fuente()
		{
			
			Tipo =  new Font(SystemFonts.DefaultFont.Name, 18, FontStyle.Regular);
			Color = Color.Green;
		}
		public Fuente(Font font, Color color)
		{
			Tipo = font;
			Color = color;
		}
        public override bool Equals(object obj)
        {
            Fuente otra = (Fuente)obj;
            return otra.Color == this.Color &&
                Tipo.FontFamily == otra.Tipo.FontFamily &&
                Tipo.Size == otra.Tipo.Size &&
                Tipo.Bold == otra.Tipo.Bold &&
                Tipo.Italic == otra.Tipo.Italic;
        }
	}
}
