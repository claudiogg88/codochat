﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodoChat.Modelo
{
    [Serializable()]
    public class Historial
    {
        public List<Mensaje> Mensajes { get; set; } //recibidos desde este contacto

        public string IdContactoSimple { get; set; }


        public void GrabarADisco()
        {

        }

        public Historial(String idContactoSimple)
        {
            this.IdContactoSimple = idContactoSimple;
            this.Mensajes = new List<Mensaje>();
        }

        public void Recibido(String mensaje)
        {
            Mensajes.Add(new Mensaje(mensaje, Mensaje.TipoMensaje.Recibido));
        }
        public void Enviado(String mensaje)
        {
            Mensajes.Add(new Mensaje(mensaje, Mensaje.TipoMensaje.Enviado));
        }

        internal void RecibidoError(string mensaje)
        {
            Mensajes.Add(new Mensaje(mensaje, Mensaje.TipoMensaje.Error));
        }

        internal Mensaje GetUltimoRecibido()
        {
            return Mensajes.Where(d => d.Tipo == Mensaje.TipoMensaje.Recibido).LastOrDefault();
        }
        internal Mensaje GetUltimoEnviado()
        {
            return Mensajes.Where(d => d.Tipo == Mensaje.TipoMensaje.Enviado).LastOrDefault();
        }

        internal List<Mensaje> GetUltimosMensajes()
        {
            return Mensajes.Skip(Mensajes.Count - 6).ToList();
        }

    }
}
