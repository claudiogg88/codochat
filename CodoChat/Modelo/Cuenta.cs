﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using agsXMPP;
using agsXMPP.Collections;
using agsXMPP.protocol.extensions.caps;
using agsXMPP.protocol.iq.agent;
using agsXMPP.protocol.iq.disco;
using agsXMPP.protocol.iq.register;
using agsXMPP.protocol.iq.roster;
using agsXMPP.Xml.Dom;
using System.ComponentModel;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;
using agsXMPP.protocol.client;
using System.Drawing;
using agsXMPP.protocol.iq.version;
using agsXMPP.protocol.iq.vcard;

namespace CodoChat.Modelo
{
    [Serializable()]
    public class Cuenta
    {
        public String IdCuenta { get; set; } //clave unica de esta cuenta en la aplicacion
        public string NombreRed { get; set; }
        public String Usuario { get; set; }
        public String Clave { get; set; }
        public String Estado { get; set; }
        public Image Logo { get; set; }
        public String Servidor { get; set; }
        public String ServidorChat { get; set; }

        public Dictionary<String, ContactoSimple> Contactos;
        public ContactoSimple Yo;
        public DateTime ActualizacionListaContactos { get; set; }

        //se logueo alguna vez con esta cuenta?
        public Boolean Validada { get; set; }

        [NonSerialized]
        protected XmppClientConnection xcc;

        public Cuenta(string usuario, string clave)
        {
            this.Contactos = new Dictionary<String, ContactoSimple>();
            this.Estado = EstadoCuenta.Desconectado;
            this.Clave = clave;
            this.Usuario = usuario;

        }
        /// <summary>
        /// Login de este correo con las credenciales
        /// que se le asignaron, si las credenciales son nulas dara error
        /// </summary>
        public void Login()
        {

            try
            {
                if (Estado != CodoChat.EstadoCuenta.Conectado)
                {

                    xcc = new XmppClientConnection(this.Servidor);
                    if (ServidorChat != null)
                    {
                        xcc.ConnectServer = ServidorChat;
                    }
                    xcc.AutoAgents = false; //lo que sea no es necesario
                    xcc.AutoRoster = false; //no hace falta solicitar la lista de contactos en cada conexion
                    xcc.OnLogin += new ObjectHandler(OnLogin);
                    xcc.OnClose += new ObjectHandler(OnClose);
                    xcc.OnPasswordChanged += new ObjectHandler(OnPasswordCambio);
                    xcc.OnXmppConnectionStateChanged += xcc_OnXmppConnectionStateChanged;
                    xcc.OnSocketError += xcc_OnSocketError;
                    CuentaConectando();
                    xcc.Open(Usuario, Clave);
                    xcc.OnError += new ErrorHandler(xcc_OnError);
                    xcc.OnAuthError += new XmppElementHandler(ErrorDeLogin);

                }
            }
            catch (Exception e)
            {
                Utiles.ImprimirExcepcion(e);
                FalloCuenta(e.Message);
            }
        }

        void xcc_OnSocketError(object sender, Exception ex)
        {

            ColorMSN.Instancia.addLog("Socket Error!");
            ColorMSN.Instancia.addLog("Error no se puede conectar {0} revise su conexion de internet ", this);
            this.Estado = EstadoCuenta.PerdioConexion;
            ColorMSN.Instancia.CuentaPerdioConexion(this);
        }

        void xcc_OnXmppConnectionStateChanged(object sender, XmppConnectionState state)
        {
            ColorMSN.Instancia.addLog("Estado de la cuenta {0} cambio a {1}", this, state);
        }

        private void OnPasswordCambio(object sender)
        {
            this.Desconectar();
        }

        /// <summary>
        /// Evento disparado cuando se desconecta la cuenta
        /// </summary>
        /// <param name="sender"></param>
        private void OnClose(object sender)
        {
            CuentaDesconectada();
        }

        public void Iniciar()
        {
            //me subcribo a que me notifiquen cuando alguien se conecta o desconecta
            xcc.OnPresence += new PresenceHandler(OnPresence);


            if (Yo == null)
            {
                GenerarYo();
            }
            else
            {
                if (DeboActualizarYo())
                {
                    BuscarImagen(Yo.Jid);
                }

            }

            //me subscribo a los eventos de recuperar contactos, se recibiran cuando se ejecute el requestroster
            xcc.OnRosterItem += new agsXMPP.XmppClientConnection.RosterHandler(NuevoContacto); //cada vez que se recibe un nuevo contacto
            xcc.OnRosterEnd += new ObjectHandler(ListaDeContactosRecuperada); //cuando se termina de actualizar la lista de contactos

            if (DeboActualizarListaContactos())
            {
                this.ActualizarListaContactos();
            }
            //envio mi prescencia, accion necesaria para que el servidor comienze a enviar las presencias de mis contactos
            xcc.SendMyPresence();
        }

        /// <summary>
        /// solicita la lista de contactos
        /// </summary>
        public void ActualizarListaContactos()
        {
            //solicita la lista de contactos
            xcc.RequestRoster();
        }

        /// <summary>
        /// La cuenta comienza a intentar conectar
        /// </summary>
        private void CuentaConectando()
        {
            this.Estado = EstadoCuenta.Conectando;
            ColorMSN.Instancia.CuentaConectandose(this.IdCuenta);

        }
        private void CuentaDesconectada()
        {
            if (this.Estado != EstadoCuenta.Fallo)
            {
                //no fue una desconexion solicitada, sino un error
                if (this.Estado != EstadoCuenta.Desconectando)
                {
                    this.Estado = EstadoCuenta.PerdioConexion;
                    ColorMSN.Instancia.addLog("Cuenta {0} perdio conexion", this);
                    ColorMSN.Instancia.addLog("Reintentando conexion de la cuenta {0} ", this);
                    this.Login();
                }
                else
                {
                    this.Estado = EstadoCuenta.Desconectado;
                }
                ColorMSN.Instancia.CuentaDesconectada(this.IdCuenta);
            }
        }
        /// <summary>
        /// Evento disparado cuando se conecta satisfactoriamente
        /// </summary>
        private void CuentaConectada()
        {
            this.Estado = EstadoCuenta.Conectado;
            ColorMSN.Instancia.CuentaConectada(this.IdCuenta);
        }
        /// <summary>
        /// Le pedimos a la cuenta que se desconecte
        /// </summary>
        public void Desconectar()
        {
            //sirve para diferenciar desconexion pedida de desconexion por falla
            this.Estado = EstadoCuenta.Desconectando;
            xcc.Close();//inicio el cierre de conexion
            ColorMSN.Instancia.CuentaDesconectando(this.IdCuenta);

        }

        /// <summary>
        /// Escucho los eventos de cada uno de los contactos
        /// </summary>
        public void EscucharContactos()
        {
            foreach (ContactoSimple contacto in Contactos.Values)
            {
                EscucharContacto(contacto.Jid);
            }
        }

        /// <summary>
        /// Fallo la cuenta
        /// </summary>
        /// <param name="mensaje"> causa de la falla</param>
        public void FalloCuenta(string mensaje)
        {
            this.Estado = EstadoCuenta.Fallo;
            ColorMSN.Instancia.CuentaFallo(this, mensaje);
        }


        public bool DeboActualizarListaContactos()
        {
            //nunca se recupero la lista de contactos
            if (Contactos.Count == 0)
            {
                return true;
            }
            //paso mas de un dia desde la ultima actualizacion
            DateTime ayer = DateTime.Now.Subtract(TimeSpan.FromDays(1));
            return ActualizacionListaContactos < ayer;
        }

        public void ContactoComenzoODejoDeEscribir(string jid)
        {
            ContactoSimple contacto = BuscarOCrear(jid);
            ColorMSN.Instancia.ContactoComenzoODejoDeEscribir(contacto);
        }

        private void MensajeErrorRecibido(string jid, String error)
        {
            ContactoSimple contacto = BuscarOCrear(jid);
            ColorMSN.Instancia.MensajeErrorRecibido(contacto, error);
        }

        public void ContactoEnvioMensaje(string jid, string mensaje)
        {
            ContactoSimple contacto = BuscarOCrear(jid);
            ColorMSN.Instancia.ContactoEnvioMensaje(contacto, mensaje);
        }

        public void ListaContactosRecuperada()
        {
            ColorMSN.Instancia.ListaContactosRecuperada(IdCuenta);
            ActualizacionListaContactos = DateTime.Now;
        }

        public bool DeboActualizarYo()
        {
            DateTime unaHoraAntes = DateTime.Now.Subtract(TimeSpan.FromHours(1));
            return (Yo.Actualizado < unaHoraAntes);
        }
        public void NuevoContacto(string jid)
        {
            BuscarOCrear(jid);
        }

        public void GenerarYo(string jid)
        {
            Yo = new ContactoSimple(jid, IdCuenta, NombreRed);
            ColorMSN.Instancia.NuevaCuentaAsociada(Yo);
        }


        public void ActualizarNickContacto(string jid, String nick)
        {
            ContactoSimple contacto = BuscarOCrear(jid);
            if (!String.IsNullOrEmpty(nick))
            {
                if (contacto == null)
                {
                    Yo.Nick = nick;
                    ColorMSN.Instancia.ActualizarMiNick(Yo, nick);
                }
                else
                {
                    contacto.Nick = nick;
                    ColorMSN.Instancia.ActualizarNickContacto(contacto, nick);
                }
            }

        }
        public void ActualizarSubNickContacto(string jid, String subNick)
        {
            ContactoSimple contacto = BuscarOCrear(jid);
            if (contacto == null)
            {
                if (subNick != null)
                {
                    ColorMSN.Instancia.ActualizarMiSubNick(Yo, subNick);
                }
            }
            else
            {
                if (subNick != null)
                {
                    ColorMSN.Instancia.ActualizarSubNickContacto(contacto, subNick);
                }
            }
        }
        public void ActualizarEstadoContacto(string jid, Estado nuevoEstado)
        {
            ContactoSimple contacto = BuscarOCrear(jid);

            if (contacto == null)
            {
                ColorMSN.Instancia.ActualizarMiEstado(Yo, nuevoEstado);
                Yo.Estado = nuevoEstado;
            }
            else
            {

                ColorMSN.Instancia.ActualizarEstadoContacto(contacto, nuevoEstado);
                contacto.Estado = nuevoEstado;
            }
        }
        public void ActualizarImagenContacto(string jid, Image nuevaImagen)
        {
            ContactoSimple contacto = BuscarOCrear(jid);
            if(nuevaImagen!=null)
            {
                if (contacto == null)
                {

                    Yo.Imagen =  Adornos.CuadrarImagen(nuevaImagen);
                    ColorMSN.Instancia.ActualizarMiImagen(Yo, nuevaImagen);
                }
                else
                {
                    contacto.Imagen = Adornos.CuadrarImagen(nuevaImagen);
                    ColorMSN.Instancia.ActualizarImagenContacto(contacto, nuevaImagen);
                }
            }

        }
        public void ActualizarNombrePublicoContacto(string jid, string nombrePublico)
        {
            if (nombrePublico == null)
            {
                jid = nombrePublico;
            }
            ContactoSimple contacto = BuscarOCrear(jid);
            if (contacto == null)
            {
                Yo.NombrePublico = nombrePublico;
            }
            else
            {
                contacto.NombrePublico = nombrePublico;
            }
        }

        //deprecado por el momento
        public bool DeboActualizarContacto(string jid)
        {
            ContactoSimple contacto = BuscarOCrear(jid);
            DateTime unaHoraAntes = DateTime.Now.Subtract(TimeSpan.FromHours(1));
            return (contacto.Actualizado < unaHoraAntes);
        }
        /// <summary>
        /// Devuelve el contactosimple con este jid
        /// si el jid es igual al propio devuelve null
        /// </summary>
        /// <param name="jid"></param>
        /// <returns></returns>
        private ContactoSimple BuscarOCrear(string jid)
        {
            if (Yo.Jid.Equals(jid))
            {
                return null;
            }
            ContactoSimple contacto = null;

            if (Contactos.ContainsKey(jid))
            {
                contacto = Contactos[jid];
            }
            if (contacto == null)
            {
                contacto = new ContactoSimple(jid, this.IdCuenta, NombreRed);
                ColorMSN.Instancia.NuevoContacto(contacto);
                EscucharContacto(jid);
                Contactos.Add(jid, contacto);
            }
            return contacto;
        }


        public void ContactoDebeRecibirMensaje(string jid, string mensaje)
        {
            EnviarMensaje(jid, mensaje);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;
            Cuenta otra = (Cuenta)obj;
            return this.IdCuenta.Equals(otra.IdCuenta);
        }

        public override string ToString()
        {
            return this.Usuario == null ? this.NombreRed : this.IdCuenta;
        }

        public void ListoParaEscucharContactos()
        {
            //escucho los eventos de los contactos existentes
            EscucharContactos();
        }

        void ErrorDeLogin(object sender, Element e)
        {
            FalloCuenta("Clave o Usuario Erroneos");
        }

        void xcc_OnError(object sender, Exception ex)
        {
            FalloCuenta(ex.Message);
        }
        /// <summary>
        /// Evento disparado al conectarse
        /// provoca que se inicie el cliente.
        /// </summary>
        /// <param name="sender"></param>
        private void OnLogin(object sender)
        {
            Validada = true;
            CuentaConectada();
            ListoParaEscucharContactos();
            Iniciar();
        }


        /// <summary>
        /// Escuchar cuando este contacto me habla
        /// </summary>
        /// <param name="jid"></param>
        public void EscucharContacto(String jid)
        {
            xcc.MessageGrabber.Add(new Jid(jid), new BareJidComparer(), new MessageCB(EnMensajeRecibidoLocal), jid);

        }
        private void EnMensajeRecibidoLocal(object sender, agsXMPP.protocol.client.Message msg, object data)
        {


            //chat -- The message is sent in the context of a one-to-one chat conversation. A compliant client SHOULD present the message in an interface enabling one-to-one chat between the two parties, including an appropriate conversation history.
            //error -- An error has occurred related to a previous message sent by the sender (for details regarding stanza error syntax, refer to [XMPP‑CORE]). A compliant client SHOULD present an appropriate interface informing the sender of the nature of the error.
            //groupchat -- The message is sent in the context of a multi-user chat environment (similar to that of [IRC]). A compliant client SHOULD present the message in an interface enabling many-to-many chat between the parties, including a roster of parties in the chatroom and an appropriate conversation history. Full definition of XMPP-based groupchat protocols is out of scope for this memo (for details see [JEP‑0045]).
            //headline -- The message is probably generated by an automated service that delivers or broadcasts content (news, sports, market information, RSS feeds, etc.). No reply to the message is expected, and a compliant client SHOULD present the message in an interface that appropriately differentiates the message from standalone messages, chat sessions, or groupchat sessions (estilos.g., by not providing the recipient with the ability to reply).
            //normal -- The message is a single message that is sent outside the context of a one-to-one conversation or groupchat, and to which it is expected that the recipient will reply. A compliant client SHOULD present the message in an interface enabling the recipient to reply, but without a conversation history.


            String jid = (String)data;
            //Console.WriteLine("(" + jid + "):" + msg.Body);
            if (msg.Type == MessageType.error)
            {
                MensajeErrorRecibido(jid, msg.Error.Code.ToString());
                return;
            }
            if (string.IsNullOrEmpty(msg.Body)) //empezo  a escribir o dejo de escribir
            {
                //ContactoComenzoODejoDeEscribir(jid);
            }
            else
            {
                ContactoEnvioMensaje(jid, msg.Body);
            }
        }

        /// <summary>
        /// Termino de recuperarse la lista de contactos
        /// </summary>
        /// <param name="sender"></param>
        private void ListaDeContactosRecuperada(object sender)
        {
            ListaContactosRecuperada();
        }

        /// <summary>
        /// Metodo que se llama cada vez que se recupera un contacto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="item"></param>
        public void NuevoContacto(object sender, RosterItem item)
        {
            NuevoContacto(item.Jid.Bare);
            ActualizarNickContacto(item.Jid.Bare, item.Name);
            BuscarImagen(item.Jid.Bare);
            BuscarNombrePublico(item.Jid.Bare);
        }



        /// <summary>
        /// Generar mi propio contacto
        /// </summary>
        public void GenerarYo()
        {
            Jid mio = xcc.MyJID;
            GenerarYo(mio.Bare);
            BuscarImagen(mio.Bare);
            ActualizarNickContacto(mio.Bare, mio.User);
        }



        /// <summary>
        ///nueva presencia recibida desde el servidor
        ///presencia algo relacionado con el estado del contacto
        ///en la presencia viene un estado conectado/desconectado
        ///un Tipo de presencia disponible, ocupado, ausente, por volver
        ///un resumen de la foto de la vcard del contacto, para ver si la cambio
        /// </summary>
        private void OnPresence(object sender, Presence pres)
        {
            //estoy recibiendo actualizaciones de mi propio contacto
            if (Yo != null && pres.From.Bare.Equals(Yo.Jid))
            {
                return;
            }
            BuscarImagen(pres.From.Bare);
            NuevoContacto(pres.From.Bare);
            ActualizarSubNickContacto(pres.From.Bare, pres.Status);
            ActualizarEstadoContacto(pres.From.Bare, ConvertirAEstado(pres));

        }

        /// <summary>
        /// Solicito la vcard del contacto jid
        /// </summary>
        /// <param name="contacto"></param>
        /// <param name="VcardRecibida"></param>
        public void SolicitarVCard(String jid, Action<object, IQ, object> VcardRecibida)
        {
            VcardIq viq = new VcardIq(IqType.get, new Jid(jid));
            xcc.IqGrabber.SendIq(viq, new IqCB(VcardRecibida), jid);
        }


        //auxiliar para identificar el Tipo de presencia recibido
        private Estado ConvertirAEstado(Presence pres)
        {
            //away -- The entity or resource is temporarily away.
            //chat -- The entity or resource is actively interested in chatting.
            //dnd -- The entity or resource is busy (dnd = "Do Not Disturb").
            //xa -- The entity or resource is away for an extended period (xa = "eXtended Away").
            Estado devolver = CodoChat.Estado.Desconocido;
            if (pres.Type == PresenceType.unavailable)
                devolver = CodoChat.Estado.Desconectado;
            if (pres.Type == PresenceType.available)
                devolver = CodoChat.Estado.Online;
            if (pres.Show == ShowType.NONE)
                return devolver;
            else
            {
                if (pres.Show == ShowType.away)
                    devolver = CodoChat.Estado.Ausente;
                if (pres.Show == ShowType.chat)
                    devolver = CodoChat.Estado.Online;
                if (pres.Show == ShowType.dnd)
                    devolver = CodoChat.Estado.Ocupado;
                if (pres.Show == ShowType.xa)
                    devolver = CodoChat.Estado.Ausente;
            }
            return devolver;
        }


        public void EnviarMensaje(String jid, string msg)
        {

            if (this.Estado != EstadoCuenta.PerdioConexion)
            {
                xcc.Send(new agsXMPP.protocol.client.Message(new Jid(jid), MessageType.chat, msg));
            }
            else
            {
                MensajeErrorRecibido(jid, "Imposible enviar mensaje, no estas conectado");
            }

        }


        /// <summary>
        /// Buscar la imagen para este contacto
        /// </summary>
        /// <param name="jid"></param>
        public virtual void BuscarImagen(String jid)
        {
            SolicitarVCard(jid, VcardRecibida); //probamos con el otro metodo
        }

        public virtual void BuscarNombrePublico(String jid)
        {
            ActualizarNombrePublicoContacto(jid, jid);
        }


        public void VcardRecibida(object sender, IQ iq, object data)
        {
            String jid = (String)data;
            if (iq.Vcard != null)
            {
                ActualizarNickContacto(jid, iq.Vcard.Fullname);
                if (iq.Vcard.Photo != null)
                    ActualizarImagenContacto(jid, iq.Vcard.Photo.Image);
            }

        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return this.IdCuenta.GetHashCode();
        }

        internal void Limpiar()
        {
            this.ActualizacionListaContactos = new DateTime();
            this.Contactos.Clear();
        }
    }
    [Serializable()]
    public class CuentaGoogle : Cuenta
    {

        public CuentaGoogle(string usuario, string clave)
            : base(usuario, clave)
        {
            Logo = CodoChat.Properties.Resources.gmail_logo;
            IdCuenta = Usuario + "@" + NombreCliente.Google;
            NombreRed = NombreCliente.Google;
            Servidor = "gmail.com";
            ServidorChat = "talk.google.com";
            Repositorio.Agregar(this);
        }
    }

    //implementacion del cliente con comportamiento general
    [Serializable()]
    public class CuentaFacebook : Cuenta
    {
        private static WebClient wc = new WebClient();

        public CuentaFacebook(string usuario, string clave)
            : base(usuario, clave)
        {
            Logo = CodoChat.Properties.Resources.Facebook_logo; ;
            IdCuenta = Usuario + "@" + NombreCliente.Facebook;
            NombreRed = NombreCliente.Facebook;
            Servidor = "chat.facebook.com";
            ServidorChat = null;
            Repositorio.Agregar(this);
        }

        public override void BuscarImagen(String jid)
        {
            String usuario = jid.Substring(0, jid.IndexOf("@")).Replace("-", String.Empty);
            try
            {
                byte[] bytes = wc.DownloadData("https://graph.facebook.com/" + usuario + "/picture?type=large");
                Image nueva = Image.FromStream(new MemoryStream(bytes));
                ActualizarImagenContacto(jid, nueva);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                SolicitarVCard(jid, VcardRecibida); //probamos con el otro metodo
            }
        }
        public override void BuscarNombrePublico(String jid)
        {
            String usuario = jid.Substring(0, jid.IndexOf("@")).Replace("-", String.Empty);
            try
            {
                String datos = wc.DownloadString("https://graph.facebook.com/" + usuario);
                String nombrePublico = ((JsonObject)SimpleJson.DeserializeObject(datos))["username"].ToString(); ;
                ActualizarNombrePublicoContacto(jid, nombrePublico + "@chat.facebook.com");
            }
            catch
            {

            }
        }

    }
}