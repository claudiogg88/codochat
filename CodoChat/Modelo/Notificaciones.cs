﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.Modelo
{
    class Notificaciones
    {
        ///////////////////////////////////////////NOTIFICACIONES A LA GUI////////////////////////////////////////////////////
        /*

        /// <summary>
        /// Llamada al inicio del programa
        /// </summary>
        /// <param name="contacto"></param>
        /// <param name="grupos"></param>
        /// <param name="Contactos"></param>
        public static void Iniciar(ContactoColor contacto, List<Grupo> grupos, List<ContactoColor> Contactos, List<Cuenta> cuentas)
        {
            MiContactoFueCreado(contacto);
            GruposAgregados(grupos);
            ContactosAgregados(Contactos);
            CuentasAgregadas(cuentas);
        }
        internal static void CuentasAgregadas(List<Cuenta> cuentas)
        {
            foreach (Cuenta cuenta in cuentas)
            {
                CuentaAgregada(cuenta);
                CuentaCambioEstado(cuenta);
            }
        }
        internal static void ContactosAgregados(List<ContactoColor> contactos)
        {
            foreach (ContactoColor contacto in contactos)
            {
                ContactoAgregado(contacto);
            }
        }
        internal static void ContactoAgregado(ContactoColor contacto)
        {
            String grupo = "Desconectados";
            if (contacto.Estado == Estado.Desconectado)
            {
                grupo = "Desconectados";
            }
            else
            {
                grupo = "Conectados";
            }
            GUI.ContactoAgregado(grupo, contacto.IdContactoColor);
            ContactoCambioEstado(contacto);
            ContactoCambioImagen(contacto);
            ContactoCambioNick(contacto);
            ContactoCambioSubnick(contacto);
            ContactoCambioRedes(contacto);
        }

        internal static void MiContactoFueCreado(ContactoColor contacto)
        {
            GUI.MiImagenPerfilCambio(contacto.Imagen);
            GUI.MiEstadoCambio(Adornos.ObtenerBordeEstado(contacto.Estado));
            GUI.MiNickCambio(Adornos.AdornarNick(contacto));
            GUI.MiSubNickCambio(Adornos.AdornarSubNick(contacto));

        }


        internal static void ContactoCambioImagen(ContactoColor contacto)
        {
            GUI.ContactoCambioImagen(contacto.IdContactoColor, contacto.Imagen);
        }

        internal static void ContactoCambioEstado(ContactoColor contacto)
        {
            Image imageEstado = Adornos.ObtenerBordeEstado(contacto.Estado);
            GUI.ContactoCambioEstado(contacto.IdContactoColor, imageEstado);

        }
        //estaba desconectado y ahora esta en linea
        internal static void ContactoConectado(ContactoColor contacto, Perfil actual)
        {
            GUI.ContactoCambioGrupo(contacto.IdContactoColor, "Desconectados", "Conectados");
            if (actual.SonidoEnConexion)
            {
                GUI.SonidoEnContactoConectado();
            }
        }

        internal static void ContactoDesconectado(ContactoColor contacto)
        {

            GUI.ContactoCambioGrupo(contacto.IdContactoColor, "Conectados", "Desconectados");
        }
        internal static void ContactoCambioSubnick(ContactoColor contacto)
        {
            GUI.ContactoCambioSubnick(contacto.IdContactoColor, Adornos.AdornarSubNick(contacto));
        }

        internal static void ContactoCambioNick(ContactoColor contacto)
        {
            GUI.ContactoCambioNick(contacto.IdContactoColor, Adornos.AdornarNick(contacto));
            GUI.ContactoCambioNickConversacion(contacto.IdContactoColor, Adornos.AdornarNickConversacion(contacto));
        }

        internal static void MiContactoCambioImagen(ContactoColor contacto)
        {
            GUI.MiImagenPerfilCambio(contacto.Imagen);
        }

        internal static void MiContactoCambioEstado(ContactoColor contacto)
        {
            Image imageEstado = Adornos.ObtenerBordeEstado(contacto.Estado);
            GUI.MiEstadoCambio(imageEstado);
        }

        internal static void MiContactoCambioSubnick(ContactoColor contacto)
        {
            GUI.MiSubNickCambio(Adornos.AdornarSubNick(contacto));
        }

        internal static void MiContactoCambioNick(ContactoColor contacto)
        {
            GUI.MiNickCambio(Adornos.AdornarNick(contacto));
            GUI.MiNickConversacionCambio(Adornos.AdornarNickConversacion(contacto));
        }

        internal static void ContactoCambioListaEmoticones(ContactoColor contacto)
        {
            ContactoCambioSubnick(contacto);
            ContactoCambioNick(contacto);
        }

        internal static void ContactoCambioFuente(ContactoColor contacto)
        {
            GUI.ContactoCambioFuente(contacto.IdContactoColor, contacto.Fuente.Tipo, contacto.Fuente.Color);
        }

        internal static void MiFuenteFueActualizada(ContactoColor contacto)
        {
            GUI.MiFuenteCambio(contacto.Fuente.Tipo, contacto.Fuente.Color);
        }

        internal static void MiListaEmoticonesCambio(ContactoColor contacto)
        {
            MiContactoCambioNick(contacto);
            MiContactoCambioSubnick(contacto);
        }

        internal static void ContactoDebeRecibirMensaje(ContactoColor enviador, ContactoColor recibidor)
        {
            GUI.ContactoDebeRecibirMensaje(recibidor.IdContactoColor, Adornos.AdornarMensajeConversacion(enviador, ColorMSN.Instancia.Historial.GetUltimoEnviado().Texto));
        }

        internal static void ContactoEliminado(ContactoColor contacto)
        {
            GUI.ContactoEliminado(contacto.IdContactoColor);
        }

        internal static void CuentaEliminada(Cuenta cuenta)
        {
            GUI.CuentaEliminada(cuenta.IdCuenta);
        }

        internal static void CuentaAgregada(Cuenta cuenta)
        {
            GUI.CuentaAgregada(cuenta.IdCuenta, cuenta.NombreRed);
        }

        internal static void LogAgregado(EventoLog log)
        {
            GUI.LogAgregado(log.tiempo, log.Log);
        }


        internal static void ContactoEnvioMensaje(ContactoColor contacto, Perfil actual, string mensaje)
        {
            GUI.ContactoEnvioMensaje(contacto.IdContactoColor, Adornos.AdornarMensajeConversacion(contacto, mensaje), actual.SonidoAlRecibirMensaje, actual.TitilarEnMensaje);

        }


        internal static void ContactoDejoDeEscribir(ContactoColor contacto)
        {
            GUI.ContactoDejoDeEscribir(contacto.IdContactoColor);
        }

        internal static void ContactoComenzoAEscribir(ContactoColor contacto)
        {
            GUI.ContactoComenzoAEscribir(contacto.IdContactoColor);
        }

        internal static void CuentaFalloConexion(Cuenta cuenta, string mensaje)
        {
            GUI.CuentaCambioEstado(cuenta.IdCuenta, mensaje);
        }

        internal static void CuentaCambioEstado(Cuenta cuenta)
        {
            GUI.CuentaCambioEstado(cuenta.IdCuenta, cuenta.Estado);
        }

        internal static void GruposAgregados(List<Grupo> GruposContactos)
        {
            foreach (Grupo GrupoContactos in GruposContactos)
            {
                GUI.GrupoAgregado(GrupoContactos.Orden, GrupoContactos.Nombre);
            }
        }
        internal static void EmoticonAgregado(Emoticon nuevoEmoticon)
        {
            GUI.EmoticonAgregado(nuevoEmoticon.Abreviatura, nuevoEmoticon.Imagen);
        }

        internal static void EmoticonEditado(String idViejo, Emoticon nuevoEmoticon)
        {
            GUI.EmoticonEditado(idViejo, nuevoEmoticon.Abreviatura);
        }

        internal static void EmoticonEliminado(string idViejo)
        {
            GUI.EmoticonEliminado(idViejo);
        }


        internal static void ContactoCambioRedes(ContactoColor contacto)
        {
            List<String> redes = new List<string>();
            foreach (String idContactoSimple in contacto.ContactosSimple)
            {
                redes.Add(Repositorio.ContactoSimpleCol[idContactoSimple].NombreRed);
            }
            if (contacto.UsuarioColor != null)
            {
                redes.Add(NombreCliente.ColorMSN);
            }

            GUI.ContactoCambioRedes(contacto.IdContactoColor, redes);
        }
         */
    }
}
