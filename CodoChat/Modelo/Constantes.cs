﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodoChat
{

	public enum Estado
	{
		Desconocido = 0,
		Online = 1,
		Desconectado = 2,
		Ausente = 3,
		Ocupado = 4,
        Invisible = 5
	}
	class EstadoCuenta
	{
		public static readonly String Conectando = "Conectando";
		public static readonly String Conectado = "Conectado";
		public static readonly String Desconectado = "Desconectado";
        public static readonly String Desconectando = "Desconectando";
        public static readonly String PerdioConexion = "Perdio Conexion";
		public static readonly String Fallo = "Fallo";
	}
	class PropiedadContacto
	{
		public static readonly String Fuente = "Fuente";
		public static readonly String Emoticones = "Emoticones";
		public static readonly String Nick = "Nick";
		public static readonly String Subnick = "Subnick";
		public static readonly String Estado = "Estado";
		public static readonly String Usuario = "Usuario";
		public static readonly String Imagen = "Imagen";
		public static readonly String LinkContacto = "LinkContacto";
        public static readonly String Asociados = "Asociados";
        public static readonly String Perfil = "Perfil";
    }
	class NombreCliente
	{
		public static readonly string Facebook = "Facebook";
		public static readonly string Google = "Google";
		public static readonly string ColorMSN = "ColorMSN";
	}
    class NombreGrupo
    {
        public static readonly string Todos = "Todos";
        public static readonly string Ocultos = "Ocultos";
        public static readonly string Conectados = "Conectados";
        public static readonly string Desconectados = "Desconectados";
    }
}
