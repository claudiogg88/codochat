﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodoChat.Modelo
{
    [Serializable()]
    public class Grupo
    {
        public String Nombre { get; set; }
        public Boolean Editable { get; set; }
        public Boolean Oculto { get; set; }
        public Boolean Visible { get; set; }
        public int Orden { get; set; }

        public string IdGrupo;
        public Grupo(string nombre, int orden, Boolean editable, Boolean visible)
        {
            Editable = editable;
            IdGrupo = Utiles.GenerarClave();
            Nombre = nombre;
            Orden = orden;
            Oculto = false;
            Visible = visible;
        }
        public String Titulo { get { return Orden + "-" + Nombre; } }
    }
}
