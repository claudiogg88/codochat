﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using CodoChat.Modelo;
namespace CodoChat.Modelo
{
    [Serializable()]
    public class ContactoColor
    {

        public string IdContactoColor { get; set; } //id unico en la aplicacion
        public Image Imagen { get; set; }
        //el nick debera guardar rtf en vez de texto plano, pero sin imagenes
        public string Nick { get; set; }
        public string Subnick { get; set; }
        public Estado Estado { get; set; }
        public bool Escribiendo { get; set; }
        public string IdContactoSimpleEnviado { get; set; }

        public string IdContactoSimpleRecibido { get; set; }
        public List<String> Grupos { get; set; }

        /// <summary>
        /// ids de los contactos simple asociados a este contacto
        /// </summary>
        public List<String> ContactosSimple { get; set; } //
        public Fuente Fuente { get; set; }
        public Dictionary<String, Emoticon> Emoticones { get; set; }
        /// <summary>
        /// nombre del usuario en nuestro servidor
        /// </summary>
        public string UsuarioColor { get; set; } 
        /// <summary>
        /// indica ultima vez que se sincronizo con el servidor
        /// </summary>
        public DateTime UltimaActualizacion { get; set; } //

        /// <summary>
        /// indica si el servidor tiene una version mas nueva de este contacto
        /// </summary>
        [NonSerialized]
        public Boolean Actualizar = false;

        /// <summary>
        /// habilita no desabilita la sobrescritura de las propiedades por parte de las cuentas externas(facebook, google)
        /// </summary>
        public Boolean SobrescribirPropiedades { get; set; }

        [NonSerialized]
        public Boolean ConectadoDesdeCodoChat = false;
        public ContactoColor()
        {
            this.Nick = "";
            this.Subnick = "";
            this.IdContactoColor = Utiles.GenerarClave();
            this.Estado = Estado.Desconectado;
            this.Imagen = Adornos.GetImagenDefault();
            this.Escribiendo = false;
            this.Grupos = new List<string>();
            this.Emoticones = new Dictionary<string, Emoticon>();
            this.ContactosSimple = new List<String>();
            this.Fuente = new Fuente();
            this.UsuarioColor = null;
            this.SobrescribirPropiedades = true;
            this.UltimaActualizacion = new DateTime();//la fecha 0 sirve para q el contacto se actualize rapidamente desde el servidor
            this.Actualizar = false;
            this.Grupos.Add(ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Desconectados).IdGrupo);
            this.Grupos.Add(ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Todos).IdGrupo);
            Repositorio.Agregar(this);
        }
        public void AgregarContactoAsociado(ContactoSimple contactoSimple)
        {
            contactoSimple.IdContactoColor = this.IdContactoColor;
            this.ContactosSimple.Add(contactoSimple.IdContactoSimple);
        }


        internal void EliminarContactoAsociado(ContactoSimple contactoSimple)
        {
            this.ContactosSimple.Remove(contactoSimple.IdContactoSimple);
        }

        internal Boolean TieneRed(String NombreRed)
        {
            foreach(String idContactoSimple in ContactosSimple)
            {
                if (Repositorio.ContactoSimpleCol[idContactoSimple].NombreRed == NombreRed)
                {
                    return true;
                }
            }
            return false;
        }
        public override String ToString()
        {
            string nombre = null;
            if(ContactosSimple.Count>0)
            {
                nombre = this.GetContactoSimples().Count > 0 ? this.GetContactoSimples()[0].ToString() : null;
            }
            if(nombre == null)
            {
                nombre = this.IdContactoColor;
            }
            return nombre;
        }
        public List<ContactoSimple> GetContactoSimples()
        {
            List<ContactoSimple> simples = new List<ContactoSimple>();
            foreach(string idContactoSimple in ContactosSimple){
                if (Repositorio.ContactoSimpleCol.ContainsKey(idContactoSimple))
                {
                    simples.Add(Repositorio.ContactoSimpleCol[idContactoSimple]);
                }
                
            }
            return simples;
        }

        public List<Grupo> GetGrupos()
        {
            List<Grupo> grupos = new List<Grupo>();
            foreach (string idGrupo in Grupos)
            {
                if (Repositorio.GrupoCol.ContainsKey(idGrupo))
                {
                    grupos.Add(Repositorio.GrupoCol[idGrupo]);
                }

            }
            return grupos;
        }


        internal void ReiniciarYo()
        {
            this.Nick = Utiles.NickDefaultRTF("Mi nick :)");
            this.Subnick = Utiles.SubNickDefaultRTF("Mi subnick :O");
            this.Estado = Estado.Online;
            this.Fuente = new Fuente(new Font(SystemFonts.DefaultFont.Name, 18, FontStyle.Regular), Color.Blue);
            this.UltimaActualizacion = new DateTime();
            this.Emoticones = new Dictionary<string, Emoticon>();
        }
    }
}
