﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using CodoChat.Forms;
using CodoChat.Modelo;
using CodoChat;
using System.Threading;

using System.Net;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Specialized;
using System.Web;
using CodoChat.ModeloVista;

namespace CodoChat.Modelo
{


    public class Servidor
    {
        private System.Windows.Forms.Timer operador;
        private DateTime ultimaSubidaDeMiContacto;
        public List<Dictionary<String, object>> tareas;
        String cuentasAsociadas;
        private int numeroDeRonda;


        public enum Operacion
        {
            BajarContacto = 0,
            SubirMiContacto = 1,
            EnviarDatos = 2,
        }
        public enum EstadoTarea
        {
            Esperando = 0,
            Ejecutando = 1,
            Fallo = 2,
            Terminada = 3
        }

        //readonly static string HOST = "http://localhost/chat2/web/app_dev.php/personal/";
        readonly static string HOST = "http://181.168.167.249:8011/codophp/codochat/web/personal/";
        readonly static string TIPO = "tipo";
        readonly static string IDTAREA = "idtarea";
        readonly static string CONTACTO = "contacto";
        readonly static string ESTADO = "estado";
        readonly static string ERROR = "error";
        readonly static string DESCRIPCION = "descripcion";
        readonly static string NOHAYCUENTASASOCIADAS = "sincuentas";
        readonly static string MEDIDOR = "medidor";
        readonly static string RONDA = "ronda";
        readonly static string RUTA = "ruta";
        readonly static string RUTABAJAR = "bajar";
        readonly static string RUTASUBIR = "subir";
        readonly static string RUTAENVIARDATOS = "enviarDatos";
        readonly static string PARAMETROS = "parametros";
        readonly static string METODO = "metodo";
        readonly static string POST = "post";
        readonly static string SUBIRARCHIVO = "subir archivo";
        readonly static string BAJARARCHIVO = "bajar archivo";
        readonly static string NO = "no";
        readonly static string SI = "si";
        readonly static string PARAMETROSPOST = "parametrospost";
        readonly static string FECHAACTUALIZADO = "fecha_actualizado";
        readonly static string SUBIR = "subir";
        readonly static string CREADO = "creado";
        readonly static string RESPUESTA = "respuesta";
        readonly static string RUTAARCHIVO = "rutaarchivosubir";
        readonly static string RUTAYO = Utiles.GetRutaApp() + "yo.dat";
        readonly static string DATOS = "datos";
        readonly static string FECHAACTUAL = "fecha_actual";
        readonly static string ACTUALIZADO = "actualizado";
        readonly static string MISCUENTAS = "mis_cuentas";
        readonly static string CONTACTOS = "contactos";
        readonly static string CUENTASREPETIDAS = "cuentas_repetidas";
        readonly static string CLAVE = "clave";
        readonly static string NUEVACLAVE = "nueva_clave";
        readonly static string BAJAR = "bajar";


        public void Login()
        {
            ultimaSubidaDeMiContacto = DateTime.Now;
            tareas = new List<Dictionary<string, object>>();
            operador = new System.Windows.Forms.Timer();
            operador.Tick += new EventHandler(EnOperar);
            operador.Interval = (1000) * (5); //enviar datos
            operador.Enabled = true;
            operador.Start();

            numeroDeRonda = 0;
            EnOperar(null, null);
        }

        private void EnOperar(object sender, EventArgs e)
        {
            Thread tareas = new Thread(Ronda);
            tareas.Start();
        }
        private void Ronda()
        {
            //FASE 1
            //Esto crea todas las tareas, pero no las ejecuta
            if (ColorMSN.Instancia.HayCuentasAsociadas())
            {
                if (!HayTareasPendientes())
                {
                    EnviarDatos();
                    RealizarTareas();
                    numeroDeRonda++;
                }
                else
                {
                    ColorMSN.Instancia.addLog("No se enviario datos, Todavia hay tareas pendientes");
                }
            }
            else
            {
                ColorMSN.Instancia.addLog("No se enviario datos, No hay cuentas asociadas");
            }

        }

        private void SubirMiContacto()
        {
            ContactoColor contactoColor = ColorMSN.Instancia.Yo;
            Utiles.GuardarContacto(contactoColor);
            Dictionary<String, object> tarea = new Dictionary<string, object>();
            tarea[TIPO] = Operacion.SubirMiContacto;
            tarea[CONTACTO] = contactoColor;
            tarea[ESTADO] = EstadoTarea.Esperando;
            tarea[RUTA] = RUTASUBIR;
            tarea[METODO] = SUBIRARCHIVO;
            tarea[RUTAARCHIVO] = RUTAYO;
            tarea[DESCRIPCION] = String.Format("Mi contacto con clave {0} fue actualizado el {1} y debe ser subido al servidor", contactoColor.UsuarioColor, contactoColor.UltimaActualizacion);
            tarea[PARAMETROS] = String.Format("/{0}/{1}/{2}/", contactoColor.UsuarioColor, Utiles.segundosDesdeEl2000(contactoColor.UltimaActualizacion), contactoColor.Estado);
            AgregarTarea(tarea);
        }
        /// <summary>
        /// Bajar contacto desde el servidor
        /// </summary>
        /// <param name="contacto"></param>
        public void BajarContacto(ContactoColor contacto, DateTime fechaEnServidor)
        {
            Dictionary<String, object> tarea = new Dictionary<string, object>();
            tarea[TIPO] = Operacion.BajarContacto;
            tarea[CONTACTO] = contacto;
            tarea[ESTADO] = EstadoTarea.Esperando;
            tarea[FECHAACTUALIZADO] = fechaEnServidor;
            tarea[RUTA] = RUTABAJAR;
            tarea[DESCRIPCION] = String.Format("El contacto {2} con clave {0} fue actualizado por ultima vez el {1} y el servidor tiene una version de la fecha {3}", contacto.UsuarioColor, contacto.UltimaActualizacion, contacto, fechaEnServidor);
            tarea[METODO] = BAJARARCHIVO;
            if (contacto.UsuarioColor == null)
            {
                throw new CodoServerException(String.Format("No se puede crear tarea bajar contacto para el contacto {0}, no tiene usuario color ", contacto));
            }
            tarea[PARAMETROS] = String.Format("/{0}/", contacto.UsuarioColor);
            AgregarTarea(tarea);
        }

        /*
         *
         * {
         *      nuevaclave : NO //no o la clave
         *      subir : NO, //subir contacto si o no
         *      bajar : NO //bajar contacto si o no
         *      contactos :
         *      [
         *              {
         *               contacto : nombrepublico
         *               clave : clavecolor
         *               bajar : NO
         *               estado : estado
         *              },
         *              {
         *               contacto : nombrepublico
         *               clave : clavecolor
         *               bajar : SI
         *               estado : estado
         *              }
         *      ]
         * }
         * {
         *      clave : usuariocolor 
         *      fecha_actual : int,
         *      actualizado : int, //ultima actualizacion de mi contacto
         *      mis_cuentas : [ cuenta1, cuenta2  ], //todas mis cuentas validadas
         *      estado : estado, //mi estado
         *      contactos :
         *      [
         *              {
         *               contacto : nombrepublico
         *               actualizado : int //fecha en que actualize este contacto por ultima vez
         *              },
         *              {
         *               contacto : jidcontacto
         *               actualizado : int
         *              }
         *      ]
         * }
         * 
         * 
         */
        public void EnviarDatos()
        {
            ContactoColor contacto = ColorMSN.Instancia.Yo;
            Dictionary<String, object> tarea = new Dictionary<string, object>();
            NameValueCollection parametrosPost = new NameValueCollection();
            parametrosPost.Add(DATOS, CrearJSONDatos());

            tarea[TIPO] = Operacion.EnviarDatos;
            tarea[ESTADO] = EstadoTarea.Esperando;
            tarea[RUTA] = RUTAENVIARDATOS;
            tarea[METODO] = POST;
            tarea[PARAMETROSPOST] = parametrosPost;
            tarea[PARAMETROS] = "/";
            tarea[DESCRIPCION] = String.Format("Enviar mi estado al servidor... ");
            AgregarTarea(tarea);
        }

        private void AgregarTarea(Dictionary<string, object> tarea)
        {
            tarea[IDTAREA] = Utiles.GenerarClave(5);
            tarea[RONDA] = numeroDeRonda;
            ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}-{5}", tarea[RONDA], tarea[IDTAREA], "A", tarea[TIPO], tarea[DESCRIPCION], tarea[PARAMETROS]);
            tareas.Add(tarea);
        }

        private void FinalizarTarea(Dictionary<string, object> tarea)
        {
            Stopwatch sp = (Stopwatch)tarea[MEDIDOR];
            sp.Stop();
            long tiempo = sp.ElapsedMilliseconds / 1000;
            tarea[ESTADO] = EstadoTarea.Terminada;
            ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}s", tarea[RONDA], tarea[IDTAREA], "T", tarea[TIPO], tiempo);
        }

        private void FalloTarea(Dictionary<string, object> tarea, Exception e)
        {
            Stopwatch sp = (Stopwatch)tarea[MEDIDOR];
            sp.Stop();
            long tiempo = sp.ElapsedMilliseconds / 1000;
            ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}s-{5}", tarea[RONDA], tarea[IDTAREA], "F", tarea[TIPO], tiempo, e.Message);
            if (tarea[RESPUESTA] != null && tarea[RESPUESTA].GetType() == typeof(byte[]))
            {
                ColorMSN.Instancia.addLog("Respuesta:\n {0}", Encoding.ASCII.GetString((byte[])tarea[RESPUESTA]));
            }
            else
            {
                ColorMSN.Instancia.addLog("Respuesta:\n {0}", tarea[RESPUESTA]);
            }

            tarea[ESTADO] = EstadoTarea.Fallo;
            tarea[ERROR] = e;
        }

        private void RealizarTareas()
        {

            for (int i = 0; i < tareas.Count; i++)
            {
                Dictionary<string, object> tarea = tareas[i];
                EstadoTarea estado = (EstadoTarea)tarea[ESTADO];
                if (estado == EstadoTarea.Esperando)
                {
                    try
                    {


                        bool completado = RealizarTarea(tarea);
                        //la tarea es sincronica, los metodos procesar y finalizar seran llamados en otra parte
                        if (completado)
                        {
                            ProcesarRespuesta(tarea);
                            FinalizarTarea(tarea);
                        }

                    }
                    catch (Exception e)
                    {
                        FalloTarea(tarea, e);
                    }
                }

            }
        }

        private bool RealizarTarea(Dictionary<string, object> tarea)
        {
            ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}", tarea[RONDA], tarea[IDTAREA], "R", tarea[TIPO]);
            WebClient cliente = new WebClient();
            string ruta = tarea[RUTA].ToString();
            string parametros = tarea[PARAMETROS].ToString();
            string metodo = tarea[METODO].ToString();
            string url = HOST + ruta + parametros;
            tarea[MEDIDOR] = Stopwatch.StartNew();
            tarea[RESPUESTA] = null;
            tarea[ESTADO] = EstadoTarea.Ejecutando;
            Boolean terminada = false;

            if (metodo == POST)
            {
                var postParams = (NameValueCollection)tarea[PARAMETROSPOST];
                byte[] datos = cliente.UploadValues(url, "POST", postParams);
                tarea[RESPUESTA] = Encoding.UTF8.GetString(datos);
                terminada = true;
            }
            if (metodo == BAJARARCHIVO)
            {

                cliente.DownloadDataAsync(new Uri(url), tarea);
                cliente.DownloadProgressChanged += (obj, e) =>
                    {
                        //ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}/{5} KB descargados {6}% completado", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], e.BytesReceived / 1024, e.TotalBytesToReceive / 1024, e.ProgressPercentage);

                    };
                cliente.DownloadDataCompleted += cliente_DownloadDataCompleted;
                terminada = false;
            }
            if (metodo == SUBIRARCHIVO)
            {
                string rutaYo = (string)tarea[RUTAARCHIVO];

                long tam = new FileInfo(rutaYo).Length;
                ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-Intentando subir {4} KB Asincronicamente", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], tam / 1024);
                cliente.UploadFileCompleted += cliente_UploadFileCompleted;
                cliente.UploadFileAsync(new Uri(url), POST, rutaYo, tarea);
                cliente.UploadProgressChanged += (obj, e) =>
                {
                    //ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}/{5} KB enviados...", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], e.BytesSent / 1024, tam / 1024);
                };
                terminada = false;
            }
            return terminada;

        }

        private void ProcesarRespuesta(Dictionary<string, object> tarea)
        {

            ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}", tarea[RONDA], tarea[IDTAREA], "P", tarea[TIPO]);
            Operacion tipo = (Operacion)tarea[TIPO];

            switch (tipo)
            {
                case Operacion.BajarContacto:
                    {
                        var arreglo = (byte[])tarea[RESPUESTA];
                        ContactoColor contacto = (ContactoColor)Utiles.ByteArrayToObject(arreglo);
                        contacto.UltimaActualizacion = (DateTime)tarea[FECHAACTUALIZADO];
                        ContactoColor propio = (ContactoColor)tarea[CONTACTO];
                        ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], String.Format("El Contacto {0} ha sido bajado correctamente", propio));
                        MimetizarContacto(propio, contacto);
                    } break;
                case Operacion.SubirMiContacto:
                    {
                        var arreglo = (byte[])tarea[RESPUESTA];
                        string respuesta = Encoding.ASCII.GetString(arreglo);
                        if (respuesta == SI || respuesta == CREADO)
                        {
                            ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], String.Format("Mi Contacto ha sido subido correctamente {0}", respuesta));
                            break;
                        }

                        throw new CodoServerException("Error al subir mi contacto, error desconocido");
                    };
                case Operacion.EnviarDatos:
                    {
                        string respuesta = tarea[RESPUESTA].ToString();
                        ProcesarRespuestaEnviarDatos(respuesta, tarea);
                        break;
                    }
            }
        }

        private void ProcesarRespuestaEnviarDatos(String respuesta, Dictionary<string, object> tarea)
        {
            //operador.Interval = (1000) * (500); //enviar datos
            JsonObject datos = (JsonObject)SimpleJson.DeserializeObject(respuesta);
            //ColorMSN.Instancia.addLog(respuesta);

            String nuevaClave = datos[NUEVACLAVE].ToString();
            String actualizado = datos[ACTUALIZADO].ToString();
            if (nuevaClave != NO)
            {
                ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], String.Format("Se recibio nueva clave {0} desde el servidor", nuevaClave));
                ColorMSN.Instancia.Yo.UsuarioColor = nuevaClave;
            }
            string bajar = datos[BAJAR].ToString();
            if (bajar == SI)
            {
                ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], "El servidor me informa que debo actualizar mi contacto");
                BajarContacto(ColorMSN.Instancia.Yo, Utiles.fechaSegundosDesdeEl2000(actualizado));
            }
            string subir = datos[SUBIR].ToString();
            if (subir == SI)
            {
                ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], "El servidor me informa que debo subir mi contacto");
                this.cuentasAsociadas = null;
                SubirMiContacto();
            }

            JsonArray contactos = (JsonArray)datos[CONTACTOS];
            foreach (JsonObject jsonContacto in contactos)
            {
                if (jsonContacto[CONTACTO] == null)
                {
                    continue;
                }
                String nombrePublico = jsonContacto[CONTACTO].ToString();
                ContactoColor contacto = Repositorio.BuscarPorNombrePublico(nombrePublico);
                if (contacto == null)
                {
                    continue;
                }

                String clave = jsonContacto[CLAVE].ToString();
                String strEstado = jsonContacto[ESTADO].ToString();
                bajar = jsonContacto[BAJAR].ToString();
                actualizado = jsonContacto[ACTUALIZADO].ToString();
                if (clave != NO)
                {
                    Estado nuevo = (Estado)Enum.Parse(typeof(Estado), strEstado, true);
                    ColorMSN.Instancia.ActualizarEstadoContactoDesdeServidor(nuevo, contacto);
                    //desde antes era un usuario color
                    if (clave == contacto.UsuarioColor)
                    {
                        ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], String.Format("El contacto {0} es un contacto color y su estado es {1}", nombrePublico, nuevo));
                    }
                    else
                    {
                        ColorMSN.Instancia.NuevoContactoColor(contacto, clave);
                    }

                    if (bajar == SI)
                    {

                        BajarContacto(contacto, Utiles.fechaSegundosDesdeEl2000(actualizado));
                    }
                }
                else
                {
                    //antes era un usuariocolor
                    if (contacto.UsuarioColor != null)
                    {
                        if(contacto.ContactosSimple.Count == 1)
                        {
                            contacto.UsuarioColor = null;
                            ColorMSN.Instancia.addLog("{0}-{1}-{2}-{3}-{4}", tarea[RONDA], tarea[IDTAREA], "D", tarea[TIPO], String.Format("El contacto  {0} no fue encontrado en el servidor ya no es un contacto color :(", contacto));
                            Modelos.SetActualizarContacto(contacto);
                            ColorMSN.Instancia.ActualizarEstadoContactoDesdeServidor(Estado.Desconectado, contacto);
                            ColorMSN.Instancia.BajaContactoColor(contacto);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// asigna las propiedades actualizadas
        /// al contacto propio
        /// </summary>
        /// <param name="propio">el que tenemos en memoria</param>
        /// <param name="actualizado">el que recibimos desde el servidor</param>
        private void MimetizarContacto(ContactoColor propio, ContactoColor actualizado)
        {
            //asignamos la hora en que fue actualizado por ultima vez
            propio.UltimaActualizacion = actualizado.UltimaActualizacion;
            //el contacto no debe ser actualizado desde los demas clientes
            ColorMSN.Instancia.DeshabilitarSobrescritura(propio);
            if (propio == ColorMSN.Instancia.Yo)
            {
                //ColorMSN.Instancia.ActualizarMiEstado(); no actualizar aca, porque sino cree que el contacto esta siempre actualizado
                ColorMSN.Instancia.ActualizarMiFuenteDesdeServidor(actualizado.Fuente);
                ColorMSN.Instancia.ActualizarMiNickDesdeServidor(actualizado.Nick);
                ColorMSN.Instancia.ActualizarMiSubNickDesdeServidor(actualizado.Subnick);
                ColorMSN.Instancia.ActualizarMiImagenDesdeServidor(actualizado.Imagen);
                ColorMSN.Instancia.ActualizarMisEmoticonesDesdeServidor(actualizado.Emoticones);
            }
            else
            {
                //ColorMSN.Instancia.ActualizarMiEstado();  no actualizar aca, porque sino cree que el contacto esta siempre actualizado
                ColorMSN.Instancia.ActualizarFuenteContactoDesdeServidor(actualizado.Fuente, propio);
                ColorMSN.Instancia.ActualizarNickContactoDesdeServidor(actualizado.Nick, propio);
                ColorMSN.Instancia.ActualizarSubNickContactoDesdeServidor(actualizado.Subnick, propio);
                ColorMSN.Instancia.ActualizarImagenContactoDesdeServidor(actualizado.Imagen, propio);
                ColorMSN.Instancia.ActualizarEmoticonesContactoDesdeServidor(actualizado.Emoticones, propio);
            }
        }


        void cliente_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            var algo = e.UserState;
            Dictionary<string, object> tarea = (Dictionary<string, object>)algo;
            try
            {
                tarea[RESPUESTA] = e.Result;
                ProcesarRespuesta(tarea);
                FinalizarTarea(tarea);
            }
            catch (Exception ex)
            {
                FalloTarea(tarea, ex);
            }
        }


        void cliente_UploadFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            var algo = e.UserState;
            Dictionary<string, object> tarea = (Dictionary<string, object>)algo;
            if (e.Error != null)
            {
                FalloTarea(tarea, e.Error);
            }
            else
            {
                try
                {
                    tarea[RESPUESTA] = e.Result;
                    ProcesarRespuesta(tarea);
                    FinalizarTarea(tarea);
                }
                catch (Exception ex)
                {
                    FalloTarea(tarea, ex);
                }
            }
        }



        private class CodoServerException : Exception
        {
            public CodoServerException(string mensaje)
                : base(mensaje)
            {

            }
        }
        /// <summary>
        /// Tengo que avisar que me desconecte
        /// </summary>
        public void Logout()
        {
            EnviarDatos();
        }
        private bool HayTareasPendientes()
        {
            return tareas.Count(d => d[ESTADO].ToString() == EstadoTarea.Esperando.ToString() || d[ESTADO].ToString() == EstadoTarea.Ejecutando.ToString()) != 0;
        }
        public string CrearJSONDatos()
        {
            ContactoColor yo = ColorMSN.Instancia.Yo;
            JsonObject datos = new JsonObject();
            datos[CLAVE] = yo.UsuarioColor;
            datos[FECHAACTUAL] = Utiles.segundosDesdeEl2000(DateTime.Now);
            datos[ACTUALIZADO] = Utiles.segundosDesdeEl2000(yo.UltimaActualizacion);
            datos[MISCUENTAS] = JSONCuentasConectadas();
            datos[ESTADO] = yo.Estado.ToString();
            datos[CONTACTOS] = JSONContactos();
            return datos.ToString();

        }

        private object JSONCuentasConectadas()
        {
            if (ColorMSN.Instancia.HayCuentasAsociadas())
            {
                JsonArray datos = new JsonArray();
                foreach (Cuenta asociada in ColorMSN.Instancia.Asociadas)
                {
                    if (asociada.Validada)
                    {
                        datos.Add(asociada.Yo.Jid);
                    }
                }
                if (cuentasAsociadas == datos.ToString())
                {
                    return CUENTASREPETIDAS;
                }
                else
                {
                    cuentasAsociadas = datos.ToString();
                    return datos;
                }
            }
            else
            {
                return NOHAYCUENTASASOCIADAS;
            }
        }


        private object JSONContactos()
        {
            JsonArray datos = new JsonArray();
            foreach (Cuenta asociada in ColorMSN.Instancia.Asociadas)
            {
                if (asociada.Validada)
                {
                    foreach (ContactoSimple contacto in asociada.Contactos.Values)
                    {
                        JsonObject con = new JsonObject();
                        ContactoColor color = Repositorio.BuscarPorContactoSimple(contacto);
                        con[CONTACTO] = contacto.NombrePublico;
                        con[ACTUALIZADO] = Utiles.segundosDesdeEl2000(color.UltimaActualizacion);
                        datos.Add(con);
                    }
                }
            }
            return datos;
        }
    }

}
