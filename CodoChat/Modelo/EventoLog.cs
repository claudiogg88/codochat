﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodoChat.Modelo
{
	 [Serializable()]
	public class EventoLog
	{
		public string Log { get; set; }
		public DateTime tiempo {get;set;}

		public EventoLog(string log)
		{
			this.Log = log;
			tiempo = DateTime.Now;
		}
		public override string ToString()
		{
			return "[" + tiempo.ToString() + "]\t"+ Log;
		}
	}
}
