﻿using CodoChat.ModeloVista;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.Modelo
{
    /// <summary>
    /// ClienteExterno-->ColorMSN
    /// Notificaciones desde los clientes externos (facebook, gmail)
    /// </summary>
    partial class ColorMSN
    {

        ///////////////////////////////NOTIFICACIONES DE LA CUENTA///////////////////////////////////////////////////

        internal void CuentaFallo(Cuenta cuenta, String mensaje)
        {
            addLog("Cuenta Fallo " + cuenta.IdCuenta + " causa: " + mensaje);
            Modelos.modeloListaCuenta.Actualizar = true;
        }
        internal void CuentaPerdioConexion(Cuenta cuenta)
        {
            addLog("Cuenta Perdio conexion {0}", cuenta);
            Modelos.modeloListaCuenta.Actualizar = true;
        }

        public void ListaContactosRecuperada(string idCuenta)
        {
            addLog("Lista de contactos recuperada en " + idCuenta);
        }


        internal void CuentaConectandose(string idCuenta)
        {
            Cuenta cuenta = this.GetCuenta(idCuenta);
            if (cuenta != null)
            {
                addLog("Cuenta: " + idCuenta + " conectandose ");
                Modelos.modeloListaCuenta.Actualizar = true;
            }
        }
        internal void CuentaPerdioConexion(string idCuenta)
        {
            Cuenta cuenta = this.GetCuenta(idCuenta);
            if (cuenta != null)
            {
                addLog("Cuenta: " + idCuenta + " conectandose ");
                Modelos.modeloListaCuenta.Actualizar = true;
            }
        }
        internal void CuentaReConectandose(string idCuenta)
        {
            Cuenta cuenta = this.GetCuenta(idCuenta);
            if (cuenta != null)
            {
                addLog("Cuenta: " + idCuenta + " reconectandose ");
                Modelos.modeloListaCuenta.Actualizar = true;
            }
        }
        internal void CuentaDesconectada(string idCuenta)
        {
            Cuenta cuenta = this.GetCuenta(idCuenta);
            if (cuenta != null)
            {
                addLog("Cuenta: " + idCuenta + " desconectada ");
                DesconectarContactosDeCuenta(cuenta);
                Modelos.modeloListaCuenta.Actualizar = true;
            }

        }
        private void DesconectarContactosDeCuenta(Cuenta cuenta)
        {
            foreach (ContactoSimple externo in cuenta.Contactos.Values)
            {
                ContactoColor contacto = BuscarPorContactoSimple(externo);
                if (contacto.SobrescribirPropiedades)
                {
                    Estado viejo = contacto.Estado;
                    contacto.Estado = Estado.Desconectado;
                    Modelos.SetActualizarContacto(contacto);
                }

            }
        }
        internal void CuentaDesconectando(string idCuenta)
        {
            Cuenta cuenta = this.GetCuenta(idCuenta);
            if (cuenta != null)
            {
                addLog("Cuenta: " + idCuenta + " desconectandose...");
                Modelos.modeloListaCuenta.Actualizar = true;
            }
        }
        internal void CuentaConectada(string idCuenta)
        {
            Cuenta cuenta = this.GetCuenta(idCuenta);
            if (cuenta != null)
            {
                addLog("Cuenta: " + idCuenta + " conectada ");
                Modelos.modeloListaCuenta.Actualizar = true;
            }
        }

        ///////////////////////////////////ACTUALIZACIONES A MI CONTACTO//////////////////////////////////

        public void ActualizarMiNick(ContactoSimple yoExterno, String nick)
        {
            if (Yo.SobrescribirPropiedades)
            {
                String nuevoNickRtf = Utiles.NickDefaultRTF(nick);
                if (!Yo.Nick.Equals(nuevoNickRtf))
                {
                    Yo.Nick = nuevoNickRtf;
                    Modelos.SetActualizarYo();

                    addLog("Cuenta: " + yoExterno.IdCuenta + " Cambio mi nick ");

                }
            }

        }
        public void ActualizarMiSubNick(ContactoSimple yoExterno, String subnNick)
        {
            if (Yo.SobrescribirPropiedades)
            {
                String nuevoSubnickRtf = Utiles.SubNickDefaultRTF(subnNick);
                if (!Yo.Subnick.Equals(nuevoSubnickRtf))
                {
                    Yo.Subnick = nuevoSubnickRtf;
                    Modelos.SetActualizarYo();
                    addLog("Cuenta: " + yoExterno.IdCuenta + " Cambio mi subnick ");
                }
            }

        }
        public void ActualizarMiEstado(ContactoSimple yoExterno, Estado nuevoEstado)
        {
            if (Yo.SobrescribirPropiedades)
            {
                if (Yo.Estado != nuevoEstado)
                {
                    Yo.Estado = nuevoEstado;
                    Modelos.SetActualizarYo();
                    addLog("Cuenta: " + yoExterno.IdCuenta + " Cambio mi estado ");

                }
            }
        }
        public void ActualizarMiImagen(ContactoSimple yoExterno, Image nuevaImagen)
        {
            if (Yo.SobrescribirPropiedades)
            {
                Image cuadrada = Adornos.CuadrarImagen(nuevaImagen);
                if (!Adornos.ImagenesIguales(Yo.Imagen, cuadrada))
                {
                    Yo.Imagen = cuadrada;
                    Modelos.SetActualizarYo();
                    addLog("Cuenta: " + yoExterno.IdCuenta + " Cambio mi imagen ");

                }
            }
        }

        /////////////////////////ACTUALIZACIONES A LOS CONTACTOS///////////////////////////////////////
        public void ActualizarNickContacto(ContactoSimple contactoSimple, String nick)
        {
            ContactoColor contacto = BuscarPorContactoSimple(contactoSimple);
            if (contacto != null && contacto.SobrescribirPropiedades)
            {
                String nuevoNick = Utiles.NickDefaultRTF(nick);
                if (!contacto.Nick.Equals(nuevoNick))
                {
                    contacto.Nick = nuevoNick;
                    Modelos.SetActualizarContacto(contacto);
                    addLog("Cuenta: " + contactoSimple.IdCuenta + " Cambio el nick del contacto " + contacto);
                }
            }
        }
        public void ActualizarSubNickContacto(ContactoSimple contactoSimple, String subnick)
        {
            ContactoColor contacto = BuscarPorContactoSimple(contactoSimple);
            if (contacto != null && contacto.SobrescribirPropiedades)
            {
                String nuevoSubNick = Utiles.NickDefaultRTF(subnick);
                if (!contacto.Subnick.Equals(nuevoSubNick))
                {
                    contacto.Subnick = nuevoSubNick;
                    Modelos.SetActualizarContacto(contacto);
                    addLog("Cuenta: " + contactoSimple.IdCuenta + " Cambio el subnick del contacto " + contacto);

                }
            }
        }
        public void ActualizarEstadoContacto(ContactoSimple contactoSimple, Estado nuevoEstado)
        {
            ContactoColor contacto = BuscarPorContactoSimple(contactoSimple);
            if (contacto != null && !contacto.ConectadoDesdeCodoChat)
            {
                if (contacto.Estado != nuevoEstado)
                {

                    Estado viejo = contacto.Estado;
                    contacto.Estado = nuevoEstado;

                    if (viejo == Estado.Desconectado)
                    {
                        this.GetPreferencias().ContactoConectado();
                        bool cambio = this.GetPreferencias().AsignarGrupo(contacto);
                        if (cambio)
                        {
                            Modelos.SetActualizarGruposContactos();
                        }
                    }
                    else
                    {
                        if (nuevoEstado == Estado.Desconectado)
                        {
                            bool cambio = this.GetPreferencias().AsignarGrupo(contacto);
                            if (cambio)
                            {
                                Modelos.SetActualizarGruposContactos();
                            }
                        }
                    }
                    Modelos.SetActualizarContacto(contacto);
                    addLog("Cuenta: " + contactoSimple.IdCuenta + " Cambio el estado del contacto " + contacto);
                }
            }
        }
        public void ActualizarImagenContacto(ContactoSimple contactoSimple, Image nuevaImage)
        {
            ContactoColor contacto = BuscarPorContactoSimple(contactoSimple);
            if (contacto.SobrescribirPropiedades)
            {
                Image cuadrada = Adornos.CuadrarImagen(nuevaImage);
                if (!Adornos.ImagenesIguales(contacto.Imagen, cuadrada))
                {
                    contacto.Imagen = cuadrada;
                    Modelos.SetActualizarContacto(contacto);
                    addLog("Cuenta: " + contactoSimple.IdCuenta + " Cambio imagen del contacto " + contacto);
                }
            }
        }
        public void ContactoComenzoODejoDeEscribir(ContactoSimple contactoSimple)
        {

            ContactoColor contacto = BuscarPorContactoSimple(contactoSimple);
            contacto.Escribiendo = !contacto.Escribiendo;
            Modelos.SetActualizarVentanaChatContacto(contacto);
        }
        public void ContactoEnvioMensaje(ContactoSimple contactoSimple, string mensaje)
        {

            ContactoColor contacto = BuscarPorContactoSimple(contactoSimple);

            contacto.IdContactoSimpleRecibido = contactoSimple.IdContactoSimple;

            if (contacto.IdContactoSimpleEnviado != contactoSimple.IdContactoSimple)
            {
                contacto.IdContactoSimpleEnviado = contactoSimple.IdContactoSimple;
                Modelos.SetActualizarIdReceptorVentanaChat(contacto);
            }
            addLog("Contacto {0} envio mensaje, alias: {1} ", contactoSimple, contactoSimple.NombrePublico);
            contacto.Escribiendo = false;
            AgregarHistorialRecibido(contactoSimple, mensaje);
            GUI.ContactoEnvioMensaje(contacto.IdContactoColor, Adornos.AdornarMensajeConversacion(contacto, mensaje));
            this.GetPreferencias().MensajeRecibido(contacto);

        }

        private void AgregarHistorialRecibido(ContactoSimple contactoSimple, string mensaje)
        {
            Historial elegido = Historiales[contactoSimple.IdContactoSimple];
            elegido.Recibido(mensaje);
        }

        internal void MensajeErrorRecibido(ContactoSimple contactoSimple, String error)
        {
            addLog("mensaje error recibido " + contactoSimple);
            ContactoColor contacto = BuscarPorContactoSimple(contactoSimple);
            contacto.Escribiendo = false;
            this.GetPreferencias().MensajeErrorRecibido();
            GUI.ContactoEnvioMensaje(contacto.IdContactoColor, Adornos.AdornarMensajeConversacion(contacto, "Fallo al enviar mensaje causa: " + error));
        }
        private void AgregarHistorialErrorEnviado(ContactoSimple contactoSimple, string mensaje)
        {
            Historial elegido = Historiales[contactoSimple.IdContactoSimple];
            elegido.RecibidoError(mensaje);
        }
        /// <summary>
        /// Alguna cuenta creo un nuevo contacto
        /// </summary>
        /// <param name="contactoSimple"></param>
        /// <param name="idCuenta"></param>
        public void NuevoContacto(ContactoSimple contactoSimple)
        {
            addLog("Nuevo Contacto descubierto: " + contactoSimple + " en el cliente: " + contactoSimple.IdCuenta);
            ContactoColor contacto = new ContactoColor();
            contacto.AgregarContactoAsociado(contactoSimple);
            contacto.IdContactoSimpleRecibido = contactoSimple.IdContactoSimple;
            contacto.IdContactoSimpleEnviado = contactoSimple.IdContactoSimple;

            Historiales[contactoSimple.IdContactoSimple] = new Historial(contactoSimple.IdContactoSimple);
            Contactos.Add(contacto);
            this.Preferencias.AsignarGrupo(contacto);
            Modelos.SetActualizarListaContactos();
            Modelos.SetActualizarGruposContactos();

        }
        public void NuevaCuentaAsociada(ContactoSimple yoExterno)
        {

            if (!Yo.ContactosSimple.Contains(yoExterno.IdContactoSimple))
            {
                Yo.AgregarContactoAsociado(yoExterno);
                addLog("Nueva cuenta asociada " + yoExterno.Jid + " desde el cliente " + yoExterno.IdCuenta);
            }
        }

    }

}
