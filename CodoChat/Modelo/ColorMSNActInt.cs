﻿using CodoChat.ModeloVista;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.Modelo
{
    /// <summary>
    /// Actualizaciones a mi contacto desde la gui del programa
    /// </summary>
    partial class ColorMSN
    {
        public void ActualizarPerfil(Preferencias perfil)
        {
            this.Preferencias = perfil;
            addLog("Cambie mis preferencias");
        }

        internal void AgregarEmoticon(Emoticon nuevoEmoticon)
        {
            this.Yo.Emoticones.Add(nuevoEmoticon.Abreviatura, nuevoEmoticon);
            Modelos.SetActualizarEmoticones();
            addLog("Agregue emoticon " + nuevoEmoticon.Abreviatura);
            this.Yo.UltimaActualizacion = DateTime.Now;
        }
        internal void EditarEmoticon(string abreviaturaVieja, Emoticon nuevoEmoticon)
        {
            this.Yo.Emoticones.Remove(abreviaturaVieja);
            this.Yo.Emoticones.Add(nuevoEmoticon.Abreviatura, nuevoEmoticon);
            Modelos.SetActualizarEmoticones();
            addLog("edite emoticon " + abreviaturaVieja);
            this.Yo.UltimaActualizacion = DateTime.Now;
            
        }
        internal void EliminarEmoticon(Emoticon emoticon)
        {
            this.Yo.Emoticones.Remove(emoticon.Abreviatura);
            Modelos.SetActualizarEmoticones();
            addLog("Elimine emoticon " + emoticon.Abreviatura);
            this.Yo.UltimaActualizacion = DateTime.Now;
           
        }

        public void ActualizarMiNick(String rtfNuevoNick)
        {

            if (!Yo.Nick.Equals(rtfNuevoNick))
            {
                Yo.Nick = rtfNuevoNick;
                Modelos.SetActualizarYo();
                addLog("Cambie mi nick desde el cliente");
                this.Yo.UltimaActualizacion = DateTime.Now;
              
            }
        }

        public void ActualizarMiSubNick(String rtfNuevoSubNick)
        {

            if (!Yo.Subnick.Equals(rtfNuevoSubNick))
            {
                Yo.Subnick = rtfNuevoSubNick;
                Modelos.SetActualizarYo();
                addLog("Cambie mi subnick desde el cliente");
                this.Yo.UltimaActualizacion = DateTime.Now;
               
            }
        }


        public void ActualizarMiFuente(Fuente nuevo)
        {
            Yo.Fuente = nuevo;
            Modelos.SetActualizarYo();
            addLog("Cambio mi fuente desde el cliente");
            this.Yo.UltimaActualizacion = DateTime.Now;
         
        }

        public void ActualizarMiImagen(Image nuevaImagen)
        {
            Image cuadrada = Adornos.CuadrarImagen(nuevaImagen);
            if (!Adornos.ImagenesIguales(Yo.Imagen, cuadrada))
            {
                Yo.Imagen = cuadrada;
                Modelos.SetActualizarYo();
                addLog("Cambie mi imagen desde el cliente");
                this.Yo.UltimaActualizacion = DateTime.Now;
               
            }
        }

        public void ActualizarMiEstado(Estado nuevoEstado)
        {
            Yo.Estado = nuevoEstado;
            Modelos.SetActualizarYo();
            addLog("Cambie mi estado desde el cliente");
        }

        internal void EliminarCuenta(string idCuenta)
        {
            Cuenta cuenta = GetCuenta(idCuenta);
            //deslogueamos para que deje de buscar contactos
            cuenta.Desconectar();

            //eliminamos mi asociado de esa cuenta.
            if (cuenta.Yo != null)
            {
                Yo.EliminarContactoAsociado(cuenta.Yo);
            }

            //eliminamos todos los contactos que hayamos agregado
            //por esa cuenta
            foreach (ContactoSimple externo in cuenta.Contactos.Values)
            {
                EliminarContacto(externo);
            }

            //removemos esa cuenta de la lista de asociadas
            Asociadas.Remove(cuenta);
            Modelos.modeloListaCuenta.Actualizar = true;
            addLog("Cuenta " + idCuenta + " eliminada");
            Repositorio.CuentaCol.Remove(idCuenta);

            Modelos.SetActualizarListaContactos();
            Modelos.SetActualizarGruposContactos();
        }
        public void EliminarContacto(ContactoSimple contactoSimple)
        {
            ContactoColor contacto = BuscarPorContactoSimple(contactoSimple);
            if (contacto != null)
            {
                contacto.EliminarContactoAsociado(contactoSimple);
                if (contacto.ContactosSimple.Count == 0)
                {
                    Contactos.Remove(contacto);
                }
                addLog("Contacto eliminado " + contacto.IdContactoColor);
                Repositorio.ContactoSimpleCol.Remove(contactoSimple.IdContactoSimple);
                Repositorio.ContactoColorCol.Remove(contactoSimple.IdContactoColor);
            }
        }


        public void ContactoDebeRecibirMensaje(string idContactoColor, string mensaje)
        {

            ContactoColor contacto = GetContacto(idContactoColor);
            ContactoSimple contactoSimple = Repositorio.ContactoSimpleCol[contacto.IdContactoSimpleEnviado];
            Cuenta cuenta = GetCuenta(contactoSimple.IdCuenta);
            AgregarHistorialEnviado(contactoSimple,mensaje);
            cuenta.ContactoDebeRecibirMensaje(contactoSimple.Jid, mensaje);
            GUI.ContactoDebeRecibirMensaje(contacto.IdContactoColor, Adornos.AdornarMensajeConversacion(Yo, mensaje));
            addLog("enviando mensaje al contacto {0} alias {1} ", contactoSimple, contactoSimple.NombrePublico);

        }

        private void AgregarHistorialEnviado(ContactoSimple contactoSimple, string mensaje)
        {
            Historial elegido = Historiales[contactoSimple.IdContactoSimple];
            elegido.Enviado(mensaje);
        }


        public void LimpiarLogs()
        {
            this.Logs.Clear();
        }


        internal void DeshabilitarSobrescritura(ContactoColor contacto)
        {
            contacto.SobrescribirPropiedades = false;
        }
        internal void HabilitarSobrescritura(ContactoColor contacto)
        {
            contacto.SobrescribirPropiedades = true;
        }
    }

}
