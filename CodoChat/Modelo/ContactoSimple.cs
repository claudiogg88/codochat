﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using CodoChat.Modelo;
namespace CodoChat.Modelo
{
    [Serializable()]
    public class ContactoSimple
    {
        public string Jid { get; set; } //un nombre unico respecto al servidor
        public string IdContactoColor { get; set; } //asociacion con un contacto general

        public string IdCuenta { get; set; } //asociacion con la cuenta a la que pertenece

        public string IdContactoSimple { get; set; } //id unico en la aplicacion

        public string NombreRed { get; set; } //facebook, gmail

        public DateTime Actualizado { get; set; }//ultima vez actualizado

        public Estado Estado { get; set; } //estado en esta cuenta

        public Image Imagen { get; set; }


        public string NombrePublico { get; set; }

        public string Nick { get; set; }
        public ContactoSimple(string jid, string idCuenta, String nombreRed)
        {
            this.Jid = jid;
            this.IdCuenta = idCuenta;
            this.Estado = Estado.Desconocido;
            this.NombreRed = nombreRed;
            this.Imagen = Adornos.GetImagenDefault();
            IdContactoSimple = Utiles.GenerarClave();
            Repositorio.Agregar(this);
        }


        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return this.IdContactoSimple.Equals(((ContactoSimple)obj).IdContactoSimple);
        }

        public override int GetHashCode()
        {
            return this.IdContactoSimple.GetHashCode();
        }
        public override string ToString()
        {
            return this.Nick != null ? this.Nick : (this.NombrePublico != null ? this.NombrePublico : this.Jid);
        }



        public string GetNombrePublicoOJID() { return this.NombrePublico != null ? this.NombrePublico : this.Jid; }
    }
}
