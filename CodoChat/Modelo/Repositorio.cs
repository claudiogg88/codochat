﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.Modelo
{
    public class Repositorio
    {

        public static Dictionary<String, ContactoSimple> ContactoSimpleCol = new Dictionary<string, ContactoSimple>();
        public static Dictionary<String, ContactoColor> ContactoColorCol = new Dictionary<string, ContactoColor>();
        public static Dictionary<String, Cuenta> CuentaCol = new Dictionary<string, Cuenta>();
        public static Dictionary<String, Grupo> GrupoCol = new Dictionary<string, Grupo>();
        internal static void Agregar(object elemento)
        {
            if(elemento==null)
            {
                return;
            }
            if(elemento.GetType() == typeof(List<Grupo>)){
                List<Grupo> grupos = (List<Grupo>)elemento;
                foreach (Grupo grupo in grupos)
                {
                    Agregar(grupo);
                }
                return;
            }
            if (elemento.GetType() == typeof(List<CodoChat.Modelo.ContactoColor>))
            {
                List<CodoChat.Modelo.ContactoColor> contactos = (List<CodoChat.Modelo.ContactoColor>)elemento;
                foreach (ContactoColor contacto in contactos)
                {
                    Agregar(contacto);
                }
                return;
            }
            if (elemento.GetType() == typeof(Dictionary<string, CodoChat.Modelo.ContactoSimple>))
            {
                Dictionary<string, CodoChat.Modelo.ContactoSimple> contactos = (Dictionary<string, CodoChat.Modelo.ContactoSimple>)elemento;
                foreach (ContactoSimple contacto in contactos.Values)
                {
                    Agregar(contacto);
                }
                return;
            }
            if (elemento.GetType() == typeof(List<CodoChat.Modelo.Cuenta>))
            {
                List<CodoChat.Modelo.Cuenta> cuentas = (List<CodoChat.Modelo.Cuenta>)elemento;
                foreach (Cuenta cuenta in cuentas)
                {
                    Agregar(cuenta);
                }
                return;
            }
            if (elemento.GetType() == typeof(CodoChat.Modelo.ContactoSimple))
            {
                CodoChat.Modelo.ContactoSimple contactoSimple = (CodoChat.Modelo.ContactoSimple)elemento;
                ContactoSimpleCol[contactoSimple.IdContactoSimple] = contactoSimple;
                return;
            }
            if (elemento.GetType() == typeof(CodoChat.Modelo.ContactoColor))
            {
                CodoChat.Modelo.ContactoColor contactoColor = (CodoChat.Modelo.ContactoColor)elemento;
                ContactoColorCol[contactoColor.IdContactoColor] = contactoColor;
                return;
            }
            if ( typeof(CodoChat.Modelo.Cuenta).IsAssignableFrom(elemento.GetType()))
            {
                CodoChat.Modelo.Cuenta cuenta = (CodoChat.Modelo.Cuenta)elemento;
                CuentaCol[cuenta.IdCuenta] = cuenta;
                return;
            }
            if (typeof(CodoChat.Modelo.Grupo).IsAssignableFrom(elemento.GetType()))
            {
                CodoChat.Modelo.Grupo grupo = (CodoChat.Modelo.Grupo)elemento;
                GrupoCol[grupo.IdGrupo] = grupo;
                return;
            }
            throw new Exception("No se encontro a donde agregar ese elemento");
        }


        internal static ContactoColor BuscarPorContactoSimple(ContactoSimple contactoSimple)
        {
            return ContactoColorCol[contactoSimple.IdContactoColor];
        }

        internal static ContactoColor BuscarPorNombrePublico(string nombrePublico)
        {
            ContactoSimple contactoSimple = ContactoSimpleCol.Values.Where(c => c.NombrePublico == nombrePublico).FirstOrDefault();
            if(contactoSimple!=null)
            {
                return ContactoColorCol[contactoSimple.IdContactoColor];
            }
            else
            {
                return null;
            }
        }
    }
}
