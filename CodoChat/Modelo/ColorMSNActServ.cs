﻿using CodoChat.ModeloVista;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.Modelo
{
    /// <summary>
    /// Actualizaciones a los contactos desde el servidor de codochat
    /// </summary>
    partial class ColorMSN
    {
        ///////////////////////////////////ACTUALIZACIONES A UN CONTACTO DESDE EL SERVIDOR CODOCHAT//////////////////////////////////
        public void ActualizarFuenteContactoDesdeServidor(Fuente nueva, ContactoColor contacto)
        {
            if (!contacto.Fuente.Equals(nueva))
            {
                contacto.Fuente = nueva;
                Modelos.SetActualizarVentanaChatContacto(contacto);
                addLog("Contacto " + contacto + " cambio su fuente");
            }

        }
        public void ActualizarEmoticonesContactoDesdeServidor(Dictionary<String, Emoticon> nueva, ContactoColor contacto)
        {
            Utiles.GuardarImagenes(nueva);
            contacto.Emoticones.Clear();
            foreach (Emoticon nuevo in nueva.Values)
            {
                contacto.Emoticones.Add(nuevo.Abreviatura, nuevo);
            }
            Modelos.SetActualizarContacto(contacto);
            addLog("Contacto {0} cambio su lista de emoticones en codochat ", contacto); 
        }
        public void ActualizarNickContactoDesdeServidor(String nickRtfNuevo, ContactoColor contacto)
        {
            //contacto.UltimaActualizacion = DateTime.Now;
            if (!contacto.Nick.Equals(nickRtfNuevo))
            {
                contacto.Nick = nickRtfNuevo;
                Modelos.SetActualizarContacto(contacto);
                addLog("Contacto " + contacto + " cambio su nick  en codochat ");
            }
        }
        public void ActualizarSubNickContactoDesdeServidor(String subnickRtfNuevo, ContactoColor contacto)
        {
            //contacto.UltimaActualizacion = DateTime.Now;
            if (!contacto.Subnick.Equals(subnickRtfNuevo))
            {
                contacto.Subnick = subnickRtfNuevo;
                Modelos.SetActualizarContacto(contacto);
                addLog("Contacto " + contacto + " cambio su subnick en codochat  ");
            }
        }

        /// <summary>
        /// Este es el verdadero estado en el codochat
        /// </summary>
        /// <param name="nuevoEstado"></param>
        /// <param name="contacto"></param>
        public void ActualizarEstadoContactoDesdeServidor(Estado nuevoEstado, ContactoColor contacto)
        {
            //el contacto no envio informacion en mas de 5 minutos, lo consideramos desconectado
            if(nuevoEstado == Estado.Desconocido)
            {
                nuevoEstado = Estado.Desconectado;
            }

            if (Estado.Desconectado == nuevoEstado)
            {
                if (contacto.ConectadoDesdeCodoChat)
                {
                    contacto.ConectadoDesdeCodoChat = false;
                    //ignoramos el estado, dejamos q los demas clientes lo actualizen de ahora en adelante
                    Modelos.SetActualizarContacto(contacto);
                    bool cambio = this.GetPreferencias().AsignarGrupo(contacto);
                    if (cambio)
                    {
                        Modelos.SetActualizarGruposContactos();
                    }
                }              
            }
            else
            {
                //esta conectado con nuestro cliente, debemos ignorar las 
                //actualizaciones desde facebook, google, etc.
                if (!contacto.ConectadoDesdeCodoChat)
                {
                    contacto.ConectadoDesdeCodoChat = true;
                    Modelos.SetActualizarContacto(contacto);
                }

            }
            if (contacto.ConectadoDesdeCodoChat)
            {
                if (contacto.Estado != nuevoEstado)
                {
                    Estado viejo = contacto.Estado;
                    contacto.Estado = nuevoEstado;

                    if (viejo == Estado.Desconectado)
                    {
                        this.GetPreferencias().ContactoConectado();
                        bool cambio = this.GetPreferencias().AsignarGrupo(contacto);
                        if(cambio)
                        {
                            Modelos.SetActualizarGruposContactos();
                        }
                    }
                    Modelos.SetActualizarContacto(contacto);
                    addLog("Contacto " + contacto + " cambio su estado en codochat ");
                }
            }
            else
            {
                //TODO considerar para mas cuentas asociadas a un mismo contacto simple
                ContactoSimple con = Repositorio.ContactoSimpleCol[contacto.ContactosSimple[0]];
                nuevoEstado = Repositorio.BuscarPorContactoSimple(con).Estado;
                ActualizarEstadoContacto(con,nuevoEstado);
            }
        }

        public void ActualizarImagenContactoDesdeServidor(Image nuevaImagen, ContactoColor contacto)
        {
            //contacto.UltimaActualizacion = DateTime.Now;
            if (!Adornos.ImagenesIguales(contacto.Imagen, nuevaImagen))
            {
                contacto.Imagen = nuevaImagen;
                Modelos.SetActualizarContacto(contacto);
                addLog("Contacto " + contacto + " cambio su imagen  en codochat ");
                return;
            }
        }


        /////////////////////////////////ACTUALIZACIONES A MI CONTACTO DESDE EL SERVIDOR CODOCHAT//////////////////////////////////
        public void ActualizarMiNickDesdeServidor(String rtfNuevoNick)
        {
            //Yo.UltimaActualizacion = DateTime.Now;
            Yo.Nick = rtfNuevoNick;
            Modelos.SetActualizarYo();
            addLog("El servidor cambio mi nick");
            
        }
        public void ActualizarMiSubNickDesdeServidor(String rtfNuevoSubNick)
        {
            //Yo.UltimaActualizacion = DateTime.Now;
            Yo.Subnick = rtfNuevoSubNick;
            Modelos.SetActualizarYo();
            addLog("El servidor cambio mi subnick ");
        }
        public void ActualizarMiFuenteDesdeServidor(Fuente nuevo)
        {
            //Yo.UltimaActualizacion = DateTime.Now;
            Yo.Fuente = nuevo;
            Modelos.SetActualizarYo();
            addLog("El servidor cambio mi fuente ");
        }
        public void ActualizarMiImagenDesdeServidor(Image nuevaImagen)
        {
            //Yo.UltimaActualizacion = DateTime.Now;
            Yo.Imagen = nuevaImagen;
            Modelos.SetActualizarYo();
            addLog("El servidor cambio mi imagen ");

        }
        internal void ActualizarMisEmoticonesDesdeServidor(Dictionary<string, Emoticon> nueva)
        {
            Modelos.modeloFormEmoticones.AltaEmoticonesDesdeArchivo(nueva);
            Modelos.SetActualizarEmoticones();
            addLog("El servidor cambio mis emoticones ");
        }

        internal void NuevoContactoColor(ContactoColor contacto, string usuarioColor)
        {
            addLog("Nuevo contacto color para el contacto , agregando clave {1} a {0}", contacto, usuarioColor);
            contacto.UsuarioColor = usuarioColor;
            DeshabilitarSobrescritura(contacto);
            Modelos.SetActualizarContacto(contacto);
        }
        internal void BajaContactoColor(ContactoColor contacto)
        {
            addLog("El contacto  {1} ya no es un contactocolor", contacto);
            HabilitarSobrescritura(contacto);
            Modelos.SetActualizarContacto(contacto);
            GetCuenta(Repositorio.ContactoSimpleCol[contacto.IdContactoSimpleEnviado].IdCuenta).ActualizarListaContactos();
        }
    }
        
}
