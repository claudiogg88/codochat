﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodoChat.ModeloVista
{
    public class ModeloFormEmoticones
    {
        public Boolean Actualizar { get; set; }
        public Dictionary<String, ListViewItem> listViewItemEmoticonesPropios;
        FormEmoticones formEmoticones;


        public ModeloFormEmoticones(FormEmoticones formEmoticones)
        {
            this.formEmoticones = formEmoticones;
            listViewItemEmoticonesPropios = new Dictionary<String, ListViewItem>();
            Actualizar = true;

            Modelos.Agregar(this);
        }

        public void ActualizarVentanaEmoticones()
        {
            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {


                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    //esta solo se sincroniza una vez
                    if (formEmoticones.listaEmoticonesPropios.Items.Count == 0)
                    {
                        var emoDefault = ColorMSN.Instancia.Emoticones.Values.OrderBy(d => d.Abreviatura);
                        foreach (Emoticon emoticon in emoDefault)
                        {
                            if (emoticon.Nombre.Equals(emoticon.Abreviatura))
                            {
                                String nombre = emoticon.Nombre;
                                Image imagen = emoticon.Imagen;
                                ListViewItem itemEmoticon = new ListViewItem(nombre);
                                itemEmoticon.Name = nombre; //va a ser la clave
                                itemEmoticon.Text = nombre;
                                itemEmoticon.ImageKey = emoticon.MD5;
                                itemEmoticon.ToolTipText = nombre;
                                formEmoticones.imagenesEmoticonesDefault.Images.Add(emoticon.MD5, imagen);
                                formEmoticones.listaEmoticonesDefault.Items.Add(itemEmoticon);
                            }
                        }
                    }

                    var emoPropios = ColorMSN.Instancia.Yo.Emoticones.Values.OrderBy(d => d.Abreviatura).ToList();
                    //eliminar sobrantes
                    List<ListViewItem> eliminar = new List<ListViewItem>();
                    foreach (ListViewItem listViewItemEmoticon in listViewItemEmoticonesPropios.Values)
                    {
                        string md5 = listViewItemEmoticon.ImageKey;
                        string abreviatura = listViewItemEmoticon.Name;
                        if (!emoPropios.Exists(d => d.MD5 == md5 && d.Abreviatura == abreviatura))
                        {
                            eliminar.Add(listViewItemEmoticon);
                        }
                    }
                    foreach (ListViewItem listViewItemEmoticon in eliminar)
                    {
                        formEmoticones.imagenesEmoticonesPropios.Images.RemoveByKey(listViewItemEmoticon.ImageKey);
                        formEmoticones.listaEmoticonesPropios.Items.Remove(listViewItemEmoticon);
                        listViewItemEmoticonesPropios.Remove(listViewItemEmoticon.Name);
                    }
                    //agregar
                    List<Emoticon> agregar = new List<Emoticon>();
                    foreach (Emoticon propio in emoPropios)
                    {
                        if (!listViewItemEmoticonesPropios.Values.ToList().Exists(d => d.Name == propio.Abreviatura && propio.MD5 == d.ImageKey))
                        {
                            agregar.Add(propio);
                        }
                    }
                    foreach (Emoticon nuevo in agregar)
                    {
                        listViewItemEmoticonesPropios.Remove(nuevo.Abreviatura);
                        String nombre = nuevo.Nombre;
                        Image imagen = nuevo.Imagen;
                        ListViewItem itemEmoticon = new ListViewItem(nombre);
                        itemEmoticon.Name = nombre; //va a ser la clave
                        itemEmoticon.Text = nombre;
                        itemEmoticon.ImageKey = nuevo.MD5;
                        itemEmoticon.ToolTipText = nombre;
                        formEmoticones.imagenesEmoticonesPropios.Images.Add(nuevo.MD5, imagen);
                        formEmoticones.listaEmoticonesPropios.Items.Add(itemEmoticon);
                        listViewItemEmoticonesPropios[nuevo.Abreviatura] = itemEmoticon;
                    }

                    Actualizar = false;
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar form emoticones {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }
        }

        public void RellenarEmoticonesAjenos()
        {
            ContactoColor receptor = Repositorio.ContactoColorCol[formEmoticones.IdContactoReceptor];
            formEmoticones.listaEmoticonesAjenos.Items.Clear();
            foreach (Emoticon nuevo in receptor.Emoticones.Values)
            {

                String nombre = nuevo.Nombre;
                Image imagen = nuevo.Imagen;
                ListViewItem itemEmoticon = new ListViewItem(nombre);
                itemEmoticon.Name = nombre; //va a ser la clave
                itemEmoticon.Text = nombre;
                itemEmoticon.ImageKey = nuevo.MD5;
                itemEmoticon.ToolTipText = nombre;
                formEmoticones.imagenesEmoticonesAjenos.Images.Add(nuevo.MD5, imagen);
                formEmoticones.listaEmoticonesAjenos.Items.Add(itemEmoticon);
            }
        }



        internal void Crear()
        {
            FormEmoticon datos = new FormEmoticon();
            datos.ShowDialog(formEmoticones);
            procesarFormCrear(datos);
        }

        private void procesarFormCrear(FormEmoticon datos)
        {
            if (datos.Aceptado)
            {
                Emoticon nuevoEmoticon = new Emoticon(datos.Abreviatura, datos.Abreviatura, datos.Imagen, true);
                List<Emoticon> todos = new List<Emoticon>();
                todos.AddRange(ColorMSN.Instancia.Emoticones.Values);
                todos.AddRange(ColorMSN.Instancia.Yo.Emoticones.Values);
                bool error = false;
                if (nuevoEmoticon.Abreviatura.Length == 0)
                {
                    MessageBox.Show(datos, "La abreviatura no puede estar vacia");
                    error = true;
                }

                bool imagenRepetida = todos.Count(d => Utiles.CompararImagenes(d.Imagen, nuevoEmoticon.Imagen)) > 0;
                if (imagenRepetida)
                {
                    MessageBox.Show(datos, "Ya existe un emoticon con la misma imagen");
                    error = true;
                }

                bool abreviaturaRepetida = todos.Count(d => d.Abreviatura == nuevoEmoticon.Abreviatura) > 0;
                if (abreviaturaRepetida)
                {
                    MessageBox.Show(datos, "Ya existe un emoticon con la misma abreviatura");
                    error = true;
                }
                if (!error)
                {
                    nuevoEmoticon.GuardarImagen();
                    ColorMSN.Instancia.AgregarEmoticon(nuevoEmoticon);
                    datos.Dispose();
                }
                else
                {
                    datos.Aceptado = false;
                    datos.ShowDialog(formEmoticones);
                    procesarFormCrear(datos);
                }
            }
        }

        internal void Eliminar()
        {
            var emoticones = formEmoticones.listaEmoticonesPropios.SelectedItems;
            foreach (ListViewItem item in emoticones)
            {
                Emoticon emoticon = ColorMSN.Instancia.GetEmoticon(item.Name);
                if (emoticon.Editable)
                {
                    ColorMSN.Instancia.EliminarEmoticon(emoticon);
                }
                else
                {
                    MessageBox.Show("No se puede eliminar un emoticon predefinido");
                }
            }

        }

        internal void SoltarEmoticonDefault()
        {
            var emoticones = formEmoticones.listaEmoticonesDefault.SelectedItems;
            SoltarEmoticones(emoticones);
        }

        internal void SoltarEmoticonPropio()
        {
            var emoticones = formEmoticones.listaEmoticonesPropios.SelectedItems;
            SoltarEmoticones(emoticones);

        }
        private void SoltarEmoticones(ListView.SelectedListViewItemCollection emoticones)
        {

            string textoEmoticones = "";
            foreach (ListViewItem item in emoticones)
            {
                Emoticon emoticon = ColorMSN.Instancia.GetEmoticon(item.Name);
                textoEmoticones += emoticon.Abreviatura;
            }
            VentanaChat ventana = (VentanaChat)formEmoticones.Owner;
            ventana.TextoDiciendo = ventana.TextoDiciendo + textoEmoticones;
        }
        internal void RobarEmoticones()
        {
            var ListViewItemEmoticones = formEmoticones.listaEmoticonesAjenos.SelectedItems;
            ContactoColor receptor = Repositorio.ContactoColorCol[formEmoticones.IdContactoReceptor];
            Dictionary<String, Emoticon> seleccionados = new Dictionary<string, Emoticon>();
            foreach(ListViewItem item in ListViewItemEmoticones){
                seleccionados[item.Name] = receptor.Emoticones[item.Name];
            }
            AltaEmoticonesDesdeArchivo(seleccionados);
        }

        internal void Editar()
        {
            if (formEmoticones.listaEmoticonesPropios.SelectedItems.Count > 0)
            {
                string idEmoticon = formEmoticones.listaEmoticonesPropios.SelectedItems[0].Name;
                Emoticon viejo = ColorMSN.Instancia.GetEmoticon(idEmoticon);
                if (!viejo.Editable)
                {
                    MessageBox.Show(formEmoticones, "No se puede editar un emoticon predefinido");
                    return;
                }
                FormEmoticon datos = new FormEmoticon();
                datos.Abreviatura = viejo.Abreviatura;
                datos.Imagen = viejo.Imagen;
                datos.setEdicion();
                datos.ShowDialog(formEmoticones);
                procesarFormEditar(datos, viejo, idEmoticon);
            }

        }

        private void procesarFormEditar(FormEmoticon datos, Emoticon emoticon, string idEmoticonViejo)
        {
            if (datos.Aceptado)
            {

                List<Emoticon> todos = new List<Emoticon>();
                todos.AddRange(ColorMSN.Instancia.Emoticones.Values);
                todos.AddRange(ColorMSN.Instancia.Yo.Emoticones.Values);
                bool error = false;

                bool abreviaturaRepetida = todos.Count(d => d.Abreviatura == emoticon.Abreviatura) > 0;
                if (abreviaturaRepetida)
                {
                    MessageBox.Show(datos, "Ya existe un emoticon con la misma abreviatura");
                    error = true;
                }
                if (!error)
                {
                    ColorMSN.Instancia.EditarEmoticon(idEmoticonViejo, new Emoticon(datos.Abreviatura, datos.Abreviatura, datos.Imagen, true));
                    datos.Dispose();
                }
                else
                {
                    datos.Aceptado = false;
                    datos.ShowDialog(formEmoticones);
                    procesarFormEditar(datos, emoticon, idEmoticonViejo);
                }

            }
        }

        public void AltaEmoticonesDesdeArchivo(Dictionary<String, Emoticon> emoticones)
        {
            List<Emoticon> todos = new List<Emoticon>();
            todos.AddRange(ColorMSN.Instancia.Emoticones.Values);
            todos.AddRange(ColorMSN.Instancia.Yo.Emoticones.Values);

            foreach (Emoticon emoticon in emoticones.Values)
            {
                if (emoticon.Abreviatura.Length == 0)
                {
                    continue;
                }

                bool imagenRepetida = todos.Count(d => Utiles.CompararImagenes(d.Imagen, emoticon.Imagen)) > 0;
                if (imagenRepetida)
                {

                    ColorMSN.Instancia.addLog("Emoticon no agregado, ya existia la imagen");
                    continue;
                }

                bool abreviaturaRepetida = todos.Count(d => d.Abreviatura == emoticon.Abreviatura) > 0;
                if (abreviaturaRepetida)
                {
                    emoticon.Abreviatura = Utiles.GenerarClave();
                }

                emoticon.GuardarImagen();
                ColorMSN.Instancia.AgregarEmoticon(emoticon);
            }
        }

        internal void ImportarArchivo()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); ;
            openFileDialog1.Filter = "CODO Files (*.codo)|*.codo";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Dictionary<String, Emoticon> emoticones = Persistencia.DeSerializeObject<Dictionary<String, Emoticon>>(openFileDialog1.FileName);
                    AltaEmoticonesDesdeArchivo(emoticones);
                    MessageBox.Show("Importacion correcta, revise logs");
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Fallo importacion de emoticones " + e.Message);
                    MessageBox.Show("Fallo importacion de emoticones, revise logs");
                }
            }
        }

        internal void ImportarCarpeta()
        {
            FolderBrowserDialog openFolderDialog1 = new FolderBrowserDialog();

            openFolderDialog1.RootFolder = Environment.SpecialFolder.Desktop;
            if (openFolderDialog1.ShowDialog() == DialogResult.OK)
            {
                string ruta = openFolderDialog1.SelectedPath;
                try
                {

                    string[] filePaths = Directory.GetFiles(ruta);
                    Dictionary<String, Emoticon> emoticones = new Dictionary<string, Emoticon>();
                    foreach (string imagen in filePaths)
                    {
                        try
                        {
                            Image image = Image.FromFile(imagen);
                            String abreviatura = Utiles.GenerarClave();
                            emoticones.Add(abreviatura, new Emoticon(abreviatura, abreviatura, image, true));
                        }
                        catch (Exception ex)
                        {
                            ColorMSN.Instancia.addLog("No se pudo agregar el emoticon con imagen en {0} error: {1}", imagen, ex.Message);
                        }

                    }
                    AltaEmoticonesDesdeArchivo(emoticones);
                    MessageBox.Show("Importacion correcta, revise logs");
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("No se pudo agregar emoticones desde la carpeta {0} error {1} ", ruta, e.Message);
                    MessageBox.Show("Fallo importacion de emoticones, revise logs");
                }


            }
        }


        internal void FormCerrandose(FormClosingEventArgs e)
        {
            e.Cancel = true;
            formEmoticones.Hide();
            if (formEmoticones.Owner.Visible)
            {
                formEmoticones.Owner.Focus();
            }
        }


        internal void ExportarEmoticones()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = "emoticones";
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); ;
            saveFileDialog1.Filter = "CODO Files (*.codo)|*.codo";
            saveFileDialog1.FilterIndex = 0;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                bool res = Utiles.ExportarEmoticones(saveFileDialog1.FileName);
                if (res)
                {
                    MessageBox.Show("Exportacion correcta");
                }
                else
                {
                    MessageBox.Show("Fallo exportacion de emoticones, revise logs");
                }

            }
        }





        internal void ActualizarBotones()
        {
            if (formEmoticones.activa == formEmoticones.listaEmoticonesDefault)
            {
                formEmoticones.botonEditar.Enabled = false;
                formEmoticones.botonEliminar.Enabled = false;
                formEmoticones.btnRobar.Enabled = false;
            }
            if (formEmoticones.activa == formEmoticones.listaEmoticonesPropios)
            {
                formEmoticones.botonEditar.Enabled = true;
                formEmoticones.botonEliminar.Enabled = true;
                formEmoticones.btnRobar.Enabled = false;
            }
            if (formEmoticones.activa == formEmoticones.listaEmoticonesAjenos)
            {
                formEmoticones.botonEditar.Enabled = false;
                formEmoticones.botonEliminar.Enabled = false;
                formEmoticones.btnRobar.Enabled = true;
            }
        }

    }
}
