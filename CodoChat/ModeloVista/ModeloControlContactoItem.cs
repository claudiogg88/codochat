﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodoChat.ModeloVista
{
    public class ModeloControlContactoItem
    {
        ContactoColor contacto;
        Grupo grupo;
        ControlContactoItem controlContactoItem;
        public Boolean Actualizar { get; set; }

        public ModeloControlContactoItem( ControlContactoItem controlContactoItem, ContactoColor contacto, Grupo grupo)
        {
            this.Actualizar = true;
            this.contacto = contacto;
            this.grupo = grupo;
            this.controlContactoItem = controlContactoItem;
            Modelos.Agregar(this);
        }

        public void ActualizarContactoItem()
        {
            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {

                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    Image imageEstado = Adornos.ObtenerBordeEstado(contacto.Estado);
                    controlContactoItem.ImagenEstado = imageEstado;

                    controlContactoItem.ImagenPerfil = contacto.Imagen;

                    String nickAdornado = Adornos.AdornarNick(contacto);
                    controlContactoItem.TextoNick = nickAdornado;

                    String subnickAdornado = Adornos.AdornarSubNick(contacto);
                    controlContactoItem.TextoSubnick = subnickAdornado;

                    var nombres = Repositorio.ContactoSimpleCol.Values.Where(d => d.IdContactoColor == contacto.IdContactoColor).Select(d => d.GetNombrePublicoOJID());
                    controlContactoItem.Redes = "Redes: " + String.Join(",", nombres);


                    //logos de la red
                    controlContactoItem.LogoFacebook(false);
                    controlContactoItem.LogoGoogle(false);
                    controlContactoItem.LogoCodoChat(false);

                    foreach (String idContactoSimple in contacto.ContactosSimple)
                    {
                        if (Repositorio.ContactoSimpleCol[idContactoSimple].NombreRed.Contains(NombreCliente.Facebook))
                        {
                            controlContactoItem.LogoFacebook(true);
                        }
                        if (Repositorio.ContactoSimpleCol[idContactoSimple].NombreRed.Contains(NombreCliente.Google))
                        {
                            controlContactoItem.LogoGoogle(true);
                        }
                    }
                    if (contacto.UsuarioColor != null)
                    {
                        controlContactoItem.LogoCodoChat(true);
                    }
                    Actualizar = false;

                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar contacto item {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }

        }
        public void DobleClickItemContacto()
        {

            VentanaChat ventana = GUI.ObtenerVentanaChat(this.Id);
            if (ventana == null)
            {
                ventana = GUI.CrearVentanaChat(this.Id);
            }
            if (!ventana.Visible)
            {
                ventana.Show();
                ventana.WindowState = FormWindowState.Normal;
            }
            ventana.Focus();
        }


        internal ContactoColor GetContacto()
        {
            return this.contacto;
        }

        public string Id { get { return contacto.IdContactoColor; } }


        public string IdGrupo { get {return grupo.IdGrupo;} }
    }
}
