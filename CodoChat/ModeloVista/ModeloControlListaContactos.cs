﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.ModeloVista
{
    public class ModeloControlListaContactos
    {

        public Boolean Actualizar { get; set; }
        Dictionary<string, ControlGrupoContactos> controlGruposContactos = new Dictionary<string, ControlGrupoContactos>();
        ControlListaContactos controlListaContactos;
        public ModeloControlListaContactos(ControlListaContactos controlListaContactos)
        {
            this.Actualizar = true;
            this.controlListaContactos = controlListaContactos;
            Modelos.Agregar(this);
        }

        public void ActualizarListaContactos()
        {
            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    var grupos = new List<Grupo>(ColorMSN.Instancia.GruposContactos.Where(d=>d.Visible));

                    //crear grupos y agregarlos graficamente
                    foreach (Grupo grupo in grupos)
                    {
                        if (!controlGruposContactos.ContainsKey(grupo.IdGrupo))
                        {
                            ControlGrupoContactos nuevo = new ControlGrupoContactos(grupo);
                            controlGruposContactos[grupo.IdGrupo] = nuevo;
                            controlListaContactos.FLPGrupos.Controls.Add(nuevo);
                            controlListaContactos.FLPGrupos.Controls.SetChildIndex(nuevo, grupo.Orden);
                        }
                    }

                    //eliminarGrupos
                    List<String> eliminarGrupos = new List<string>();
                    foreach (ControlGrupoContactos controlGruposContacto in controlGruposContactos.Values)
                    {
                        if (!grupos.Exists(d => d.IdGrupo == controlGruposContacto.Id))
                        {
                            eliminarGrupos.Add(controlGruposContacto.Id);
                        }
                    }
                    foreach (String id in eliminarGrupos)
                    {
                        controlListaContactos.FLPGrupos.Controls.Remove(controlGruposContactos[id]);
                        controlGruposContactos.Remove(id);   
                    }


                    Actualizar = false;
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar controllistacontactos {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }

        }


    }
}
