﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.ModeloVista
{
    public class ModeloControlConversacion
    {
        public Boolean Actualizar { get; set; }
        public ContactoColor otro;
        ControlConversacion controlConversacion;
        public ModeloControlConversacion(ControlConversacion controlConversacion, ContactoColor otro)
        {
            this.Actualizar = true;
            this.otro = otro;
            this.controlConversacion = controlConversacion;
            Modelos.Agregar(this);
        }
        public void ActualizarControlConversacion()
        {

            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    ContactoColor yo = ColorMSN.Instancia.Yo;
                    
                    controlConversacion.SetSuFuenteConversacion(otro.Fuente.Tipo, otro.Fuente.Color);
                    controlConversacion.SetMiFuenteConversacion(yo.Fuente.Tipo, yo.Fuente.Color);
                    String suNick = Adornos.AdornarNickConversacion(otro);
                    String miNick = Adornos.AdornarNickConversacion(yo);
                    controlConversacion.SuNick = suNick;
                    controlConversacion.MiNick = miNick;
                    controlConversacion.SetRutaSeparador = Adornos.GetRutaSeparador();
                    Actualizar = false;
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar control conversacion {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }

        }
        public string Id { get { return this.otro.IdContactoColor; } }

    }
}
