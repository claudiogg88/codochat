﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodoChat.ModeloVista
{
    public class ModeloControlContactos
    {
        public Boolean Actualizar { get; set; }
        public Dictionary<string, ListViewItem> ListViewItemsContactos;
        public ControlContactos controlContactos;
        public ModeloControlContactos(ControlContactos control)
        {
            Actualizar = true;
            ListViewItemsContactos = new Dictionary<string, ListViewItem>();
            Modelos.Agregar(this);
            this.controlContactos = control;
        }

        public void ActualizarControlContactos()
        {

            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    //Agregar grupos
                    var grupos = (ColorMSN.Instancia.GruposContactos.Where(d => d.Editable || !d.Visible).OrderBy(d => d.Orden).ToList());
                    //crear grupos y agregarlos graficamente

                    controlContactos.cmbGrupos.ValueMember = "IdGrupo";
                    controlContactos.cmbGrupos.DisplayMember = "Titulo";
                    controlContactos.cmbGrupos.DataSource = grupos;

                    var gruposPropios = (ColorMSN.Instancia.GruposContactos.Where(d => d.Editable).OrderBy(d => d.Orden).ToList());
                    //agregar submenu al menu mover contactos
                    controlContactos.moverAToolStripMenuItem.DropDownItems.Clear();
                    foreach (Grupo grupoPropio in gruposPropios)
                    {
                        ToolStripMenuItem subMenuMoverAGrupo = new ToolStripMenuItem();
                        subMenuMoverAGrupo.Name = grupoPropio.IdGrupo;
                        subMenuMoverAGrupo.Size = new System.Drawing.Size(152, 22);
                        subMenuMoverAGrupo.Text = grupoPropio.Nombre;
                        subMenuMoverAGrupo.Click += subMenuMoverAGrupo_Click;
                        controlContactos.moverAToolStripMenuItem.DropDownItems.Add(subMenuMoverAGrupo);
                    }

                    //agregar submenu al menu copiar contactos
                    controlContactos.copiarAToolStripMenuItem.DropDownItems.Clear();
                    foreach (Grupo grupoPropio in gruposPropios)
                    {
                        ToolStripMenuItem subMenuCopiarAGrupo = new ToolStripMenuItem();
                        subMenuCopiarAGrupo.Name = grupoPropio.IdGrupo;
                        subMenuCopiarAGrupo.Size = new System.Drawing.Size(152, 22);
                        subMenuCopiarAGrupo.Text = grupoPropio.Nombre;
                        subMenuCopiarAGrupo.Click += subMenuCopiarAGrupo_Click;
                        controlContactos.copiarAToolStripMenuItem.DropDownItems.Add(subMenuCopiarAGrupo);
                    }

                    rellenarListaContactos();

                    Console.WriteLine("Actualizando " + this.GetType());

                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar controlcontactos {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace.ToString());
                }
            }
        }

        void subMenuCopiarAGrupo_Click(object sender, EventArgs e)
        {
            CopiarContactos((ToolStripMenuItem)sender);
        }

        void subMenuMoverAGrupo_Click(object sender, EventArgs e)
        {
            MoverContactos((ToolStripMenuItem)sender);
        }



        private void rellenarListaContactos()
        {
            try
            {


                string idGrupo = controlContactos.GrupoSeleccionado();

                if (idGrupo == null)
                {
                    idGrupo = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Todos).IdGrupo;
                }
                Grupo todos = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Todos);
                Grupo ocultos = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Ocultos);

                if (idGrupo == todos.IdGrupo || ocultos.IdGrupo == idGrupo)
                {
                    controlContactos.moverAToolStripMenuItem.Visible = false;
                    controlContactos.tlsRemoverDeGrupo.Visible = false;
                    controlContactos.btnEliminarGrupo.Enabled = false;
                    controlContactos.btnEditarGrupo.Enabled = false;
                    controlContactos.btnRemover.Enabled = false;

                }
                else
                {
                    controlContactos.moverAToolStripMenuItem.Visible = true;
                    controlContactos.tlsRemoverDeGrupo.Visible = true;
                    controlContactos.btnEliminarGrupo.Enabled = true;
                    controlContactos.btnEditarGrupo.Enabled = true;
                    controlContactos.btnRemover.Enabled = true;

                }
                if (ocultos.IdGrupo == idGrupo)
                {
                    controlContactos.copiarAToolStripMenuItem.Visible = false;
                    controlContactos.ocultarToolStripMenuItem.Text = "Mostrar";
                    controlContactos.btnOcultarContactos.Text = "Mostrar Contactos";
                }
                else
                {
                    controlContactos.copiarAToolStripMenuItem.Visible = true;
                    controlContactos.ocultarToolStripMenuItem.Text = "Ocultar";
                    controlContactos.btnOcultarContactos.Text = "Ocultar Contactos";
                }

                if (idGrupo == todos.IdGrupo)
                {
                    controlContactos.fusionarToolStripMenuItem.Visible = true;
                    controlContactos.separarToolStripMenuItem.Visible = true;
                }
                else
                {
                    controlContactos.fusionarToolStripMenuItem.Visible = false;
                    controlContactos.separarToolStripMenuItem.Visible = false;
                }

                List<ContactoColor> contactos = null;

                contactos = new List<ContactoColor>(ColorMSN.Instancia.Contactos.Where(d => d.Grupos.Contains(idGrupo) && !d.Grupos.Contains(ocultos.IdGrupo)));
                //crear items contacto
                controlContactos.lstContactos.Items.Clear();
                foreach (ContactoColor contacto in contactos)
                {
                    var nombres = Repositorio.ContactoSimpleCol.Values.Where(d => d.IdContactoColor == contacto.IdContactoColor).Select(d => d.GetNombrePublicoOJID());
                    ListViewItem item = new ListViewItem(new[] { contacto.ToString(), String.Join(",", nombres) });
                    item.Name = contacto.IdContactoColor;
                    controlContactos.lstContactos.Items.Add(item);
                }
            }
            catch (Exception e)
            {
                ColorMSN.Instancia.addLog("Error al actualizar controlcontactos {0}", e.Message);
                ColorMSN.Instancia.addLog(e.StackTrace.ToString());
            }
        }

        internal void CambioGrupoSeleccionado()
        {
            rellenarListaContactos();
        }

        internal void FusionarContactos()
        {
            var selec = controlContactos.lstContactos.SelectedItems;
            List<ContactoColor> contactos = new List<ContactoColor>();
            foreach (ListViewItem sel in selec)
            {
                ContactoColor contacto = Repositorio.ContactoColorCol[sel.Name];
                contactos.Add(contacto);
            }
            ColorMSN.Instancia.FusionarContactos(contactos);
        }

        internal void SepararContactos()
        {
            var selec = controlContactos.lstContactos.SelectedItems;
            List<ContactoColor> contactos = new List<ContactoColor>();
            foreach (ListViewItem sel in selec)
            {
                ContactoColor contacto = Repositorio.ContactoColorCol[sel.Name];
                contactos.Add(contacto);
            }
            ColorMSN.Instancia.SepararContactos(contactos);
        }
        internal void RemoverContactos()
        {
            string idGrupoOrigen = controlContactos.GrupoSeleccionado();
            Grupo grupoOrigen = ColorMSN.Instancia.GetGrupo(idGrupoOrigen);
            if (!grupoOrigen.Editable)
            {
                MessageBox.Show(controlContactos, "No se puede remover contactos desde este grupo");
                return;
            }
            var selec = controlContactos.lstContactos.SelectedItems;
            List<ContactoColor> contactos = new List<ContactoColor>();
            foreach (ListViewItem sel in selec)
            {
                ContactoColor contacto = Repositorio.ContactoColorCol[sel.Name];
                if (contacto.Grupos.Contains(grupoOrigen.IdGrupo))
                {
                    contactos.Add(contacto);
                }
            }
            ColorMSN.Instancia.RemoverContactosDeGrupo(contactos, grupoOrigen);
        }
        internal void NuevoGrupo()
        {
            FormGrupo datos = new FormGrupo();
            datos.ShowDialog(controlContactos);
            procesarFormCrear(datos);
        }

        private void procesarFormCrear(FormGrupo datos)
        {
            if (datos.Aceptado)
            {
                bool error = false;
                String nombre = datos.NombreGrupo;
                int orden = 0;
                try
                {
                    orden = Convert.ToInt32(datos.Orden);
                    if (orden <= 4)
                    {
                        MessageBox.Show(datos, "El orden debe ser un numero entero mayor a 4");
                        error = true;
                    }
                    if (ColorMSN.Instancia.GruposContactos.Exists(d => d.Orden == orden))
                    {
                        MessageBox.Show(datos, "Ya existe un grupo con el mismo orden");
                        error = true;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(datos, "El orden debe ser un numero entero mayor a 4");
                    error = true;
                }
                if (String.IsNullOrEmpty(nombre))
                {
                    MessageBox.Show(datos, "El nombre no puede estar vacio");
                    error = true;
                }
                if (ColorMSN.Instancia.GruposContactos.Exists(d => d.Nombre == nombre))
                {
                    MessageBox.Show(datos, "Ya existe un grupo con el mismo nombre");
                    error = true;
                }

                if (!error)
                {
                    Grupo grupo = new Grupo(nombre, orden, true, true);
                    ColorMSN.Instancia.NuevoGrupo(grupo);
                    datos.Dispose();
                }
                else
                {
                    datos.ShowDialog(controlContactos);
                    procesarFormCrear(datos);
                }
            }
        }

        internal void EditarGrupo()
        {

            string idGrupo = controlContactos.GrupoSeleccionado();
            Grupo grupo = ColorMSN.Instancia.GetGrupo(idGrupo);
            if (!grupo.Editable)
            {
                MessageBox.Show(controlContactos, "No se puede editar este grupo");
                return;
            }
            FormGrupo datos = new FormGrupo();
            datos.txtOrden.Text = grupo.Orden.ToString();
            datos.txtNombre.Text = grupo.Nombre;
            datos.ShowDialog(controlContactos);
            procesarFormEditar(datos, grupo);
        }

        private void procesarFormEditar(FormGrupo datos, Grupo grupo)
        {
            if (datos.Aceptado)
            {
                bool error = false;
                String nombre = datos.NombreGrupo;
                int orden = 0;
                try
                {
                    orden = Convert.ToInt32(datos.Orden);
                    if (orden <= 4)
                    {
                        MessageBox.Show(datos, "El orden debe ser un numero entero mayor a 4");
                        error = true;
                    }
                    if (ColorMSN.Instancia.GruposContactos.Exists(d => d.Orden == orden && d.IdGrupo != grupo.IdGrupo))
                    {
                        MessageBox.Show(datos, "Ya existe un grupo con el mismo orden");
                        error = true;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(datos, "El orden debe ser un numero entero mayor a 4");
                    error = true;
                }
                if (String.IsNullOrEmpty(nombre))
                {
                    MessageBox.Show(datos, "El nombre no puede estar vacio");
                    error = true;
                }
                if (ColorMSN.Instancia.GruposContactos.Exists(d => d.Nombre == nombre && d.IdGrupo != grupo.IdGrupo))
                {
                    MessageBox.Show(datos, "Ya existe un grupo con el mismo nombre");
                    error = true;
                }

                if (!error)
                {
                    grupo.Orden = orden;
                    grupo.Nombre = nombre;
                    ColorMSN.Instancia.GrupoEditado(grupo);
                    datos.Dispose();
                }
                else
                {
                    datos.Aceptado = false;
                    datos.ShowDialog(controlContactos);
                    procesarFormEditar(datos, grupo);
                }
            }
        }
        internal void EliminarGrupo()
        {
            string idGrupo = controlContactos.GrupoSeleccionado();
            Grupo grupo = ColorMSN.Instancia.GetGrupo(idGrupo);
            if (!grupo.Editable)
            {
                MessageBox.Show(controlContactos, "No se puede eliminar este grupo");
                return;
            }
            ColorMSN.Instancia.EliminarGrupo(grupo);
        }

        public void OcultarDesocultarContactos(string texto)
        {
            if (texto.StartsWith("Ocultar"))
            {
                OcultarContactos();
            }
            else
            {
                DesocultarContactos();
            }
        }

        private void OcultarContactos()
        {
            Grupo Ocultos = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Ocultos);
            var selec = controlContactos.lstContactos.SelectedItems;
            List<ContactoColor> contactos = new List<ContactoColor>();
            foreach (ListViewItem sel in selec)
            {
                ContactoColor contacto = Repositorio.ContactoColorCol[sel.Name];
                if (!contacto.Grupos.Contains(Ocultos.IdGrupo))
                {
                    contactos.Add(contacto);
                }

            }
            ColorMSN.Instancia.OcultarContactos(contactos);
        }
        private void DesocultarContactos()
        {
            Grupo Ocultos = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Ocultos);
            var selec = controlContactos.lstContactos.SelectedItems;
            List<ContactoColor> contactos = new List<ContactoColor>();
            foreach (ListViewItem sel in selec)
            {
                ContactoColor contacto = Repositorio.ContactoColorCol[sel.Name];
                if (contacto.Grupos.Contains(Ocultos.IdGrupo))
                {
                    contactos.Add(contacto);
                }

            }
            ColorMSN.Instancia.DesocultarContactos(contactos);
        }

        internal void CopiarContactos(ToolStripMenuItem menu)
        {
            string idGrupoOrigen = controlContactos.GrupoSeleccionado();
            Grupo grupoDest = ColorMSN.Instancia.GetGrupo(menu.Name);

            Grupo grupoOrigen = ColorMSN.Instancia.GetGrupo(idGrupoOrigen);

            if (grupoOrigen.IdGrupo == grupoDest.IdGrupo)
            {
                MessageBox.Show(controlContactos, "No se puede mover contactos al mismo grupo");
                return;
            }


            var selec = controlContactos.lstContactos.SelectedItems;
            List<ContactoColor> contactos = new List<ContactoColor>();
            foreach (ListViewItem sel in selec)
            {
                ContactoColor contacto = Repositorio.ContactoColorCol[sel.Name];
                if (!contacto.Grupos.Contains(grupoDest.IdGrupo))
                {
                    contactos.Add(contacto);
                }
            }
            ColorMSN.Instancia.CopiarContactosAGrupo(contactos, grupoDest);
        }

        internal void MoverContactos(ToolStripMenuItem menu)
        {
            string idGrupoOrigen = controlContactos.GrupoSeleccionado();
            Grupo grupoOrigen = ColorMSN.Instancia.GetGrupo(idGrupoOrigen);
            Grupo grupoDest = ColorMSN.Instancia.GetGrupo(menu.Name);
            if (!grupoOrigen.Editable)
            {
                MessageBox.Show(controlContactos, "No se puede mover contactos desde este grupo");
                return;
            }
            var selec = controlContactos.lstContactos.SelectedItems;
            List<ContactoColor> contactos = new List<ContactoColor>();
            foreach (ListViewItem sel in selec)
            {
                ContactoColor contacto = Repositorio.ContactoColorCol[sel.Name];
                if (!contacto.Grupos.Contains(grupoDest.IdGrupo))
                {
                    contactos.Add(contacto);
                }
            }
            ColorMSN.Instancia.MoverContactosAGrupo(contactos, grupoOrigen, grupoDest);
        }


    }
}
