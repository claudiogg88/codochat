﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.ModeloVista
{
    public class ModeloControlLogs
    {
        public Boolean Actualizar { get; set; }
        public List<String> textoLogs;
        private ControlLogs controlLogs;
        public bool ScrollLock { get; set; }

        public ModeloControlLogs(ControlLogs controlLogs)
        {
            Actualizar = true;
            ScrollLock = false;
            textoLogs = new List<String>();
            Modelos.Agregar(this);
            this.controlLogs = controlLogs;
        }

        public void ActualizarControlLogs()
        {

            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {


                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    StringBuilder strBuilder = new StringBuilder();
                    List<EventoLog> eventos = new List<EventoLog>(ColorMSN.Instancia.Logs);
                    foreach (EventoLog evento in eventos)
                    {
                        var strEvento = evento.ToString();
                        if (!textoLogs.Contains(strEvento))
                        {
                            strBuilder.AppendLine(strEvento);
                            textoLogs.Add(strEvento);
                        }
                    }
                    var strLogs = strBuilder.ToString();
                    if (strLogs.Length > 0)
                    {
                        if (controlLogs.Visible)
                        {
                            if (ScrollLock)
                            {
                                controlLogs.etqTitulo.Focus();
                            }
                            else
                            {
                                controlLogs.txbLogs.Focus();
                            }
                        }
                        controlLogs.txbLogs.AppendText(strLogs);
                    }
                    Actualizar = false;
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar control logs {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }
        }

        internal void LimpiarLogs()
        {
            ColorMSN.Instancia.LimpiarLogs();
            this.textoLogs.Clear();
            controlLogs.txbLogs.Clear();
        }


    }
}
