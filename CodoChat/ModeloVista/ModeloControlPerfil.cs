﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.ModeloVista
{
    public class ModeloControlPerfil
    {
        private ControlPerfil controlPerfil;
        public Boolean Actualizar { get; set; }
        public ModeloControlPerfil(ControlPerfil controlPerfil)
        {
            this.controlPerfil = controlPerfil;
            this.Actualizar = true;
            Modelos.Agregar(this);
        }


        public void ActualizarControlPerfil()
        {
            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    Preferencias Preferencias = ColorMSN.Instancia.GetPreferencias();
                    controlPerfil.sonidoAlRecibirMensaje.Checked = Preferencias.SonidoAlRecibirMensaje;
                    controlPerfil.sonidoEnConexion.Checked = Preferencias.SonidoEnConexion;
                    controlPerfil.titilarEnMensaje.Checked = Preferencias.TitilarEnMensaje;
                    controlPerfil.ocultarABandeja.Checked = Preferencias.OcultarABandeja;
                    controlPerfil.CambiosAplicados();
                    this.Actualizar = false;
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar form emoticones {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }

        }
        public void Guardar()
        {
            Preferencias perfil = new Preferencias();
            perfil.SonidoAlRecibirMensaje = controlPerfil.sonidoAlRecibirMensaje.Checked;
            perfil.SonidoEnConexion = controlPerfil.sonidoEnConexion.Checked;
            perfil.TitilarEnMensaje = controlPerfil.titilarEnMensaje.Checked;
            perfil.OcultarABandeja = controlPerfil.ocultarABandeja.Checked;
            controlPerfil.CambiosAplicados();
            ColorMSN.Instancia.ActualizarPerfil(perfil);
        }

    }
}
