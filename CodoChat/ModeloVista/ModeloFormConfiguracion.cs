﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodoChat.ModeloVista
{
    public class ModeloFormConfiguracion
    {
        public Boolean Actualizar { get; set; }
        FormConfiguracion formConfiguracion;
        public ModeloFormConfiguracion(FormConfiguracion form)
        {
            Actualizar = true;
            this.formConfiguracion = form;
            Modelos.Agregar(this);
        }

        public void ActualizarFormConfiguracion()
        {

            if (Actualizar && ColorMSN.Instancia != null)
            {
                Console.WriteLine("Actualizando " + this.GetType());
                Actualizar = false;
            }
        }

        internal void FormCerrandose(FormClosingEventArgs e)
        {
            e.Cancel = true;
            formConfiguracion.Hide();
            GUI.VentanaPrincipal.Focus();
        }
    }
}
