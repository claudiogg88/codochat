﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodoChat.ModeloVista
{
    public class ModeloControlListaCuenta
    {
        public Boolean Actualizar { get; set; }
        public Dictionary<string, ListViewItem> listViewItemsCuenta;
        private ControlListaCuenta controlListaCuenta;
        public ModeloControlListaCuenta(ControlListaCuenta controlListaCuenta)
        {
            this.listViewItemsCuenta = new Dictionary<string, ListViewItem>();
            this.Actualizar = true;
            Modelos.Agregar(this);
            this.controlListaCuenta = controlListaCuenta;
        }
        public void ActualizarListaCuenta()
        {
            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    //agregar y actualizar los items de la lista
                    foreach (Cuenta cuenta in ColorMSN.Instancia.Asociadas)
                    {
                        ListViewItem listViewItem;
                        if (!listViewItemsCuenta.ContainsKey(cuenta.IdCuenta))
                        {

                            listViewItem = new ListViewItem();

                            listViewItemsCuenta[cuenta.IdCuenta] = listViewItem;
                            controlListaCuenta.lwCuentas.Items.Add(listViewItem);
                        }
                        else
                        {
                            listViewItem = listViewItemsCuenta[cuenta.IdCuenta];
                        }
                        string nombre = cuenta.ToString();
                        listViewItem.ImageKey = cuenta.NombreRed;
                        listViewItem.Name = cuenta.IdCuenta;
                        listViewItem.Text = nombre + "(" + cuenta.Estado + ")";
                    }
                    List<ListViewItem> eliminar = new List<ListViewItem>();
                    //remover los sobrantes
                    foreach (ListViewItem listViewItem in listViewItemsCuenta.Values)
                    {
                        if (ColorMSN.Instancia.GetCuenta(listViewItem.Name) == null)
                        {
                            eliminar.Add(listViewItem);
                        }
                    }
                    foreach (ListViewItem listViewItem in eliminar)
                    {
                        listViewItemsCuenta.Remove(listViewItem.Name);
                        controlListaCuenta.lwCuentas.Items.Remove(listViewItem);
                    }



                    String idCuentaSeleccionada = controlListaCuenta.CuentaSeleccionada();
                    if (idCuentaSeleccionada == null)
                    {
                        controlListaCuenta.btnAgregar.Enabled = true;
                        controlListaCuenta.btnEditar.Enabled = false;
                        controlListaCuenta.btnEliminar.Enabled = false;
                        controlListaCuenta.botonConectar.Enabled = false;
                    }
                    else
                    {
                        Cuenta cuentaSeleccionada = ColorMSN.Instancia.GetCuenta(idCuentaSeleccionada);
                        string estadoSeleccionado = cuentaSeleccionada.Estado;
                        switch (estadoSeleccionado)
                        {
                            case "Conectando":
                                {
                                    controlListaCuenta.btnAgregar.Enabled = true;
                                    controlListaCuenta.btnEditar.Enabled = false;
                                    controlListaCuenta.btnEliminar.Enabled = false;
                                    controlListaCuenta.botonConectar.Enabled = false;
                                }
                                break;
                            case "Desconectado":
                                {
                                    controlListaCuenta.btnAgregar.Enabled = true;
                                    controlListaCuenta.btnEditar.Enabled = true;
                                    controlListaCuenta.btnEliminar.Enabled = true;
                                    controlListaCuenta.botonConectar.Enabled = true;
                                    controlListaCuenta.botonConectar.Text = "Conectar";
                                }
                                break;
                            case "Conectado":
                                {
                                    controlListaCuenta.btnAgregar.Enabled = true;
                                    controlListaCuenta.btnEditar.Enabled = true;
                                    controlListaCuenta.btnEliminar.Enabled = true;
                                    controlListaCuenta.botonConectar.Enabled = true;
                                    controlListaCuenta.botonConectar.Text = "Desconectar";

                                }
                                break;
                            case "Desconectando":
                                {
                                    controlListaCuenta.btnAgregar.Enabled = true;
                                    controlListaCuenta.btnEditar.Enabled = false;
                                    controlListaCuenta.btnEliminar.Enabled = false;
                                    controlListaCuenta.botonConectar.Enabled = false;
                                }
                                break;
                            default:
                                {
                                    controlListaCuenta.btnAgregar.Enabled = true;
                                    controlListaCuenta.btnEditar.Enabled = true;
                                    controlListaCuenta.btnEliminar.Enabled = true;
                                    controlListaCuenta.botonConectar.Enabled = true;
                                    controlListaCuenta.botonConectar.Text = "Reintentar";
                                }
                                break;
                        }
                    }

                    Actualizar = false;
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar controllistacuenta {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }
        }


        internal void Conectar()
        {
            String texto = controlListaCuenta.botonConectar.Text;
            String idCuenta = controlListaCuenta.CuentaSeleccionada();
            if (texto.Equals("Conectar") || texto.Equals("Reintentar"))
            {
                ColorMSN.Instancia.Login(idCuenta);
            }
            if (texto.Equals("Desconectar"))
            {
                ColorMSN.Instancia.Logout(idCuenta);
            }
            if (texto.Equals("Cancelar"))
            {
                ColorMSN.Instancia.Logout(idCuenta);
            }
        }

        internal void Eliminar()
        {
            String idCuenta = controlListaCuenta.CuentaSeleccionada();
            Cuenta cuenta = ColorMSN.Instancia.GetCuenta(idCuenta);
            if (MessageBox.Show("Seguro que quiere eliminar la cuenta " + cuenta.ToString() + "?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ColorMSN.Instancia.EliminarCuenta(idCuenta);
            }

        }

        internal void Crear()
        {
            FormCuenta datos = new FormCuenta();
            LlenarFormCuentas(datos);
            datos.ShowDialog(controlListaCuenta);
            procesarFormCrear(datos);
        }

        private void procesarFormCrear(FormCuenta datos)
        {
            if (datos.Aceptado)
            {
                String nombreServidor = ((KeyValuePair<String, string>)datos.comboBoxServidores.SelectedItem).Key;
                String clave = datos.textBoxClave.Text;
                String usuario = datos.textBoxUsuario.Text;
                bool error = false;
                if (String.IsNullOrEmpty(nombreServidor))
                {
                    MessageBox.Show(datos, "El cliente no puede ser nulo");
                    error = true;
                }
                if (String.IsNullOrEmpty(clave))
                {
                    MessageBox.Show(datos, "la clave no puede estar vacia");
                    error = true;

                }
                if (String.IsNullOrEmpty(usuario))
                {
                    MessageBox.Show(datos, "el usuario no puede estar vacio");
                    error = true;
                }

                if (ColorMSN.Instancia.GetCuenta(usuario + "@" + nombreServidor) != null)
                {
                    MessageBox.Show(datos, "ya existe una cuenta con el mismo usuario y cliente");
                    error = true;
                }
                if (!error)
                {
                    Cuenta cuenta = Utiles.InstanciarCuenta(nombreServidor, usuario, clave);
                    ColorMSN.Instancia.AddCuenta(cuenta);
                    datos.Dispose();
                }
                else
                {
                    datos.Aceptado = false;
                    datos.ShowDialog(controlListaCuenta);
                    procesarFormCrear(datos);
                }
            }
        }

        internal void Editar()
        {
            FormCuenta datos = new FormCuenta();
            LlenarFormCuentas(datos);
            String idCuenta = controlListaCuenta.CuentaSeleccionada();
            Cuenta cuenta = ColorMSN.Instancia.GetCuenta(idCuenta);
            datos.comboBoxServidores.Text = cuenta.NombreRed;
            datos.comboBoxServidores.Enabled = false;
            datos.textBoxClave.Text = cuenta.Clave;
            datos.textBoxUsuario.Text = cuenta.Usuario;
            datos.textBoxUsuario.Enabled = false;
            datos.ShowDialog(controlListaCuenta);
            procesarFormEditar(datos, cuenta);

        }

        private void procesarFormEditar(FormCuenta datos, Cuenta cuenta)
        {
            if (datos.Aceptado)
            {
                String nombreServidor = ((KeyValuePair<String, string>)datos.comboBoxServidores.SelectedItem).Key;
                String clave = datos.textBoxClave.Text;
                String usuario = datos.textBoxUsuario.Text;
                bool error = false;
                if (String.IsNullOrEmpty(nombreServidor))
                {
                    MessageBox.Show(datos, "El cliente no puede ser nulo");
                    error = true;
                }
                if (String.IsNullOrEmpty(clave))
                {
                    MessageBox.Show(datos, "La clave no puede estar vacia");
                    error = true;
                }
                if (String.IsNullOrEmpty(usuario))
                {
                    MessageBox.Show(datos, "El usuario no puede estar vacio");
                    error = true;
                }
                if (!error)
                {
                    cuenta.Usuario = usuario;
                    cuenta.Clave = clave;
                    datos.Dispose();
                    ColorMSN.Instancia.Login(cuenta.IdCuenta);
                }
                else
                {
                    datos.Aceptado = false;
                    datos.ShowDialog(controlListaCuenta);
                    procesarFormEditar(datos, cuenta);
                }

            }
        }

        private static void LlenarFormCuentas(FormCuenta formCuenta)
        {
            formCuenta.comboBoxServidores.DataSource = Utiles.CuentasDisponibles.ToArray();
            formCuenta.comboBoxServidores.ValueMember = "Key";
            formCuenta.comboBoxServidores.DisplayMember = "Value";
        }
    }
}
