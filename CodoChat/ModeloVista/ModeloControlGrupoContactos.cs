﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.ModeloVista
{
    public class ModeloControlGrupoContactos
    {
        Grupo Grupo;
        private Dictionary<string, ControlContactoItem> ControlContactosItem;
        public Boolean Actualizar { get; set; }
        private ControlGrupoContactos controlGrupoContactos;


        public ModeloControlGrupoContactos(ControlGrupoContactos controlGrupoContactos, Grupo grupo)
        {
            this.controlGrupoContactos = controlGrupoContactos;
            this.Grupo = grupo;
            this.ControlContactosItem = new Dictionary<string, ControlContactoItem>();
            this.Actualizar = true;
            Modelos.Agregar(this);
        }


        public void ActualizarControlGrupoContactos()
        {
            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    //agregar contactos faltantes

                    List<ContactoColor> contactos = null;
                    Grupo Ocultos = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Ocultos);
                    Grupo Desconectados = ColorMSN.Instancia.GetGrupoPorNombre(NombreGrupo.Desconectados);

                    contactos = new List<ContactoColor>(ColorMSN.Instancia.Contactos.Where(d => d.Grupos.Contains(this.Grupo.IdGrupo) && !d.Grupos.Contains(Ocultos.IdGrupo)));
                    //crear items contacto
                    List<ControlContactoItem> agregar = new List<ControlContactoItem>();
                    foreach (ContactoColor contacto in contactos)
                    {
                        if (!ControlContactosItem.ContainsKey(contacto.IdContactoColor))
                        {
                            ControlContactoItem nuevo = new ControlContactoItem(contacto, Grupo);
                            ControlContactosItem[contacto.IdContactoColor] = nuevo;
                            agregar.Add(nuevo);
                        }
                    }



                    //remover contactos sobrantes
                    List<ControlContactoItem> remover = new List<ControlContactoItem>();
                    foreach (ControlContactoItem controlContactoItem in ControlContactosItem.Values)
                    {
                        if (!Repositorio.ContactoColorCol.ContainsKey(controlContactoItem.Id) || !Repositorio.ContactoColorCol[controlContactoItem.Id].Grupos.Contains(this.Id))
                        {
                            remover.Add(controlContactoItem);
                        }
                    }

                    controlGrupoContactos.TLPContactos.SuspendLayout();
                    foreach (ControlContactoItem controlContactoItem in remover)
                    {
                        controlGrupoContactos.TLPContactos.Controls.Remove(controlContactoItem);
                    }
                    foreach (ControlContactoItem controlContactoItem in agregar)
                    {
                        controlGrupoContactos.TLPContactos.Controls.Add(controlContactoItem);
                    }

                    if (Grupo.Oculto)
                    {
                        controlGrupoContactos.OcultarContactos();
                    }
                    else
                    {
                        controlGrupoContactos.MostrarContactos();
                    }
                    string cantidad = "(" + (controlGrupoContactos.TLPContactos.Controls.Count == 0 ? "Vacio" : controlGrupoContactos.TLPContactos.Controls.Count.ToString()) + ")";
                    controlGrupoContactos.Titulo = Grupo.Nombre + cantidad;

                    controlGrupoContactos.TLPContactos.ResumeLayout();
                   
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar controlgrupocontactos {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }
        }


        public Grupo GetGrupo()
        {
            return this.Grupo;
        }
        public string Id { get { return this.Grupo.IdGrupo; } }

        public bool Oculto { get { return this.Grupo.Oculto; } set { this.Grupo.Oculto = value; } }
    }
}
