﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.ModeloVista
{
    public class Modelos
    {
        public static ModeloControlListaContactos modeloControlListaContacto;
        public static ModeloFormEmoticones modeloFormEmoticones;
        public static Dictionary<string, Dictionary<string,ModeloControlContactoItem>> modelosContacto = new Dictionary<string, Dictionary<string,ModeloControlContactoItem>>();
        public static Dictionary<string, ModeloVentanaChat> modelosVentanaChat = new Dictionary<string, ModeloVentanaChat>();
        public static ModeloControlPersonal modeloControlPersonal;
        public static Dictionary<String, ModeloControlGrupoContactos> modelosGrupo = new Dictionary<string, ModeloControlGrupoContactos>();
        public static ModeloControlListaCuenta modeloListaCuenta;
        public static ModeloVentanaPrincipal modeloFormPrincipal;
        public static ModeloControlLogs modeloControlLogs;
        public static ModeloControlPerfil modeloControlPerfil;
        public static Dictionary<string, ModeloControlConversacion> modelosControlConversacion = new Dictionary<string, ModeloControlConversacion>();
        public static ModeloFormConfiguracion modeloFormConfiguracion;
        public static ModeloControlContactos modeloControlContactos;
        public static void Agregar(object Modelo)
        {
            if (Modelo == null)
            {
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlListaContactos))
            {
                modeloControlListaContacto = (ModeloControlListaContactos)Modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloFormEmoticones))
            {
                modeloFormEmoticones = (ModeloFormEmoticones)Modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlContactoItem))
            {
                var modelo = (ModeloControlContactoItem)Modelo;
                if (!modelosContacto.ContainsKey(modelo.Id))
                {
                    modelosContacto[modelo.Id] = new Dictionary<string, ModeloControlContactoItem>();
                }
                if (modelosContacto[modelo.Id].ContainsKey(modelo.IdGrupo))
                {
                    throw new Exception("Intento de agregar devuelta un modelocontrolcontactoitem para un mismo grupo");
                }
                modelosContacto[modelo.Id][modelo.IdGrupo] = modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloVentanaChat))
            {
                var modelo = (ModeloVentanaChat)Modelo;
                if (modelosVentanaChat.ContainsKey(modelo.Id))
                {
                    throw new Exception("Intento de agregar devuelta un modelo ventana chat ");
                }
                modelosVentanaChat[modelo.Id] = modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlPersonal))
            {
                modeloControlPersonal = (ModeloControlPersonal)Modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlGrupoContactos))
            {
                var modelo = (ModeloControlGrupoContactos)Modelo;
                if (modelosGrupo.ContainsKey(modelo.Id))
                {
                    throw new Exception("Intento de agregar devuelta un modelo grupo ");
                }
                modelosGrupo[modelo.Id] = modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlListaCuenta))
            {
                modeloListaCuenta = (ModeloControlListaCuenta)Modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloVentanaPrincipal))
            {
                modeloFormPrincipal = (ModeloVentanaPrincipal)Modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlLogs))
            {
                modeloControlLogs = (ModeloControlLogs)Modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlPerfil))
            {
                modeloControlPerfil = (ModeloControlPerfil)Modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlConversacion))
            {
                var modelo = (ModeloControlConversacion)Modelo;
                if (modelosControlConversacion.ContainsKey(modelo.Id))
                {
                    throw new Exception("Intento de agregar devuelta un modeloControlConversacion ");
                }
                modelosControlConversacion[modelo.Id] = modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloFormConfiguracion))
            {
                modeloFormConfiguracion = (ModeloFormConfiguracion)Modelo;
                return;
            }
            if (Modelo.GetType() == typeof(ModeloControlContactos))
            {
                modeloControlContactos = (ModeloControlContactos)Modelo;
                return;
            }
            throw new Exception("modelo no encontrado " + Modelo.GetType());
        }


        internal static void SetActualizarVentanaChatContacto(ContactoColor contacto)
        {
            if (modelosVentanaChat.ContainsKey(contacto.IdContactoColor))
            {
                modelosVentanaChat[contacto.IdContactoColor].ActualizarReceptor = true;
            }
        }
        internal static void SetActualizarIdReceptorVentanaChat(ContactoColor contacto)
        {
            if (modelosVentanaChat.ContainsKey(contacto.IdContactoColor))
            {
                modelosVentanaChat[contacto.IdContactoColor].ActualizarIdReceptor = true;
            }
        }
        internal static void SetActualizarContactoItem(ContactoColor contacto)
        {
            if (modelosContacto.ContainsKey(contacto.IdContactoColor))
            {
                foreach (ModeloControlContactoItem modelo in modelosContacto[contacto.IdContactoColor].Values)
                {
                    modelo.Actualizar = true;
                }
            }
        }

        internal static void SetActualizarContacto(ContactoColor contacto)
        {
            SetActualizarVentanaChatContacto(contacto);
            SetActualizarContactoItem(contacto);
        }
        internal static void SetActualizarYo()
        {
            foreach (ModeloVentanaChat modelo in modelosVentanaChat.Values)
            {
                modelo.ActualizarYo = true;
            }
            modeloFormPrincipal.Actualizar = true;
            modeloControlPersonal.Actualizar = true;

        }
        /// <summary>
        /// Sincroniza la lista de contactos y grupo de contactos
        /// en el ControlListaContacto
        /// </summary>
        internal static void SetActualizarListaContactos()
        {
            modeloControlListaContacto.Actualizar = true;
        }

        /// <summary>
        /// Sincroniza los contactos de todos los grupos de contacto
        /// en ControlContactos y ControlGrupoContactos
        /// </summary>
        internal static void SetActualizarGruposContactos()
        {
            //sincronizar contactos con sus correspondientes grupos
            foreach (ModeloControlGrupoContactos modelo in modelosGrupo.Values)
            {
                modelo.Actualizar = true;
            }
            modeloControlContactos.Actualizar = true;
        }

        /// <summary>
        /// sincroniza los emoticones 
        /// FormEmoticones, FormPrincipal, ControlPersonal, ControlConversacion
        /// </summary>
        internal static void SetActualizarEmoticones()
        {
            modeloFormEmoticones.Actualizar = true;
            modeloFormPrincipal.Actualizar = true;
            modeloControlPersonal.Actualizar = true;
            foreach (ModeloControlConversacion modelo in modelosControlConversacion.Values)
            {
                modelo.Actualizar = true;
            }
            
        }




    }
}
