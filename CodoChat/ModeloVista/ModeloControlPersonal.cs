﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodoChat.ModeloVista
{
    public class ModeloControlPersonal
    {
        private ControlPersonal controlPersonal;
        public Boolean Actualizar { get; set; }

        public ModeloControlPersonal(ControlPersonal controlPersonal)
        {
            this.controlPersonal = controlPersonal;
            this.Actualizar = true;
            Modelos.Agregar(this);
        }

        public void ActualizarControlPersonal()
        {
          
            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;

                    ContactoColor contacto = ColorMSN.Instancia.Yo;
                   
                    controlPersonal.Nick = ColorMSN.Instancia.Yo.Nick;
                    controlPersonal.Subnick = ColorMSN.Instancia.Yo.Subnick;
                    List<Estado> estados = new List<Estado>();
                    estados.Add(Estado.Ocupado);
                    estados.Add(Estado.Desconectado);
                    estados.Add(Estado.Ausente);
                    estados.Add(Estado.Online);
                    controlPersonal.comboEstado.DataSource = estados;
                    controlPersonal.Estado = ColorMSN.Instancia.Yo.Estado.ToString();
                    controlPersonal.Fuente = ColorMSN.Instancia.Yo.Fuente.Tipo;
                    controlPersonal.ColorFuente = ColorMSN.Instancia.Yo.Fuente.Color;
                    controlPersonal.ImagenPerfil = ColorMSN.Instancia.Yo.Imagen;
                    controlPersonal.CambiosAplicados();
                    this.Actualizar = false;

                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar control personal {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }

        }



        internal void Guardar()
        {
            if (controlPersonal.NickCambio)
            {
                if (String.IsNullOrWhiteSpace(controlPersonal.TextoNick))
                {
                    controlPersonal.Nick = Utiles.NickDefaultRTF("Mi Nick :D");
                }
                ColorMSN.Instancia.ActualizarMiNick(controlPersonal.Nick);
            }
            if (controlPersonal.SubnickCambio)
            {
                ColorMSN.Instancia.ActualizarMiSubNick(controlPersonal.Subnick);

            }
            if (controlPersonal.EstadoCambio)
            {
                Estado nuevo = (Estado)Enum.Parse(typeof(Estado), controlPersonal.Estado.ToString());
                ColorMSN.Instancia.ActualizarMiEstado(nuevo);

            }
            if (controlPersonal.ImagenCambio)
            {
                ColorMSN.Instancia.ActualizarMiImagen(controlPersonal.ImagenPerfil);

            }
            if (controlPersonal.FuenteCambio)
            {
                ColorMSN.Instancia.ActualizarMiFuente(new Fuente(controlPersonal.Fuente, controlPersonal.ColorFuente));
            }
            ColorMSN.Instancia.DeshabilitarSobrescritura(ColorMSN.Instancia.Yo);
            controlPersonal.CambiosAplicados();
        }

        internal void ReiniciarCuenta()
        {
            ColorMSN.Instancia.Fixed = false;
            GUI.CerrarCodoChat();
        }
    }
}
