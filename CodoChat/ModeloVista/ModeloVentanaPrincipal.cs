﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodoChat.ModeloVista
{
    public class ModeloVentanaPrincipal
    {
        public Boolean Actualizar { get; set; }
        VentanaPrincipal ventanaPrincipal;
        public ModeloVentanaPrincipal(VentanaPrincipal ventanaPrincipal)
        {
            this.ventanaPrincipal = ventanaPrincipal;
            this.Actualizar = true;
            Modelos.Agregar(this);
        }
        public void ActualizarFormPrincipal()
        {

            if (Actualizar && ColorMSN.Instancia != null)
            {
                try
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    Actualizar = false;
                    ContactoColor contacto = ColorMSN.Instancia.Yo;
                    Image imageEstado = Adornos.ObtenerBordeEstado(contacto.Estado);
                    ventanaPrincipal.ImagenEstado = imageEstado;

                    ventanaPrincipal.ImagenPerfil = contacto.Imagen;

                    String nickAdornado = Adornos.AdornarNick(contacto);
                    ventanaPrincipal.Nick = nickAdornado;

                    String subnickAdornado = Adornos.AdornarSubNick(contacto);
                    ventanaPrincipal.SubNick = subnickAdornado;
                    Actualizar = false;
                }
                catch (Exception e)
                {
                    ColorMSN.Instancia.addLog("Error al actualizar ventana principal {0}", e.Message);
                    ColorMSN.Instancia.addLog(e.StackTrace);
                }
            }

        }


        internal void FormCerrandose(FormClosingEventArgs e)
        {
            e.Cancel = true;
            ventanaPrincipal.Hide();
        }

        internal void MenuPerfiles(ToolStripMenuItem sender)
        {
            MenuVisible(sender.Text);
        }

        internal void TryIconDobleClick()
        {
            ventanaPrincipal.Show();
        }

        internal void MenuCerrar()
        {
            GUI.CerrarCodoChat();
        }

        internal void MenuPersonal(ToolStripMenuItem sender)
        {
            MenuVisible(sender.Text);
        }

        internal void MenuCuentas(ToolStripMenuItem sender)
        {
            MenuVisible(sender.Text);
        }

        internal void MenuLogs(ToolStripMenuItem sender)
        {
            MenuVisible(sender.Text);
        }
        private void MenuVisible(String texto)
        {

            if (!GUI.FormConfiguracion.Visible)
            {
                GUI.FormConfiguracion.Mostrar(texto);
            }
            GUI.FormConfiguracion.Show();
            GUI.FormConfiguracion.Focus();
        }

        internal void MenuContactos(ToolStripMenuItem toolStripMenuItem)
        {
            MenuVisible(toolStripMenuItem.Text);
        }
    }
}
