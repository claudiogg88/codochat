﻿using CodoChat.Forms;
using CodoChat.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodoChat.ModeloVista
{
    public class ModeloVentanaChat
    {
        ContactoColor receptor;
        public Boolean ActualizarReceptor { get; set; }
        public Boolean ActualizarYo { get; set; }


        public bool ActualizarIdReceptor { get; set; }
        VentanaChat ventanaChat;
        public ModeloVentanaChat(VentanaChat ventana, ContactoColor Receptor)
        {
            this.ventanaChat = ventana;
            this.receptor = Receptor;
            this.ActualizarYo = true;
            this.ActualizarReceptor = true;
            this.ActualizarIdReceptor = true;
            Modelos.Agregar(this);
        }
        public void ActualizarVentanaChat()
        {
            try
            {


                if (ActualizarYo && ColorMSN.Instancia != null)
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    ActualizarYo = false;
                    ContactoColor yo = ColorMSN.Instancia.Yo;
                    Image imagenEstado = Adornos.ObtenerBordeEstado(yo.Estado);
                    ventanaChat.ImagenEstadoPropia = imagenEstado;
                    ventanaChat.ImagenPerfilPropia = yo.Imagen;
                    ventanaChat.SetFuente(yo.Fuente.Tipo, yo.Fuente.Color);
                    ActualizarYo = false;
                }

                if (ActualizarReceptor && ColorMSN.Instancia != null)
                {
                    Console.WriteLine("Actualizando " + this.GetType());
                    ActualizarReceptor = false;
                    ventanaChat.SubNickContacto = Adornos.AdornarSubNick(receptor);
                    ventanaChat.NickContacto = Adornos.AdornarNick(receptor);
                    Image imagenEstado = receptor.ConectadoDesdeCodoChat ? Adornos.ObtenerBordeEstado(receptor.Estado) : Adornos.ObtenerBordeEstado(Repositorio.ContactoSimpleCol[receptor.IdContactoSimpleEnviado].Estado);
                    ventanaChat.ImagenEstadoContacto = imagenEstado;

                    ventanaChat.ImagenPerfilContacto = receptor.UsuarioColor != null ? receptor.Imagen : Repositorio.ContactoSimpleCol[receptor.IdContactoSimpleEnviado].Imagen;
                    ventanaChat.comboRedes.DataSource = receptor.GetContactoSimples();
                    ventanaChat.comboRedes.ValueMember = "Jid";
                    ventanaChat.comboRedes.DisplayMember = "NombrePublico";
                    ventanaChat.lblInfoMensaje.Text = ColorMSN.Instancia.GetInfoUltimosMensajes(receptor);
                
                    ActualizarReceptor = false;

                }
                if(ActualizarIdReceptor)
                {
                    ventanaChat.comboRedes.SelectedItem = Repositorio.ContactoSimpleCol[receptor.IdContactoSimpleEnviado];
                    ActualizarIdReceptor = false;
                }
            }
            catch (Exception e)
            {
                ColorMSN.Instancia.addLog("Error al actualizar ventanachat {0}", e.Message);
                ColorMSN.Instancia.addLog(e.StackTrace);
            }

        }
        internal void CambioRed()
        {
            ContactoSimple seleccionado = (ContactoSimple)ventanaChat.comboRedes.SelectedItem;
            receptor.IdContactoSimpleEnviado = seleccionado.IdContactoSimple;
            Image imagenEstado = receptor.ConectadoDesdeCodoChat ? Adornos.ObtenerBordeEstado(receptor.Estado) : Adornos.ObtenerBordeEstado(Repositorio.ContactoSimpleCol[receptor.IdContactoSimpleEnviado].Estado);
            ventanaChat.ImagenEstadoContacto = imagenEstado;
            ventanaChat.ImagenPerfilContacto = receptor.UsuarioColor != null ? receptor.Imagen : Repositorio.ContactoSimpleCol[receptor.IdContactoSimpleEnviado].Imagen;
            ventanaChat.lblInfoMensaje.Text = ColorMSN.Instancia.GetInfoUltimosMensajes(receptor);
        }

        internal void MensajeRecibido()
        {
            ventanaChat.lblInfoMensaje.Text = ColorMSN.Instancia.GetInfoUltimosMensajes(receptor);
            if (ActualizarIdReceptor)
            {
                ventanaChat.comboRedes.SelectedItem = Repositorio.ContactoSimpleCol[receptor.IdContactoSimpleEnviado];        
            }
        }

        internal void MensajeEnviado()
        {
            ventanaChat.lblInfoMensaje.Text = ColorMSN.Instancia.GetInfoUltimosMensajes(receptor);
        }



        public string Id { get { return this.receptor.IdContactoColor; } }

        internal void EditarFuente()
        {
            FontDialog form = Utiles.CrearFormDialogFuente();
            DialogResult result = form.ShowDialog(ventanaChat);

            if (result == DialogResult.OK)
            {
                ColorMSN.Instancia.ActualizarMiFuente(new Fuente(form.Font, form.Color));
            }
        }

        internal void SoltoTeclaControlTextoDecir(KeyEventArgs e)
        {
            KeyEventArgs ev = (KeyEventArgs)e;
            if (ev.KeyCode == Keys.Escape)
            {
                OcultarVentanaChat();
                return;
            }
            if (ev.KeyCode == Keys.Enter)
            {
                EnviarMensaje();
                return;
            }
        }
        private void EnviarMensaje()
        {
            String mensaje = ventanaChat.TextoDiciendo;
            if (!String.IsNullOrWhiteSpace(mensaje) && !mensaje.Equals("\n"))
            {
                ColorMSN.Instancia.ContactoDebeRecibirMensaje(ventanaChat.IdVentana, mensaje);
            }
            ventanaChat.BorrarTextoDecir();
        }
        private void OcultarVentanaChat()
        {
            ventanaChat.Hide();
            ventanaChat.BorrarTextoDecir();
        }
        public void VentanaChatCerrandose(FormClosingEventArgs e)
        {
            e.Cancel = true;
            ventanaChat.BorrarTextoDecir();
            ventanaChat.WindowState = FormWindowState.Minimized;
            ventanaChat.Hide();
        }
        public void SoltoTecla(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                OcultarVentanaChat();
                return;
            }
        }
        public void ClickMenuEmoticones()
        {
            if (!GUI.FormEmoticones.Visible)
            {
                GUI.FormEmoticones.Show(ventanaChat);
            }
            else
            {
                GUI.FormEmoticones.Hide();
                GUI.FormEmoticones.Show(ventanaChat);
            }
            GUI.FormEmoticones.Focus();
        }




    }
}
