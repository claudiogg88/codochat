﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Windows.Forms;
namespace CodoChat
{
    class Persistencia
    {



        public static void SerializeObject<T>(T serializableObject, string fileName)
        {
            Stream stream = null;
            try
            {
                IFormatter formatter = new BinaryFormatter();
                stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, serializableObject);

            }
            catch (Exception e)
            {
               // MessageBox.Show(estilos.Message);
                Console.WriteLine("Error en persistencia serializar " +fileName+" "+ e.Message);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

        }

        public static T DeSerializeObject<T>(string fileName)
        {
            Stream stream = null;
            try
            {
                if (File.Exists(fileName))
                {
                    IFormatter formatter = new BinaryFormatter();
                    stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    T Object = (T)formatter.Deserialize(stream);

                    return Object;
                }
                else
                {
                    Console.WriteLine("Archivo no encontrado: "+fileName);
                    return default(T);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error en persistencia al deserializar "+fileName +" "+ e.Message);
                return default(T);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }


		public static T DeSerializeByteArray<T> (byte[] p)
		{
			IFormatter formatter = new BinaryFormatter();
			T Object = (T)formatter.Deserialize(new MemoryStream(p));
			return Object;
		}
		public static T DeSerializeStream<T>(MemoryStream stream)
		{
			IFormatter formatter = new BinaryFormatter();
			T Object = (T)formatter.Deserialize(stream);
			return Object;
		}
	}
}
