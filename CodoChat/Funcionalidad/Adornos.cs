﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CodoChat.Properties;
using System.IO;
using CodoChat.Modelo;
using System.Text.RegularExpressions;
using HtmlRichText;
using System.Drawing.Imaging;
using Khendys.Controls;
using System.Web;
namespace CodoChat
{
    static class Adornos
    {


        static Image BordeConectado;
        static Image BordeDesconectado;
        static Image BordeAusente;
        static Image BordeOcupado;
        static Image ImagenDefault;
        static Image EmoticonParaBoton;
        static bool escritoSeparador;
        static ExRichTextBox conversor;
        static HtmlRichTextBox htmlRichTextBoxLocal;
        static HtmlRichTextBox htmlRichTextBoxExterno;
        public static void IniciarAdornos()
        {
            ImagenDefault = CodoChat.Properties.Resources.imagen_default;
            BordeConectado = CodoChat.Properties.Resources.disponible;
            BordeDesconectado = CodoChat.Properties.Resources.desconectado;
            BordeOcupado = CodoChat.Properties.Resources.ocupado;
            BordeAusente = CodoChat.Properties.Resources.ausente;
            EmoticonParaBoton = CodoChat.Properties.Resources.botonemoticon;
            htmlRichTextBoxLocal = new HtmlRichTextBox();
            conversor = new ExRichTextBox();
            escritoSeparador = false;
        }


        private static string AgregarEmoticonesPersonalizadosEnHTML(ContactoColor contacto, string mensaje)
        {
            Emoticon emoticon;
            string cambio;
            foreach (KeyValuePair<string, Emoticon> par in contacto.Emoticones)
            {
                emoticon = par.Value;
                if (mensaje.IndexOf(emoticon.MD5) != -1)
                {
                    String ruta = ConvertirRutaAURL(emoticon.GetRuta());
                    cambio = "<img src=\"" + ruta + "\" alt=\"" + emoticon.Nombre + "\" height=\"" + emoticon.Alto + "\" width=\"" + emoticon.Ancho + "\">";
                    mensaje = mensaje.Replace(emoticon.MD5, cambio);
                }
            }
            return mensaje;
        }


        /// <summary>
        /// cambia las abreviatura por los md5 que tienen menos posibilidades
        /// de colisionar con alguna palabra clave en el html u otras cosas
        /// </summary>
        /// <param name="textoRtf"></param>
        /// <param name="contacto"></param>
        /// <returns></returns>
        private static String TaguearEmoticonesDelTexto(string textoRtf, ContactoColor contacto)
        {

            List<Emoticon> unidos = new List<Emoticon>();
            unidos.AddRange(ColorMSN.Instancia.Emoticones.Values);
            unidos.AddRange(contacto.Emoticones.Values);
            unidos = unidos.OrderByDescending(d => d.Abreviatura.Length).ToList();

            foreach (Emoticon emoticon in unidos)
            {
                textoRtf = textoRtf.Replace(emoticon.Abreviatura, emoticon.MD5);
            }
            return textoRtf;
        }

        /// <summary>
        /// Agrega emoticones predefinidos y propios en un texto rft
        /// </summary>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        public static string AdornarNick(ContactoColor contacto)
        {
            string nickEnRtf = contacto.Nick;
            if (String.IsNullOrEmpty(nickEnRtf))
            {
                return Utiles.NickDefaultRTF(contacto.IdContactoColor);
            }
            else
            {
                return AgregarEmoticonesEnRTF(contacto, nickEnRtf);
            }
        }

        public static string AdornarSubNick(ContactoColor contacto)
        {

            string subNickEnRtf = contacto.Subnick;
            if (String.IsNullOrEmpty(subNickEnRtf))
            {
                return "";
            }
            else
            {
                return AgregarEmoticonesEnRTF(contacto, subNickEnRtf);
            }
        }
        public static string AdornarNickConversacion(ContactoColor contacto)
        {
            return ConvertirRTFAHTMLConEmoticonesYLinks(contacto, contacto.Nick);
        }

        /// <summary>
        /// Le agregamos onda al mensaje, emoticones y convertimos links en links
        /// </summary>
        /// <param name="contacto">contacto del que se sacan los emoticones</param>
        /// <param name="mensaje">mensaje en texto plano</param>
        /// <returns></returns>
        public static string AdornarMensajeConversacion(ContactoColor contacto, String mensaje)
        {
            return ConvertirTextoPlanoAHTMLConEmoticonesYLinks(contacto, mensaje);
        }
        private static string ConvertirTextoPlanoAHTMLConEmoticonesYLinks(ContactoColor contacto, String mensaje)
        {
            Dictionary<string, string> links = new Dictionary<string, string>();
            mensaje = TaguearLinkEnString(mensaje, links);
            mensaje = TaguearEmoticonesDelTexto(mensaje, contacto);
            mensaje = HttpUtility.HtmlEncode(mensaje);
            mensaje = AgregarLinkEnString(mensaje, links);
            mensaje = AgregarEmoticonesDefaultEnHTML(mensaje);
            mensaje = AgregarEmoticonesPersonalizadosEnHTML(contacto, mensaje);
            return mensaje;
        }
        private static string AgregarEmoticonesDefaultEnHTML(string mensaje)
        {
            Emoticon emoticon;
            string cambio;
            foreach (KeyValuePair<string, Emoticon> par in ColorMSN.Instancia.Emoticones)
            {
                emoticon = par.Value;
                if (mensaje.IndexOf(emoticon.MD5) != -1 && emoticon.Abreviatura.Equals(emoticon.Nombre))
                {
                    String ruta = ConvertirRutaAURL(emoticon.GetRuta());
                    cambio = "<img src=\"" + ruta + "\" alt=\"" + emoticon.Nombre + "\" height=\"" + emoticon.Alto + "\" width=\"" + emoticon.Ancho + "\">";
                    mensaje = mensaje.Replace(emoticon.MD5, cambio);
                }
            }
            return mensaje;
        }
        private static string ConvertirRutaAURL(String original)
        {
            var uri = new System.Uri(original);
            var converted = uri.AbsoluteUri;
            return converted;
        }
        private static string AgregarLinkEnString(String html)
        {
            return Regex.Replace(html, @"((http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z‌​0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*)", @"<a href='$1'>$1</a>");
        }

        private static string TaguearLinkEnString(String html, Dictionary<string, string> links)
        {

            var palabras = html.Split();
            string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            foreach(var palabra in palabras){
                 if(reg.IsMatch(palabra)){
                     String clave = Utiles.GenerarClave();
                     links[clave] = palabra.ToString();
                     html = html.Replace(palabra, clave);
                 }
            }
            return html;
        }
        private static string AgregarLinkEnString(String html, Dictionary<string, string> links)
        {
            foreach (String clave in links.Keys)
            {
                html = html.Replace(clave, String.Format("<a href='{0}'>{0}</a>", links[clave]));
            }
            return html;
        }

        private static string ConvertirRTFAHTMLConEmoticonesYLinks(ContactoColor contacto, String texto)
        {
            texto = TaguearEmoticonesDelTexto(texto, contacto);
            String html = ConvertirRTFAHTML(texto);
            html = AgregarEmoticonesDefaultEnHTML(html);
            html = AgregarEmoticonesPersonalizadosEnHTML(contacto, html);
            html = AgregarLinkEnString(html);
            return html;
        }

        public static string CalcularRTF(Image imagen)
        {
            conversor.Clear();
            conversor.InsertImage(imagen);
            String imagenTexto = conversor.Rtf;
            imagenTexto = imagenTexto.Replace(@"}\par", "}");
            return imagenTexto;
        }

        private static string ConvertirRTFAHTML(string texto)
        {
            HtmlRichTextBox seleccionado = null;
            if (!htmlRichTextBoxLocal.InvokeRequired)
            {
                seleccionado = htmlRichTextBoxLocal;
            }
            else
            {
                if (htmlRichTextBoxExterno == null)
                {
                    htmlRichTextBoxExterno = new HtmlRichTextBox();
                    seleccionado = htmlRichTextBoxExterno;
                }
                else
                {
                    if (htmlRichTextBoxExterno.InvokeRequired)
                    {
                        Console.WriteLine("Se creo un nuevo conversor....");
                        htmlRichTextBoxExterno = new HtmlRichTextBox();

                    }
                    seleccionado = htmlRichTextBoxExterno;
                }
            }
            seleccionado.Rtf = texto;
            String html = seleccionado.GetHTML(true, true);
            return html;
        }

        public static String GetRutaSeparador()
        {
            string ruta = Path.Combine(System.IO.Path.GetTempPath(), "separador");
            if (!escritoSeparador)
            {
                CodoChat.Properties.Resources.separador1.Save(ruta);
                escritoSeparador = true;
            }
            return ConvertirRutaAURL(ruta);
        }
        private static string AgregarEmoticonesEnRTF(ContactoColor contacto, String textoRtf)
        {
            string abreviatura;
            List<Emoticon> unidos = new List<Emoticon>();
            unidos.AddRange(ColorMSN.Instancia.Emoticones.Values);
            unidos.AddRange(contacto.Emoticones.Values);
            unidos = unidos.OrderByDescending(d => d.Abreviatura.Length).ToList();

            foreach (Emoticon emoticon in unidos)
            {
                abreviatura = emoticon.Abreviatura;
                if (textoRtf.IndexOf(abreviatura) != -1)
                {
                    textoRtf = textoRtf.Replace(abreviatura, emoticon.RTF);
                }
            }


            return textoRtf;
        }



        public static Image ExtraerRectangulo(Image original, Rectangle rectangulo)
        {
            Bitmap target = new Bitmap(rectangulo.Width, rectangulo.Height);

            using (Graphics g = Graphics.FromImage(target))
            {
                g.DrawImage(original, new Rectangle(0, 0, target.Width, target.Height),
                                 rectangulo,
                                 GraphicsUnit.Pixel);
            }
            return target;
        }




        public static Image GetImagenDefault()
        {
            return ImagenDefault.Clone() as Image;
        }
        public static Image ObtenerBordeEstado(Estado estado)
        {
            if (estado == Estado.Desconectado)
                return BordeDesconectado;
            if (estado == Estado.Online)
                return BordeConectado;
            if (estado == Estado.Ausente)
                return BordeAusente;
            if (estado == Estado.Ocupado)
                return BordeOcupado;
            return BordeDesconectado;
        }

        public static Image CuadrarImagen(Image original)
        {
            int ancho = original.Width;
            int alto = original.Height;
            int izq = 0, der = 0, sup = 0, inf = 0; ;


            if (ancho > alto)
            {
                izq = (ancho - alto) / 2;
                der = ancho - izq * 2;
                sup = alto;
                inf = 0;
            }
            if (alto > ancho)
            {
                inf = (alto - ancho) / 2;
                sup = alto - inf * 2;
                izq = 0;
                der = ancho;

            }
            if (alto == ancho)
            {
                return original;
            }

            Rectangle rect = new Rectangle(izq, inf, der, sup);
            Bitmap copia = new Bitmap(original);
            Bitmap nueva = copia.Clone(rect, copia.PixelFormat);

            return nueva;
        }


        private static void CopyRegionIntoImage(Image origen, Rectangle rectOrigen, Image destino, Rectangle rectDestino)
        {
            //imagen sobre la que se va a dibujar
            using (Graphics grD = Graphics.FromImage(destino))
            {
                //dibujamos el rectangulo origen de la imagen origen
                //en la parte rectangulo destino de la imagen destino
                grD.DrawImage(origen, rectOrigen);
            }
        }

        public static bool ImagenesIguales(Image uno, Image dos)
        {
            string img1_ref;
            string img2_ref;
            Bitmap img1 = new Bitmap(uno.Clone() as Image);
            Bitmap img2 = new Bitmap(dos.Clone() as Image);
            if (img1.Width == img2.Width && img1.Height == img2.Height)
            {
                int ancho = img2.Width / 4;
                int alto = img2.Height / 4;

                for (int i = 0; i < ancho; i++)
                {
                    for (int j = 0; j < alto; j++)
                    {
                        img1_ref = img1.GetPixel(i, j).ToString();
                        img2_ref = img2.GetPixel(i, j).ToString();
                        if (img1_ref != img2_ref)
                        {
                            return false;
                        }

                    }
                }
                return true;

            }
            else
                return false;
        }


        public static Dictionary<string, Emoticon> IniciarEmoticonesDefault()
        {
            if (!Directory.Exists(Emoticon.directorio))
            {
                Directory.CreateDirectory(Emoticon.directorio);
            }
            Dictionary<string, Emoticon> emoticones = new Dictionary<string, Emoticon>();
            //abreviatura,   nombre abreviatura
            emoticones.Add(":-)", new Emoticon(":-)", ":-)", CodoChat.Properties.Resources._1, false));
            emoticones.Add(":)", new Emoticon(":-)", ":)", CodoChat.Properties.Resources._1, false));

            emoticones.Add(":-D", new Emoticon(":-D", ":-D", CodoChat.Properties.Resources._2, false));
            emoticones.Add(":D", new Emoticon(":-D", ":D", CodoChat.Properties.Resources._2, false));
            emoticones.Add(":d", new Emoticon(":-D", ":d", CodoChat.Properties.Resources._2, false));
            emoticones.Add(":-d", new Emoticon(":-D", ":-d", CodoChat.Properties.Resources._2, false));

            emoticones.Add(";-)", new Emoticon(";-)", ";-)", CodoChat.Properties.Resources._3, false));
            emoticones.Add(";)", new Emoticon(";-)", ";)", CodoChat.Properties.Resources._3, false));

            emoticones.Add(":-O", new Emoticon(":-O", ":-O", CodoChat.Properties.Resources._4, false));
            emoticones.Add(":O", new Emoticon(":-O", ":O", CodoChat.Properties.Resources._4, false));
            emoticones.Add(":o", new Emoticon(":-O", ":o", CodoChat.Properties.Resources._4, false));
            emoticones.Add(":-o", new Emoticon(":-O", ":-o", CodoChat.Properties.Resources._4, false));

            emoticones.Add(":-P", new Emoticon(":-P", ":-P", CodoChat.Properties.Resources._5, false));
            emoticones.Add(":P", new Emoticon(":-P", ":P", CodoChat.Properties.Resources._5, false));
            emoticones.Add(":p", new Emoticon(":-P", ":p", CodoChat.Properties.Resources._5, false));
            emoticones.Add(":-p", new Emoticon(":-P", ":-p", CodoChat.Properties.Resources._5, false));

            emoticones.Add("(H)", new Emoticon("(H)", "(H)", CodoChat.Properties.Resources._6, false));
            emoticones.Add("(h)", new Emoticon("(H)", "(h)", CodoChat.Properties.Resources._6, false));

            emoticones.Add(":-@", new Emoticon(":-@", ":-@", CodoChat.Properties.Resources._7, false));
            emoticones.Add(":@", new Emoticon(":-@", ":@", CodoChat.Properties.Resources._7, false));

            emoticones.Add(":-$", new Emoticon(":-$", ":-$", CodoChat.Properties.Resources._8, false));
            emoticones.Add(":$", new Emoticon(":-$", ":$", CodoChat.Properties.Resources._8, false));

            emoticones.Add(":-S", new Emoticon(":-S", ":-S", CodoChat.Properties.Resources._9, false));
            emoticones.Add(":S", new Emoticon(":-S", ":S", CodoChat.Properties.Resources._9, false));
            emoticones.Add(":s", new Emoticon(":-S", ":s", CodoChat.Properties.Resources._9, false));
            emoticones.Add(":-s", new Emoticon(":-S", ":-s", CodoChat.Properties.Resources._9, false));

            emoticones.Add(":-(", new Emoticon(":-(", ":-(", CodoChat.Properties.Resources._10, false));
            emoticones.Add(":(", new Emoticon(":-(", ":(", CodoChat.Properties.Resources._10, false));

            emoticones.Add(":'(", new Emoticon(":'(", ":'(", CodoChat.Properties.Resources._11, false));

            emoticones.Add(":-|", new Emoticon(":-|", ":-|", CodoChat.Properties.Resources._12, false));
            emoticones.Add(":|", new Emoticon(":-|", ":|", CodoChat.Properties.Resources._12, false));

            emoticones.Add("(6)", new Emoticon("(6)", "(6)", CodoChat.Properties.Resources._13, false));

            emoticones.Add("(A)", new Emoticon("(A)", "(A)", CodoChat.Properties.Resources._14, false));
            emoticones.Add("(a)", new Emoticon("(A)", "(a)", CodoChat.Properties.Resources._14, false));

            emoticones.Add("(L)", new Emoticon("(L)", "(L)", CodoChat.Properties.Resources._15, false));
            emoticones.Add("(l)", new Emoticon("(L)", "(l)", CodoChat.Properties.Resources._15, false));

            emoticones.Add("(U)", new Emoticon("(U)", "(U)", CodoChat.Properties.Resources._16, false));
            emoticones.Add("(u)", new Emoticon("(U)", "(u)", CodoChat.Properties.Resources._16, false));

            emoticones.Add("(M)", new Emoticon("(M", "(M)", CodoChat.Properties.Resources._17, false));
            emoticones.Add("(m)", new Emoticon("(M)", "(m)", CodoChat.Properties.Resources._17, false));

            emoticones.Add("(@)", new Emoticon("(@)", "(@)", CodoChat.Properties.Resources._18, false));

            emoticones.Add("(&)", new Emoticon("(&)", "(&)", CodoChat.Properties.Resources._19, false));

            emoticones.Add("(S)", new Emoticon("(S)", "(S)", CodoChat.Properties.Resources._20, false));
            emoticones.Add("(s)", new Emoticon("(S)", "(s)", CodoChat.Properties.Resources._10, false));

            emoticones.Add("(*)", new Emoticon("(*)", "(*)", CodoChat.Properties.Resources._21, false));

            emoticones.Add("(~)", new Emoticon("(~)", "(~)", CodoChat.Properties.Resources._22, false));

            emoticones.Add("(8)", new Emoticon("(8)", "(8)", CodoChat.Properties.Resources._23, false));

            emoticones.Add("(E)", new Emoticon("(E)", "(E)", CodoChat.Properties.Resources._24, false));
            emoticones.Add("(e)", new Emoticon("(e)", "(e)", CodoChat.Properties.Resources._24, false));

            emoticones.Add("(F)", new Emoticon("(F)", "(F)", CodoChat.Properties.Resources._25, false));
            emoticones.Add("(f)", new Emoticon("(F)", "(f)", CodoChat.Properties.Resources._25, false));

            emoticones.Add("(W)", new Emoticon("(W)", "(W)", CodoChat.Properties.Resources._26, false));
            emoticones.Add("(w)", new Emoticon("(W)", "(w)", CodoChat.Properties.Resources._26, false));

            emoticones.Add("(O)", new Emoticon("(O)", "(O)", CodoChat.Properties.Resources._27, false));
            emoticones.Add("(o)", new Emoticon("(O)", "(o)", CodoChat.Properties.Resources._27, false));

            emoticones.Add("(K)", new Emoticon("(K)", "(K)", CodoChat.Properties.Resources._28, false));
            emoticones.Add("(k)", new Emoticon("(K)", "(k)", CodoChat.Properties.Resources._28, false));

            emoticones.Add("(G)", new Emoticon("(G)", "(G)", CodoChat.Properties.Resources._29, false));
            emoticones.Add("(g)", new Emoticon("(K)", "(g)", CodoChat.Properties.Resources._29, false));

            emoticones.Add("(^)", new Emoticon("(^)", "(^)", CodoChat.Properties.Resources._30, false));

            emoticones.Add("(P)", new Emoticon("(P)", "(P)", CodoChat.Properties.Resources._31, false));
            emoticones.Add("(p)", new Emoticon("(P)", "(p)", CodoChat.Properties.Resources._31, false));

            emoticones.Add("(I)", new Emoticon("(I)", "(I)", CodoChat.Properties.Resources._32, false));
            emoticones.Add("(i)", new Emoticon("(I)", "(i)", CodoChat.Properties.Resources._32, false));

            emoticones.Add("(C)", new Emoticon("(C)", "(C)", CodoChat.Properties.Resources._33, false));
            emoticones.Add("(c)", new Emoticon("(C)", "(c)", CodoChat.Properties.Resources._33, false));

            emoticones.Add("(T)", new Emoticon("(T)", "(T)", CodoChat.Properties.Resources._34, false));
            emoticones.Add("(t)", new Emoticon("(T)", "(t)", CodoChat.Properties.Resources._34, false));
            emoticones.Add("({)", new Emoticon("({)", "({)", CodoChat.Properties.Resources._35, false));

            emoticones.Add("(})", new Emoticon("(})", "(})", CodoChat.Properties.Resources._36, false));

            emoticones.Add("(B)", new Emoticon("(B)", "(B)", CodoChat.Properties.Resources._37, false));
            emoticones.Add("(b)", new Emoticon("(B)", "(b)", CodoChat.Properties.Resources._37, false));

            emoticones.Add("(D)", new Emoticon("(D)", "(D)", CodoChat.Properties.Resources._38, false));
            emoticones.Add("(d)", new Emoticon("(D)", "(d)", CodoChat.Properties.Resources._38, false));

            emoticones.Add("(Z)", new Emoticon("(Z)", "(Z)", CodoChat.Properties.Resources._39, false));
            emoticones.Add("(z)", new Emoticon("(Z)", "(z)", CodoChat.Properties.Resources._39, false));

            emoticones.Add("(X)", new Emoticon("(X)", "(X)", CodoChat.Properties.Resources._40, false));
            emoticones.Add("(x)", new Emoticon("(X)", "(x)", CodoChat.Properties.Resources._40, false));

            emoticones.Add("(Y)", new Emoticon("(Y)", "(Y)", CodoChat.Properties.Resources._41, false));
            emoticones.Add("(y)", new Emoticon("(Y)", "(y)", CodoChat.Properties.Resources._41, false));

            emoticones.Add("(N)", new Emoticon("(N)", "(N)", CodoChat.Properties.Resources._42, false));
            emoticones.Add("(n)", new Emoticon("(N)", "(n)", CodoChat.Properties.Resources._42, false));

            emoticones.Add(":-[", new Emoticon(":-[", ":-[", CodoChat.Properties.Resources._43, false));
            emoticones.Add(":[", new Emoticon(":-[", ":[", CodoChat.Properties.Resources._43, false));


            emoticones.Add("(#)", new Emoticon("(#)", "(#)", CodoChat.Properties.Resources._46, false));

            emoticones.Add("(R)", new Emoticon("(R)", "(R)", CodoChat.Properties.Resources._47, false));
            emoticones.Add("(r)", new Emoticon("(R)", "(r)", CodoChat.Properties.Resources._47, false));

            emoticones.Add(":-#", new Emoticon(":-#", ":-#", CodoChat.Properties.Resources._48, false));
            emoticones.Add(":#", new Emoticon(":-#", ":#", CodoChat.Properties.Resources._48, false));

            emoticones.Add("8o|", new Emoticon("8o|", "8o|", CodoChat.Properties.Resources._49, false));

            emoticones.Add("8-|", new Emoticon("8-|", "8-|", CodoChat.Properties.Resources._50, false));

            emoticones.Add("^o)", new Emoticon("^o)", "^o)", CodoChat.Properties.Resources._51, false));

            emoticones.Add(":-*", new Emoticon(":-*", ":-*", CodoChat.Properties.Resources._52, false));

            emoticones.Add("+o(", new Emoticon("+o(", "+o(", CodoChat.Properties.Resources._53, false));

            emoticones.Add("(SN)", new Emoticon("(SN)", "(SN)", CodoChat.Properties.Resources._54, false));
            emoticones.Add("(sn)", new Emoticon("(SN)", "(sn)", CodoChat.Properties.Resources._54, false));

            emoticones.Add("(TU)", new Emoticon("(TU)", "(TU)", CodoChat.Properties.Resources._55, false));
            emoticones.Add("(tu)", new Emoticon("(TU)", "(tu)", CodoChat.Properties.Resources._55, false));

            emoticones.Add("(PL)", new Emoticon("(PL)", "(PL)", CodoChat.Properties.Resources._56, false));
            emoticones.Add("(pl)", new Emoticon("(PL)", "(pl)", CodoChat.Properties.Resources._56, false));

            emoticones.Add("(||)", new Emoticon("(||)", "(||)", CodoChat.Properties.Resources._57, false));

            emoticones.Add("(PI)", new Emoticon("(PI)", "(PI)", CodoChat.Properties.Resources._58, false));
            emoticones.Add("(pi)", new Emoticon("(PI)", "(pi)", CodoChat.Properties.Resources._58, false));

            emoticones.Add("(SO)", new Emoticon("(SO)", "(SO)", CodoChat.Properties.Resources._59, false));
            emoticones.Add("(so)", new Emoticon("(SO)", "(so)", CodoChat.Properties.Resources._59, false));

            emoticones.Add("(AU)", new Emoticon("(AU)", "(AU)", CodoChat.Properties.Resources._60, false));
            emoticones.Add("(au)", new Emoticon("(AU)", "(au)", CodoChat.Properties.Resources._60, false));

            emoticones.Add("(AP)", new Emoticon("(AP)", "(AP)", CodoChat.Properties.Resources._61, false));
            emoticones.Add("(ap)", new Emoticon("(AP)", "(ap)", CodoChat.Properties.Resources._61, false));

            emoticones.Add("(UM)", new Emoticon("(UM)", "(UM)", CodoChat.Properties.Resources._62, false));
            emoticones.Add("(um)", new Emoticon("(UM)", "(um)", CodoChat.Properties.Resources._62, false));

            emoticones.Add("(IP)", new Emoticon("(IP)", "(IP)", CodoChat.Properties.Resources._63, false));
            emoticones.Add("(ip)", new Emoticon("(IP)", "(ip)", CodoChat.Properties.Resources._63, false));

            emoticones.Add("(PC)", new Emoticon("(PC)", "(PC)", CodoChat.Properties.Resources._64, false));
            emoticones.Add("(pc)", new Emoticon("(PC)", "(pc)", CodoChat.Properties.Resources._64, false));

            emoticones.Add("(MP)", new Emoticon("(MP)", "(MP)", CodoChat.Properties.Resources._65, false));
            emoticones.Add("(mp)", new Emoticon("(MP)", "(mp)", CodoChat.Properties.Resources._65, false));

            emoticones.Add("(BRB)", new Emoticon("(BRB)", "(BRB)", CodoChat.Properties.Resources._66, false));
            emoticones.Add("(brb)", new Emoticon("(BRB)", "(brb)", CodoChat.Properties.Resources._66, false));

            emoticones.Add("(ST)", new Emoticon("(ST)", "(ST)", CodoChat.Properties.Resources._67, false));
            emoticones.Add("(st)", new Emoticon("(ST)", "(st)", CodoChat.Properties.Resources._67, false));


            emoticones.Add("(YN)", new Emoticon("(YN)", "(YN)", CodoChat.Properties.Resources._68, false));
            emoticones.Add("(yn)", new Emoticon("(YN)", "(yn)", CodoChat.Properties.Resources._68, false));

            emoticones.Add("(H5)", new Emoticon("(H5)", "(H5)", CodoChat.Properties.Resources._69, false));
            emoticones.Add("(h5)", new Emoticon("(H5)", "(h5)", CodoChat.Properties.Resources._69, false));

            emoticones.Add("(MO)", new Emoticon("(MO)", "(MO)", CodoChat.Properties.Resources._70, false));
            emoticones.Add("(mo)", new Emoticon("(MO)", "(mo)", CodoChat.Properties.Resources._70, false));

            emoticones.Add("(BAH)", new Emoticon("(BAH)", "(BAH)", CodoChat.Properties.Resources._71, false));
            emoticones.Add("(bah)", new Emoticon("(BAH)", "(bah)", CodoChat.Properties.Resources._71, false));

            emoticones.Add(":^)", new Emoticon(":^)", ":^)", CodoChat.Properties.Resources._72, false));

            emoticones.Add("*-)", new Emoticon("*-)", "*-)", CodoChat.Properties.Resources._73, false));

            emoticones.Add("(LI)", new Emoticon("(LI)", "(LI)", CodoChat.Properties.Resources._74, false));
            emoticones.Add("(li)", new Emoticon("(LI)", "(li)", CodoChat.Properties.Resources._74, false));

            emoticones.Add("<:o)", new Emoticon("<:o)", "<:o)", CodoChat.Properties.Resources._75, false));

            emoticones.Add("(NAH)", new Emoticon("(NAH)", "(NAH)", CodoChat.Properties.Resources._79, false));
            emoticones.Add("(nah)", new Emoticon("(NAH)", "(nah)", CodoChat.Properties.Resources._79, false));

            emoticones.Add("('.')", new Emoticon("('.')", "('.')", CodoChat.Properties.Resources._81, false));

            emoticones.Add("|-)", new Emoticon("|-)", "|-)", CodoChat.Properties.Resources._82, false));

            return emoticones;
        }






    }
}
