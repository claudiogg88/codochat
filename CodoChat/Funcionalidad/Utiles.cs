﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using CodoChat.Modelo;
using System.Net;
using System.Drawing;
using System.Security.Cryptography;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;


namespace CodoChat
{
    class Utiles
    {
        public static new Dictionary<String, String> CuentasDisponibles = new Dictionary<String, String>() { { NombreCliente.Facebook, "@chat.facebook.com" }, { NombreCliente.Google, "@gmail.com" } };






        private static FontDialog CrearFormDialogNick(int maxSize, Font font, Color color)
        {
            FontDialog form = new FontDialog();
            form.MaxSize = maxSize;
            form.MinSize = maxSize;
            form.Font = font;
            form.Color = color;
            form.AllowScriptChange = false;
            form.AllowVerticalFonts = false;
            form.AllowVectorFonts = false;
            form.ShowColor = true;
            return form;
        }
        public static FontDialog CrearFormDialogNick(Font font, Color color)
        {
            return CrearFormDialogNick(18, font, color);
        }
        public static FontDialog CrearFormDialogSubNick(Font font, Color color)
        {
            return CrearFormDialogNick(15, font, color);
        }
        public static FontDialog CrearFormDialogFuente()
        {
            FontDialog form = new FontDialog();
            Fuente fuente = ColorMSN.Instancia.GetFuente();
            form.ShowColor = true;
            form.Font = fuente.Tipo;
            form.Color = fuente.Color;
            form.MaxSize = 36;
            form.MinSize = 18;
            form.AllowScriptChange = false;
            form.AllowVerticalFonts = false;
            form.AllowVectorFonts = false;
            return form;
        }


        public static byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        public static Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }

        /// <summary>
        /// verifica si existe el directorio
        /// si no existe lo creacrea
        /// </summary>
        /// <param name="ruta">ruta a verificar</param>
        public static void VerificarDirectorio(string ruta)
        {
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }
        public static string eliminarSaltoDeLinea(string entrada)
        {
            return entrada.Substring(0, entrada.Length - 1);
        }
        /// <summary>
        /// Convierte la fecha pasada a utc
        /// y calcula los segundos que pasaron desde el 2000
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        internal static long segundosDesdeEl2000(DateTime fecha)
        {
            DateTime el2000 = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)fecha.ToUniversalTime().Subtract(el2000).TotalSeconds;
        }
        internal static DateTime fechaSegundosDesdeEl2000(string actualizado)
        {
            long segundosDesdeEl2000 = Convert.ToInt64(actualizado);
            DateTime el2000 = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime fechaOriginal = el2000.AddSeconds(segundosDesdeEl2000).ToLocalTime();
            return fechaOriginal;
        }

        internal static ColorMSN LevantarCuenta()
        {

            ColorMSN cuenta = Persistencia.DeSerializeObject<ColorMSN>(GetRutaApp() + "cuenta.dat");
            return cuenta;
        }

        internal static void GuardarCuenta(ColorMSN cuenta)
        {
            Persistencia.SerializeObject<ColorMSN>(cuenta, GetRutaApp() + "cuenta.dat");
        }


        internal static string GetRutaApp()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CodoChat\\");
        }
        internal static void GuardarContacto(ContactoColor contactoColor)
        {
            Persistencia.SerializeObject<ContactoColor>(contactoColor, GetRutaApp() + "yo.dat");
        }


        public static MemoryStream SerializeToStream<T>(T datos)
        {
            MemoryStream stream = new MemoryStream();
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, datos);
            return stream;
        }

        public static MemoryStream ConvertirStreamEnMemoryStream(Stream stream)
        {
            System.IO.MemoryStream memStream = new System.IO.MemoryStream();

            byte[] buffer = new byte[1024];
            int byteCount;
            do
            {
                byteCount = stream.Read(buffer, 0, buffer.Length);
                memStream.Write(buffer, 0, byteCount);
            } while (byteCount > 0);

            memStream.Position = 0;
            return memStream;
        }



        internal static void ImprimirExcepcion(Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
        }


        public static object DeserializeBase64(string s)
        {
            // We need to know the exact length of the string - Base64 can sometimes pad us by a byte or two
            int p = s.IndexOf(':');
            int length = Convert.ToInt32(s.Substring(0, p));

            // Extract data from the base 64 string!
            byte[] memorydata = Convert.FromBase64String(s.Substring(p + 1));
            MemoryStream rs = new MemoryStream(memorydata, 0, length);
            BinaryFormatter sf = new BinaryFormatter();
            object o = sf.Deserialize(rs);
            return o;
        }
        public static string SerializeBase64(object o)
        {
            // Serialize to a base 64 string
            byte[] bytes;
            long length = 0;
            MemoryStream ws = new MemoryStream();
            BinaryFormatter sf = new BinaryFormatter();
            sf.Serialize(ws, o);
            length = ws.Length;
            bytes = ws.GetBuffer();
            string encodedData = bytes.Length + ":" + Convert.ToBase64String(bytes, 0, bytes.Length, Base64FormattingOptions.None);
            return encodedData;
        }

        public static string CalcularMD5(Image image)
        {
            byte[] bytes = null;
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat); // gif for example
                bytes = ms.ToArray();
            }

            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(bytes);

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }




        //public static string CalcularMD5(Image imagen)
        //{
        //    string strImagen = SerializeBase64(imagen);
        //    string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
        //    Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
        //    strImagen = r.Replace(strImagen, "");


        //    int t = strImagen.Length;
        //    int d = t / 20;
        //    string strHash = "";
        //    for(int i = 0 ; i< t ; i+=d)
        //    {
        //        strHash += strImagen[i];
        //    }

        //    return strHash;
        //}


        internal static bool CompararImagenes(Image image1, Image image2)
        {
            string str1 = SerializeBase64(image1);
            string str2 = SerializeBase64(image2);
            int d = 0;
            int t = str1.Length;
            int max = t / 100;
            if (str1.Length != str2.Length)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < t; i++)
                {
                    if (str1[i] != str2[i])
                    {
                        d++;
                    }
                    if (d > max)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public static string NickDefaultRTF(string texto)
        {

            String original = @"{\rtf1\ansi\ansicpg1252\deff0\deflang3082{\fonttbl{\f0\fnil\fcharset0 Microsoft Sans Serif;}}\viewkind4\uc1\pard\b\f0\fs%LETRAPOR2% %REEMPLAZO%\par}";
            return original.Replace("%LETRAPOR2%", (18 * 2) + "").Replace("%REEMPLAZO%", texto);
        }

        public static string SubNickDefaultRTF(string texto)
        {
            String original = @"{\rtf1\ansi\ansicpg1252\deff0\deflang3082{\fonttbl{\f0\fnil\fcharset0 Microsoft Sans Serif;}}{\colortbl ;\red0\green0\blue0;}\viewkind4\uc1\pard\cf1\i\f0\fs%LETRAPOR2% %REEMPLAZO%\cf0\b\i0\par";
            return original.Replace("%LETRAPOR2%", (15 * 2) + "").Replace("%REEMPLAZO%", texto);
        }


        internal static string RTFATextoPlano(string texto)
        {

            using (RichTextBox rtb = new RichTextBox())
            {
                rtb.Rtf = texto;
                return rtb.Text;
            }
        }

        internal static Cuenta InstanciarCuenta(string idCuenta, string usuario, string clave)
        {
            if (idCuenta.Equals(NombreCliente.Facebook))
            {
                return new CuentaFacebook(usuario, clave);
            }
            if (idCuenta.Equals(NombreCliente.Google))
            {
                return new CuentaGoogle(usuario, clave);
            }
            return null;
        }

        internal static String GetNickAdornado(ContactoColor contacto)
        {
            return Adornos.AdornarNick(contacto);
        }
        internal static String GetSubNickAdornado(ContactoColor contacto)
        {
            return Adornos.AdornarSubNick(contacto);
        }
        public static string CalcularRTF(Image imagen)
        {
            return Adornos.CalcularRTF(imagen);
        }


        internal static bool ExportarEmoticones(string ruta)
        {
            try
            {
                Persistencia.SerializeObject<Dictionary<String, Emoticon>>(ColorMSN.Instancia.Yo.Emoticones, ruta);
                return true;
            }
            catch (Exception e)
            {
                ColorMSN.Instancia.addLog("Fallo exportacion de emoticones causa: " + e.Message);
                ImprimirExcepcion(e);
                return false;
            }
        }




        private static Random random = new Random((int)DateTime.Now.Ticks);//thanks to McAden
        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }


        public static string GenerarClave()
        {
            return RandomString(15);
        }
        public static string GenerarClave(int largo)
        {
            return RandomString(largo);
        }

        internal static void GuardarImagenes(Dictionary<string, Emoticon> nueva)
        {
            foreach (Emoticon emoticon in ColorMSN.Instancia.Yo.Emoticones.Values)
            {
                if (!emoticon.Guardada())
                {
                    emoticon.GuardarImagen();
                }
            }
        }
        internal static void ChequearEmoticones()
        {
            //elimino emoticones repetidos
            bool cambio = false;
            List<string> limpios = new List<string>();
            Dictionary<string, Emoticon> nuevos = new Dictionary<string, Emoticon>();
            foreach (Emoticon def in ColorMSN.Instancia.Emoticones.Values)
            {
                limpios.Add(def.MD5);
            }
            foreach (Emoticon emoticon in ColorMSN.Instancia.Yo.Emoticones.Values)
            {
                if (!limpios.Contains(emoticon.MD5))
                {
                    nuevos[emoticon.Abreviatura] = emoticon;
                    limpios.Add(emoticon.MD5);
                }
                else
                {
                    ColorMSN.Instancia.addLog("Eliminado emoticon {0} con imagen repetida...", emoticon.Abreviatura);
                    cambio = true;
                }
            }
            if (cambio)
            {
                ColorMSN.Instancia.Yo.Emoticones = nuevos;
                ColorMSN.Instancia.Yo.UltimaActualizacion = DateTime.Now;
                ColorMSN.Instancia.addLog("Cambio mi lista de emoticones....");
            }

            //guardar devuelta las imagenes inexistentes
            if (!Directory.Exists(Emoticon.directorio))
            {
                Directory.CreateDirectory(Emoticon.directorio);
            }
            foreach (Emoticon emoticon in ColorMSN.Instancia.Emoticones.Values)
            {
                if (!File.Exists(emoticon.GetRuta()))
                {
                    emoticon.GuardarImagen();
                }
            }
            foreach (Emoticon emoticon in ColorMSN.Instancia.Yo.Emoticones.Values)
            {
                if (!File.Exists(emoticon.GetRuta()))
                {
                    emoticon.GuardarImagen();
                }

            }
            foreach (ContactoColor contacto in ColorMSN.Instancia.Contactos)
            {
                foreach (Emoticon emoticon in contacto.Emoticones.Values)
                {
                    if (!File.Exists(emoticon.GetRuta()))
                    {
                        emoticon.GuardarImagen();
                    }

                }
            }




        }

        internal static List<Grupo> CrearGrupos()
        {
            return new List<Grupo>() { new Grupo(NombreGrupo.Conectados, 3, false, true), new Grupo(NombreGrupo.Desconectados, 4, false, true), new Grupo(NombreGrupo.Todos, 1, false, false), new Grupo(NombreGrupo.Ocultos, 2, false, false) };
        }
    }


}
