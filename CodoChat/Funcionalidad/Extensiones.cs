﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;

namespace CodoChat
{
    static class Extensiones
    {


        public static void ChangeFont(this RichTextBox box, Font font, Color color)
        {
            box.ForeColor = color;
            box.Font = font;
        }
        public static void UIThreadInvoke(this Control control, Action code)
        {
            try
            {
                if (control.InvokeRequired)
                {
                    try
                    {
                        control.Invoke(code);
                    }
                    catch (Exception e)
                    {
                        Utiles.ImprimirExcepcion(e);
                    }
                    return;
                }
                else
                {
                    code.Invoke();
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("excepcion en extencsiones.cs " + e.Message);
            }

        }
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        public static bool EsVisibleDeVerdad(this Control control)
        {
            IntPtr activeHandle = GetForegroundWindow();
            return (activeHandle == control.Handle);
        }
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);
        private static int MakeLong(short lowPart, short highPart)
        {
            return (int)(((ushort)lowPart) | (uint)(highPart << 16));
        }

        public static void ListViewItem_SetSpacing(this ListView listview, short leftPadding, short topPadding)
        {
            const int LVM_FIRST = 0x1000;
            const int LVM_SETICONSPACING = LVM_FIRST + 53;
            SendMessage(listview.Handle, LVM_SETICONSPACING, IntPtr.Zero, (IntPtr)MakeLong(leftPadding, topPadding));
        }

        //terrible quilombo para hacer titilar la ventana


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

        [StructLayout(LayoutKind.Sequential)]
        private struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public UInt32 dwTimeout;
        }


        //Stop flashing. The system restores the window to its original state. 
        private const UInt32 FLASHW_STOP = 0;
        //Flash the window caption. 
        private const UInt32 FLASHW_CAPTION = 1;
        //Flash the taskbar button. 
        private const UInt32 FLASHW_TRAY = 2;
        //Flash both the window caption and taskbar button.
        //This is equivalent to setting the FLASHW_CAPTION | FLASHW_TRAY flags. 
        private const UInt32 FLASHW_ALL = 3;
        //Flash continuously, until the FLASHW_STOP flag is set. 
        private const UInt32 FLASHW_TIMER = 4;
        //Flash continuously until the window comes to the foreground. 
        private const UInt32 FLASHW_TIMERNOFG = 12;
        public static void Titilar(this Form form)
        {
            form.UIThreadInvoke(delegate
            {
                IntPtr hWnd = form.Handle;
                FLASHWINFO fInfo = new FLASHWINFO();

                fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
                fInfo.hwnd = hWnd;
                fInfo.dwFlags = FLASHW_ALL | FLASHW_TIMERNOFG;
                fInfo.uCount = UInt32.MaxValue;
                fInfo.dwTimeout = 0;
                FlashWindowEx(ref fInfo);
            });
        }
    }
}
