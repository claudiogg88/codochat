﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using agsXMPP;
using agsXMPP.protocol.client;
using agsXMPP.Collections;
using agsXMPP.protocol.iq.roster;
using System.Threading;
using System.ComponentModel;
using System.IO;
using System.Drawing;

using CodoChat.Modelo;
using System.Drawing.Imaging;
using Microsoft.Win32;
namespace CodoChat
{
    class MainColorMSN
    {


        static ColorMSN cuenta;

        [STAThread]
        public static void Main()
        {

            // The folder for the roaming current user 
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            // Combine the base folder with your specific folder....
            string specificFolder = Path.Combine(folder, "CodoChat");

            // Check if folder exists and if not, create it
            if (!Directory.Exists(specificFolder))
                Directory.CreateDirectory(specificFolder);


            SystemEvents.PowerModeChanged += OnPowerModeChanged;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Adornos.IniciarAdornos();

            cuenta = Utiles.LevantarCuenta();
            if (cuenta == null || cuenta.Asociadas.Count == 0)
            {
                cuenta = new ColorMSN();
            }
            GUI.Iniciar();
            cuenta.Iniciar();
            GUI.Run();
        }
        internal static void Cerrar()
        {
            try
            {
                ColorMSN.Instancia.Logout();
                Utiles.GuardarCuenta(cuenta);
            }
            catch (Exception e)
            {
                Utiles.ImprimirExcepcion(e);
            }
        }
        private static void OnPowerModeChanged(object sender, PowerModeChangedEventArgs e)
        {
        
            ColorMSN.Instancia.addLog("El equipo volvio de hibernarse?? ");
        }

    }
}